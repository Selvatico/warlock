#!/bin/sh
# Update System
#echo 'System Update'
#apt-get -y update
#echo 'Update completed'
# Install help app
#apt-get -y install libssl-dev git-core pkg-config build-essential curl gcc g++
# Download & Unpack Node.js - v. 0.8.15
echo 'Download Node.js - v. 0.8.15'
mkdir /tmp/node-install
cd /tmp/node-install
wget http://nodejs.org/dist/v0.8.15/node-v0.8.15.tar.gz
tar -zxf node-v0.8.15.tar.gz
echo 'Node.js download & unpack completed'
# Install Node.js
echo 'Install Node.js'
cd node-v0.8.15
./configure && make && make install
echo 'Node.js install completed'