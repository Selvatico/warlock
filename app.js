var express = require('./node_modules/express'),
    routes = require('./routes'),
    http = require('http'),
    path = require('path'),
    io = require('socket.io'),
    config = require("./config/config"),
    redis = require("redis"),
    connect = require('express/node_modules/connect'),
    MemoryStore = connect.middleware.session.MemoryStore,
    RedisStore = require('connect-redis')(express),
    store;

//require('look').start(3131);


var app = express();

//Redis set UP
app.redisClient = redis.createClient(config['redis']['port'], config['redis']['host'], config['redis']['opt']);
app.redisClient.on("error", function (err) {
    console.log("Redis CLient Error " + err);
});

app.appMyConfig = config;
app.cc = 11;

//configure
app.engine('.html', require('ejs').__express);
app.version = "0.5.1";
app.configure(function () {
    app.set('port', process.env.PORT || config.server.port);
    app.set('views', __dirname + '/views');
    app.set('view engine', 'html');
    app.use(express.favicon());

    //if (config.staticUrl == "") {
        app.use(express.static(path.join(__dirname, 'public')));
        app.use('/javascripts/cls/',express.static(path.join(__dirname, 'classes')));
    //}

    app.use(express.logger('dev'));
    app.use(express.bodyParser());
    app.use(express.methodOverride());
    app.use(express.cookieParser('card_game'));
    //app.use(express.session({secret:'card_game', key:'cd.sid', store:store = new MemoryStore()}));
    app.use(express.session({secret:'card_game', key:'cd.sid',
        store:store = new RedisStore({
            client:app.redisClient,
            host:config['redis']['host'],
            port:config['redis']['port'],
            prefix:"sess1"
        })
    }));
    app.use(app.router);

});
app.configure('development', function () {
    app.use(express.errorHandler());
});


app.basePath = __dirname;

//load and set main logic
routes.mvcLoader.templateGlobalVars(app);
routes.mvcLoader.bootControllers(app);
routes.mvcLoader.bootModels(app, config['mongo']);

//create servers and socket.io server
var server = http.createServer(app);
io = io.listen(server, {'flash policy port':10843, transports:['websocket',  'flashsocket']});
io.set('log level', 1);
global.io = io;

store.parseCookie = express.cookieParser('card_game');
require('./classes/TransportManager.js')(io, store);

server.listen(app.get('port'), config.server.host, function () {
    console.log("Express server listening on port " + config.server.host + ":" + app.get('port'));
});


process.on('uncaughtException', function (err) {
    console.log('Caught exception: ' + err);
});

