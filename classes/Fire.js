/**
 /**
 * Define Earth creatures module module
 */
(function (root, factory) {
    // Node.js
    if (typeof exports !== 'undefined') {
        module.exports = factory(root, require("./UIAnimator"), require("./Creature").Creature, exports);
    } else if (typeof define === 'function' && define.amd) {
        define(['cls/UIAnimator', 'cls/Creature', 'exports'], function (UIAnimator, Creature, exports) {
            root.Fire = factory(root, UIAnimator, Creature, exports);
            return root.Fire;
        });
    }
})(this, function (root, UIAnimator, Creature, Fire) {


    Fire = {cards: {}};

    Fire.baseConfig = {
        demon: {
            baseParams: {
                lives: 12, costs: 5, element: "fire",
                attackPower: 2, abilityCost: 1, abilityElement: "earth", castTarget: "none", animateR: 1,
                castFunction: function () {
                    var _self = this,
                        player = _self.owner;

                    player.changeResource("fire", 2, 1);

                    UIAnimator.effectAnimate("demonConvert",
                        {i: _self.boardIndex, uid: player.uid, fire: player.getElement("fire") + 2, "earth": player.getElement("earth") - 1 },
                        function () {
                        });
                }
            },
            override: {
                defendMagic: function (magicDamage, type) {
                    var _self = this;
                    if (type == "fire" || type == "earth") {
                        return this.lives;
                    }
                    return _self.ancestor(this, "defendMagic", [magicDamage, type]);
                }
            }
        },
        /**
         * Increase fire resources to owner and oponent +2 every turn
         */
        firelord: {
            baseParams: {
                lives: 21, costs: 11, element: "fire", attackPower: 7,
                abilityCost: 0, abilityElement: "fire", castTarget: "friend",
                /**
                 * @param {Creature} creature
                 */
                castFunction: function (creature) {
                    if (creature.magicImune) return false;
                    var _self = this;

                    UIAnimator.effectAnimate("firelordIncrease", {
                            i: _self.boardIndex,
                            uid: _self.owner.uid,
                            source: _self.boardIndex,
                            target: creature.boardIndex
                        },
                        function () {
                            creature.setAttackPower((creature.attackPower + 2), true).setCreatureBuff("startMove", "firelordIncrease");

                        });
                    return true;
                }
            },
            override: {
                startMove: function (playerOwner, playerOpponent) {
                    this.owner.changeResource("fire", 2);
                    playerOpponent.changeResource("fire", 2);
                }
            }
        },
        /**
         * has imune to all fire abilities and fire creatures
         */
        reddrake: {
            baseParams: {
                lives: 16, costs: 7, element: "fire", attackPower: 5
            },
            override: {
                afterCall: function () {
                    var _self = this;

                    UIAnimator.effectAnimate("fireBall", { uid: _self.owner.uid, i: _self.boardIndex}, function () {
                        _self.owner.oponent().applyToAll(function drakeInGame(creature) {
                            creature.defend(3, _self);
                        });
                    });
                },
                /**
                 * @param {Number} damadge
                 * @param {Creature} attackCreature
                 * @return {*}
                 */
                defend: function (damadge, attackCreature) {
                    var _self = this;
                    if (attackCreature.element == "fire") {
                        return _self.lives;
                    }
                    return _self.ancestor(this, "defend", [damadge, attackCreature]);
                },
                defendMagic: function (magicDamage, type) {
                    var _self = this;
                    if (type == "fire") {
                        return _self.lives;
                    }
                    return _self.ancestor(this, "defendMagic", [magicDamage, type]);
                }
            }
        },
        /**
         * ex devil
         */
        hellriser: {
            baseParams: {
                lives: 24, costs: 5, element: "fire", attackPower: 4,
                abilityCost: 0, abilityElement: "fire", castTarget: "custom:fire:firend",
                /**
                 * @param {Creature} targetCreature
                 */
                castFunction: function (targetCreature) {
                    if (targetCreature.element == "fire") {
                        targetCreature.die();
                        this.owner.changeResource("fire", 2);
                    }
                }
            },
            override: {
                afterDie: function () {
                    var _self = this;
                    _self.owner.receiveDamage(10);
                    this.ancestor(this, "afterDie", []);
                }
            }
        },
        berserk: {
            baseParams: {
                lives: 20, costs: 7, element: "fire", attackPower: 5,
                abilityCost: 0, abilityElement: "fire", castTarget: "none",
                castFunction: function () {
                    var _self = this,
                        opositeCreature;
                    if (_self.lives > 4) {
                        opositeCreature = _self.owner.getCreature(_self.boardIndex);
                        if (opositeCreature) {
                            opositeCreature.defend(9, _self);
                        }
                        _self.defend(4);
                    }
                }
            },
            override: {
                defend: function (damage, attackCreature) {
                    var _self = this;
                    damage += 2;
                    return _self.ancestor(this, "defend", [damage, attackCreature]);
                }
            }
        },
        /**
         * ex salamander makes +2 to creatures atack
         */
        succubus: {
            baseParams: {
                lives: 15, costs: 8, element: "fire", attackPower: 3
            },
            override: {
                afterCall: function () {
                    var _self = this;
                    _self.owner.applyToAll(function succubusIncrease(creature) {
                        creature.setAttackPower((creature.attackPower += 2), true);
                    });
                },
                afterDie: function () {
                    var _self = this,
                        position = _self.owner.color;

                    _self.owner.applyToAll(function succubusDecrease(creature) {
                        var newAttackPower = (creature.attackPower > 2) ? creature.attackPower - 2 : 0;
                        creature.setAttackPower(newAttackPower, true);
                    });
                    this.ancestor(this, "afterDie", []);
                }
            }
        },
        /**
         * ex blademaster
         */
        fireshaman: {
            baseParams: {
                lives: 30, costs: 9, element: "fire", attackPower: 8,
                abilityCost: 0, abilityElement: "fire", castTarget: "none",
                castFunction: function () {
                    var _self = this;
                    UIAnimator.animate("centaurShot", {self: _self});
                    this.owner.oponent().receiveDamage(2);

                }
            },
            override: {
                defend: function (damage, attackCreature) {
                    var _self = this;
                    _self.owner.oponent().applyToAll(function shamnStrike(creature) {
                        creature.defend(1, _self);
                    });
                    return _self.ancestor(this, "defend", [damage, attackCreature]);
                }
            }
        },
        infernal: {
            baseParams: {lives: 44, costs: 10, element: "fire", attackPower: 9},
            override: {
                afterCall: function () {
                    var _self = this,
                        skipLose = false;

                    _self.owner.applyToAll(function infernal(monster) {
                        if (!skipLose && monster.element == "fire" && monster.boardIndex != _self.boardIndex) {
                            skipLose = true;
                            monster.die();
                        }
                    });
                    if (!skipLose) {
                        _self.defend(15, false);
                    }

                }
            }
        },
        walloffire: {
            baseParams: { lives: 25, costs: 3, element: "fire", attackPower: 0},
            override: {
                afterCall: function () {
                    var _self = this;
                    _self.owner.oponent().applyToAll(function icewallIn(monster) {
                        monster.defend(3, _self);
                    });
                },
                strike: function () {},
                setAttackPower: function () {},
                startMove: function () {
                    this.defend(4, false);
                }
            }
        },
        /**
         * When someone attack efreet he receive half of damage back.
         * Cab cast fire sheeld to friendly creture, but this creature will loose 2 health per turn
         */
        efreet: {
            baseParams: {
                lives: 33, costs: 10, attackPower: 6, element: "fire",
                abilityCost: 2, abilityElement: "fire", castTarget: "friend",
                /**
                 * @param {Creature} targetCreature
                 */
                castFunction: function (targetCreature) {
                    if (targetCreature && targetCreature.element != "fire") {
                        targetCreature.setCreatureBuff("startMove", "efreetInjure");
                    }
                    targetCreature.setCreatureBuff("defend", "efreetDefend");
                }
            },
            override: {
                /**
                 *
                 * @param {Number} damage
                 * @param {Creature} attackCreature
                 */
                defend: function (damage, attackCreature) {
                    var _self = this,
                        damageBack = Math.ceil(damage / 2);

                    attackCreature.defendMagic(((damageBack < 1) ? 1 : damageBack), "fire", true);
                    UIAnimator.animate("efreetBackDamage", attackCreature);

                    return _self.ancestor(this, "defend", [damage, attackCreature]);
                }
            }
        },
        vulcano: {
            baseParams: {
                lives: 27, costs: 12, attackPower: 1, element: "fire",
                abilityCost: 2, abilityElement: "fire", castTarget: "none",
                /**
                 * @param {Creature} targetCreature
                 */
                castFunction: function (targetCreature) {
                    var _self = this,
                        damageToHit = Math.ceil(_self.lives / 2);
                    _self.owner.applyToAll(function vulcanoExplose(creature) {
                        creature.defendMagic(damageToHit, "fire");
                    });

                    _self.owner.oponent().applyToAll(function vulcanoExplose(creature) {
                        creature.defendMagic(damageToHit, "fire");
                    });
                    _self.die();
                    UIAnimator.animate("vulcanoExplose", { a: 1});
                }
            },
            override: {
                afterCall: function () {
                    var _self = this,
                        owner = _self.owner,
                        opositeCreature;

                    _self.setAttackPower((owner.getElement("fire") + 3), true);
                    owner.oponent().changeResource("fire", -3);
                    opositeCreature = _self.owner.getCreature(_self.boardIndex);
                    if (opositeCreature) {
                        opositeCreature.defend(9, _self);
                    }
                },
                startMove: function () {
                    var _self = this;
                    _self.attackPower = _self.owner.getElement("fire") + 3;
                },
                defendMagic: function (magicDamage, type) {
                    if (!type === "fire") {
                        this.ancestor(this, "defendMagic", arguments);
                    }
                }
            }
        },
        /**
         * All units on a field suffer $25 ^damage. Each player suffers $25 ^damage.
         */
        armageddon: {
            type: "spell", target: "none", costs: 11, element: "fire",
            castSpell: function () {
                var _self = this;
                _self.receiveDamage(25);
                _self.oponent().receiveDamage(25);
                _self.applyToAllInBattle(function armageddon(creature) {
                    creature.defendMagic(25, "fire");
                });
            }
        },
        blind: {
            type: "spell", target: "none", costs: 7, element: "fire",
            castSpell: function () {
                var _self = this;
                _self.oponent().applyToAll(function blind(creature) {
                    if (creature.notIsImmune()) {
                        creature.setAttackPower(false, true, -3).setConfig({justCalled: true});
                    }
                });
            }
        },
        /**
         * You gain $6 Fire. ^As a cost, your creatures suffer $3 ^damage each.
         */
        bloodritual: {
            type: "spell", target: "none", costs: 4, element: "fire",
            castSpell: function () {
                var _self = this;
                _self.changeResource("fire", 6)
                    .applyToAll(function bloodRitual(creature) {
                        creature.defendMagic(3, "fire");
                    });
            }
        },
        fireball: {
            type: "spell", target: "none", costs: 8, element: "fire",
            castSpell: function () {
                var _self = this,
                    damage = _self.getElement("fire") + 3;

                UIAnimator.effectAnimate("fireBall", {source: "hero", uid: _self.uid}, function () {
                    _self.oponent().applyToAll(function fireballCast(creature) {
                        creature.defendMagic(damage, "fire");
                    });
                })
            }
        },
        firespikes: {
            type: "spell", target: "none", costs: 3, element: "fire",
            castSpell: function () {
                var _self = this;
                UIAnimator.effectAnimate("fireBall", {source: "hero", uid: _self.uid}, function () {
                    _self.oponent().applyToAll(function fireballCast(creature) {
                        creature.defendMagic(3, "fire");
                    });
                });
            }
        },
        flamingarrow: {
            type: "spell", target: "none", costs: 4, element: "fire",
            castSpell: function () {
                var _self = this,
                    oponent = _self.oponent(),
                    myFire = _self.getElement("fire"),
                    oponentFire = oponent.getElement("fire"),
                    damage = (myFire > oponentFire) ? ((myFire - oponentFire) * 2) : 1;

                UIAnimator.effectAnimate("flammingArrow", {uid: _self.uid}, function () {
                    oponent.receiveDamage(damage);
                });
            }
        },
        /**
         * Your enemy loses $5 Fire ^elements, while you suffer $5 ^damage as a cost for using these forbidden techniques.
         */
        forbiddenmagic: {
            type: "spell", target: "none", costs: 3, element: "fire",
            castSpell: function () {
                var _self = this;
                _self.oponent().changeResource("fire", -5);
                _self.receiveDamage(5);
            }
        }
    };

    Creature.inherit(Fire);
    return Fire;

});


