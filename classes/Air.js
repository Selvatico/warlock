/**
 * Define Air creatures module module
 */
(function (root, factory) {
    // Node.js
    if (typeof exports !== 'undefined') {
        var mediator = {
            trigger: function () {}
        };
        module.exports = factory(root, require("./UIAnimator"), require("./Creature").Creature, exports, mediator);
    } else if (typeof define === 'function' && define.amd) {
        define(['cls/UIAnimator', 'cls/Creature', 'exports', 'service/mediator'], function (UIAnimator, Creature, exports, mediator) {
            root.Air = factory(root, UIAnimator, Creature, exports);
            return root.Air;
        });
    }
})(this, function (root, UIAnimator, Creature, Air, mediator) {

        Air = {cards: {}};

        Air.baseConfig = {
            /**
             * Has ability that enemy creature will attack it is owner
             */
            fairy: {
                baseParams: { lives: 7, costs: 3, element: "air", attackPower: 3,
                    abilityCost: 1, abilityElement: "air", castTarget: "enemy",
                    /**
                     * @param {Creature} creature
                     */
                    castFunction: function (creature) {
                        creature.owner.receiveDamage(creature.attackPower);
                        UIAnimator.animate("fairyCast", creature);
                    }
                },
                override: {
                    afterCall: function () {
                        this.owner.addPreAction("afterDie", "fairyIncreaseDamage");
                    },
                    afterDie: function () {
                        this.owner.removePreAction("afterDie", "fairyIncreaseDamage");
                        this.ancestor(this, "afterDie", []);
                    }
                }
            },
            nymph: {
                baseParams: { lives: 12, costs: 3, element: "air", attackPower: 1},
                override: {
                    startMove: function () {
                        this.owner.changeResource("air", 1);
                    }
                }
            },
            gargoyle: {
                baseParams: { lives: 16, costs: 5, element: "air", attackPower: 4,
                    abilityCost: {air: 3, earth: 1}, abilityElement: "air", castTarget: "none",
                    castFunction: function () {
                        this.physicalReduction = 2;
                    }
                },
                override: {
                    defendMagic: function (magic, type) {
                        if ({earth: 1, air: 1}[type]) {
                            return this.lives;
                        } else {
                            return this.ancestor(this, "defendMagic", arguments);
                        }
                    }
                }
            },
            phoenix: {
                baseParams: { lives: 20, costs: 6, element: "air", attackPower: 4},
                override: {
                    die: function (opCreature) {
                        if (opCreature && opCreature.element && opCreature.element == "fire") {
                            this.changeHitPoints(999);
                        } else {
                            this.ancestor(this, "die", arguments);
                        }
                    }
                }
            },
            // add set attacks equal to oposite creature
            soulofwinds: {
                baseParams: { lives: 200, costs: 6, element: "air", attackPower: 4},
                override: {
                    startMove: function () {
                        var _self = this;
                        _self.ancestor(this, "defend", [20, false]);
                    },
                    defend: function (damage, attackCreature) {
                        this.ancestor(this, "defend", [20, attackCreature]);
                    }
                }
            },
            manticore: {
                baseParams: { lives: 19, costs: 7, element: "air", attackPower: 5,
                    abilityCost: 2, abilityElement: "air", castTarget: "enemy",
                    /**
                     *
                     * @param {Creature} creature
                     */
                    castFunction: function (creature) {
                        if (creature.abilityElement) {
                            creature.abilityElement = false;

                            mediator.trigger("manticora-cast", creature);
                        }
                    }
                },
                override: {
                    strike: function (opCreature, opPlayer) {
                        if (opCreature.abilityElement) {
                            this.attackPower += 3;
                        }
                        this.ancestor(this, "strike", [opCreature, opPlayer]);
                        if (opCreature.abilityElement) {
                            this.attackPower -= 3;
                        }
                    },
                    defendMagic: function (magicDamage, type) {
                        magicDamage = Math.ceil(magicDamage / 2);
                        this.ancestor(this, "defendMagic", [magicDamage, type]);
                    }
                }
            },
            zeus: {
                baseParams: { lives: 20, costs: 9, element: "air", attackPower: 3,
                    abilityCost: 2, abilityElement: "air", castTarget: "enemy",
                    /**
                     * Strikes light and inflict 8 damage, cannot strike creature of level 7  and higher
                     * @param {Creature} creature
                     */
                    castFunction: function (creature) {
                        if (creature.costs < 7) {
                            creature.defendMagic(8, "air", true);
                            return true;
                        }
                        return false;
                    }
                },
                override: {
                    /**
                     *
                     * @param {Creature} opCreature
                     * @param {Hero} opPlayer
                     */
                    strike: function (opCreature, opPlayer) {
                        if (opCreature.creatureName == "lordlight") {
                            opCreature.die();
                        } else {
                            this.ancestor(this, "strike", [opCreature, opPlayer]);
                        }
                    }
                }
            },
            lordlight: {
                baseParams: { lives: 22, costs: 9, element: "air", attackPower: 5,
                    abilityCost: 2, abilityElement: "air", castTarget: "enemy",
                    castFunction: function (creature) {
                        creature.defendMagic(this.owner.getElement("air"), "air", true);
                    }
                },
                override: {
                    afterCall: function () {
                        this.owner.addPreAction("castSpell", "lordlightHeal");
                    },
                    strike: function (opCreature, opPlayer) {
                        if (opCreature.creatureName == "zeus") {
                            opCreature.die();
                        } else {
                            this.ancestor(this, "strike", [opCreature, opPlayer]);
                        }
                    },
                    afterDie: function () {
                        this.owner.removePreAction("castSpell", "lordlightHeal");
                        this.ancestor(this, "afterDie", []);
                    }
                }
            },
            titan: {
                baseParams: { lives: 28, costs: 11, element: "air", attackPower: 7,
                    abilityCost: 1, abilityElement: "air", castTarget: "none",
                    castFunction: function () {
                        this.owner.oponent().applyToAll(function TitanFist(creature) {
                            if (creature.element == "earth") {
                                creature.defendMagic(3, "air", true);
                            }
                        });
                    }
                },
                override: {
                    afterCall: function () {
                        this.owner.oponent().changeResource("air", -3);
                    },
                    startMove: function () {
                        var _self = this,
                            setAttack = _self.defaultAttackPower;

                        this.owner.applyToAll(function titanCheck(monster) {
                            if (monster.element == "air" && monster.boardIndex != _self.boardIndex) {
                                setAttack += 1;
                            }
                        });

                        if (setAttack > 0) {
                            _self.setAttackPower(setAttack, true);
                        }
                    }
                }
            },
            kronos: {
                baseParams: { lives: 24, costs: 10, element: "air", attackPower: 4,
                    abilityCost: 3, abilityElement: "air", castTarget: "enemy",
                    /**
                     *
                     * @param {Creature} opCreature
                     */
                    castFunction: function (opCreature) {
                        var newHp = Math.ceil(opCreature.lives / 2),
                            diff = opCreature.lives - newHp;

                        opCreature.maxHealthPoint = newHp;
                        if (diff > 0) {
                            opCreature.changeHitPoints(-diff);
                        }
                    }
                },
                override: {
                    /**
                     * @param {Creature} opCreature
                     * @param {Hero} opPlayer
                     */
                    strike: function (opCreature, opPlayer) {
                        var _self = this,
                            oldAttack = this.attackPower;
                        if (opCreature.costs < 10) {
                            _self.setAttackPower((oldAttack + (_self.costs - opCreature)));
                        }
                        _self.ancestor(this, "strike", arguments);
                        _self.setAttackPower(oldAttack);

                    }
                }
            },
            astralcloud: {
                baseParams: { lives: 28, costs: 11, element: "air", attackPower: 7},
                override: {
                    afterCall: function () {

                    },
                    defend: function (damage, attackCreature) {
                        damage -= 2;
                        this.ancestor(this, "defend", [damage, attackCreature]);
                    }
                }
            },
            /*    guardian : {
             baseParams:{ lives:30, costs:8, element:"air", attackPower:5,
             abilityCost: 1, abilityElement:"air", castTarget : "friend",
             *//**
             * @param {Creature} monster
             *//*
             castFunction:function (monster) {

             }},
             override:{
             afterDie : function () {
             var _self = this;

             }
             }
             },*/
            /**
             * Reduce damage taken by creature for 5
             */
            airshield: {
                type: "spell", target: "friend", costs: 4, element: "air",
                castSpell: function () {
                    this.applyToAll(function setShield(monster) {
                        monster.physicalReduction += 2;
                    });

                }
            },
            blackwind: {
                type: "spell", target: "none", costs: 8, element: "air",
                castSpell: function () {
                    var _self = this,
                        index = 0,
                        highest = 0;

                    _self.oponent().applyToAll(function blackwind(creature) {
                        if (creature.costs >= highest) {
                            highest = creature.costs;
                            index = creature.boardIndex;
                        }
                    });

                    if (highest > 0) {
                        _self.oponent().boardHolder[index].die();
                    }
                }
            },
            lighting: {
                type: "spell", target: "none", costs: 9, element: "air",
                castSpell: function () {
                    var _self = this,
                        damageToGive = _self.getElement("air") + 2;

                    _self.oponent().applyToAll(function lighting(creature) {
                        if (creature.magicImune) return true;
                        creature.defendMagic(damageToGive, "air");
                        damageToGive -= 2;
                        return true;
                    });
                }
            },
            healingwind: {
                type: "spell", target: "none", costs: 4, element: "air",
                castSpell: function () {
                    var _self = this,
                        toHeal = 0;
                    _self.applyToAllInBattle(function healingwind(monster) {
                        if (monster.magicImune) return true;
                        toHeal += 1;
                        if (monster.element == "air") {
                            toHeal += 1;
                        }
                        return true;
                    });

                    if (toHeal > 0) {
                        _self.heal(toHeal);
                    }
                }
            },
            lightningbolt: {
                type: "spell", target: "none", costs: 4, element: "air",
                castSpell: function () {
                    var _self = this;
                    _self.oponent().receiveDamage(6);
                    UIAnimator.animate("lightningbolt", _self);
                }
            },
            moonlight: {
                type: "spell", target: "none", costs: 9, element: "air",
                castSpell: function () {
                    var _self = this;
                    _self.applyToAllInBattle(function moonlight(monster) {
                        monster.changeHitPoints(9999);
                    });
                }
            },
            plague: {
                type: "spell", target: "none", costs: 12, element: "air",
                castSpell: function () {
                    var _self = this;
                    _self.applyToAllInBattle(function plague(monster) {
                        var toChange = monster.lives - 1;
                        monster.changeHitPoints(-toChange);
                    });
                }
            },
            starfall: {
                type: "spell", target: "none", costs: 3, element: "air",
                castSpell: function () {
                    var _self = this,
                        damageCount = 0;
                    _self.oponent().applyToAll(function plague() {
                        damageCount++;
                    });

                    _self.oponent().applyToAll(function starfall(monster) {
                        monster.defendMagic(damageCount, "air")
                    });
                }
            },
            wingsofwisdom: {
                type: "spell", target: "none", costs: 8, element: "air",
                castSpell: function () {
                    var _self = this;
                    _self.changeResource({
                        fire: 2,
                        water: 2,
                        earth: 2,
                        life: 2,
                        death: 2
                    })
                }
            },
            spellbreaker: {
                type: "spell", target: "none", costs: 7, element: "air",
                castSpell: function () {
                    var _self = this;
                    _self.applyToAll(function setspellbraker(monster) {
                        monster.magicImune = true;
                    });
                }
            }
        };


        //define aour creatures from main creature class
        Creature.inherit(Air);

    return Air;
});