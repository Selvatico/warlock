(function (root, factory) {
    // Node.js
    if (typeof exports !== 'undefined') {

        var mediator = { trigger: function(){}};
        module.exports.CardsHero = factory(require("events"), require('util'), require("./Hero").Hero, require("./Desk").Desk, mediator);

    } else if (typeof define === 'function' && define.amd) {
        //client side require.js definition
        define(
            [
                'service/events',
                'service/helper',
                'cls/Hero',
                'cls/Desk',
                'service/mediator'
            ], function (Events, Helper, Hero, Desk, mediator) {

                root.CardsHero = factory(Events, Helper, Hero, Desk, mediator);
                return root.CardsHero;

            });
    }
})(this, function (events, util, Hero, Desk, mediator) {

    /**
     *
     * @class Represent Game
     * @inheritDoc EventEmitter
     * @constructor
     */
    function CardsHero() {

        /**
         * Store temp socket ids
         * @type {Array}
         */
        this.tempSockets = [];

        this.CONNECTED = 1;

        this.DISCONNECTED = -1;

        /**
         * Unique game identificator
         * @type {String}
         */
        this.uid = this.createUID();


        /**
         * To hold timer ID to terminate if member come back to game
         * @type {Object}
         */
        this.terminateStatement = {top: false, bottom: false};

        /**
         *
         * @type {Desk}
         */
        this.desk = false;

        /**
         * List of members in the game. Can't be changed after game start.
         * Indicates disconnects and reconnects
         * @type {Object}
         */
        this.playerInGame = {top: false, bottom: false};

        /**
         * Object with players {Hero} objects.
         * @type {Object}
         */
        this.players = {};


        /**
         * Top  - first, bottom - second
         * @type {String}
         */
        this.currentTurn = "top";


        /**
         * Game status pending, run, paused
         * @type {String}
         */
        this.gameStatus = "pending";

        /**
         * Step of inheritance
         */
        events.EventEmitter.call(this);


        /**
         * Set game winner ID
         * @type {String}
         */
        this.winner = null;


        /**
         *
         * @type {CardsHeroManager}
         */
        this.manager = false;

        /**
         * Perform main create opearations
         */
        this.createGame();

        /**
         *  Time when game finished
         * @type {Date}
         */
        this.gameStartTime = null;


        /**
         * Log of main game actiona ex. callCard, castspell
         * @type {Array}
         */
        this.gameLog = [];
    }


    util.inherits(CardsHero, events.EventEmitter);


    /**
     * Return list of most config to unsearilize on client side
     * @private
     */
    CardsHero.prototype._serialize = function () {
        var _self = this,
            dataToGo = {
                gameStatus: _self.gameStatus,
                currentTurn: _self.currentTurn,
                playerInGame: _self.playerInGame,
                uid: _self.uid,
                gameLog: _self.gameLog
            };
        return (dataToGo);
    };


    /**
     * Reinit log messages on client side
     */
    CardsHero.prototype.reinitUILog = function () {
        if (this.gameLog.length > 0) {
            $.each(this.gameLog, function (index, logEntry) {
                var logMsg = {
                    type: "game",
                    actor: "player",
                    action: (logEntry.what.spell) ? "castSpell" : "callMonster",
                    actorName: logEntry.who,
                    internalName: logEntry.what.name,
                    showName:  logEntry.what.name,
                    time: logEntry.time
                };
                //CardsUI.service.log(logMsg)
                mediator.trigger('log-entry', logMsg);
            });
        }
    };

    /**
     * Set interval to destroy game after member disconnected from game
     * @param {String} uid Player UID
     */
    CardsHero.prototype.setDestroyInterval = function (uid) {
        var _self = this,
            color = (_self.playerInGame.top == uid) ? "top" : "bottom",
            oponent = (color == "top") ? "bottom" : "top",
            oponentID = this.playerInGame[oponent];

        _self.terminateStatement[color] = setTimeout(function destroyGameDisconenct() {
            console.log("Launch restroy after tmiout");

            //set winner and other things
            _self.players[oponentID].becomeWinner();

            //send to memeber notification
            _self.sendToGame("syncGame", {i: oponentID, sync: "becomeWinner", args: [true]}, oponentID);

        }, 60000);
    };


    /**
     * Terminate destroy game when member returned to game
     * @param uid
     */
    CardsHero.prototype.clearDestroyTimeout = function (uid) {
        var color = (this.playerInGame.top == uid) ? "top" : "bottom";
        clearTimeout(this.terminateStatement[color]);
        this.terminateStatement[color] = false;
    };

    /**
     * Set current winner
     * @param {String} uid Winner ID
     */
    CardsHero.prototype.setWinner = function (uid) {
        if (this.playerInGame.top == uid || this.playerInGame.bottom == uid) {
            this.winner = uid;
        } else {
            throw Error("Trying to set winner with not exist ID");
        }
        return this;
    };

    /**
     * Set properties
     * @param config
     */
    CardsHero.prototype.setConfig = function (config) {
        if (config && Object.keys(config).length > 0) {
            for (var k in config) {
                if (config.hasOwnProperty(k) && this.hasOwnProperty(k)) {
                    this[k] = config[k];
                }
            }
        }
    };

    /**
     * Create Unique ID for players and games
     * @param {Number} [length] Length of ID
     * @return {String} generated UID
     */
    CardsHero.prototype.createUID = function (length) {
        var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz",
            randomisation = '',
            llen = length || 14;
        for (var i = 0; i < llen; i++) {
            var rnum = Math.floor(Math.random() * chars.length);
            randomisation += chars.substring(rnum, rnum + 1);
        }
        return randomisation;
    };

    /**
     * Check if game has free seatsto conenct
     * @return {Boolean}
     */
    CardsHero.prototype.hasFreeSeats = function () {
        return (!this.playerInGame.top || !this.playerInGame.bottom);
    };

    /**
     * Check if game status allow to connect
     * @return {Boolean}
     */
    CardsHero.prototype.isPending = function () {
        return this.gameStatus == "pending";
    };

    /**
     * Check if member already registered in this game
     * @param {Number} uid Identifier member from database
     */
    CardsHero.prototype.inGame = function (uid) {
        return  (this.playerInGame.top == uid || this.playerInGame.bottom == uid);
    };

    /**
     * Check if member is connected
     * @param {Number} id MemberId of player
     * @return {Boolean}
     */
    CardsHero.prototype.isConnected = function (id) {
        return this.players[id].status == this.CONNECTED;
    };

    /**
     * After reconnect or after connect second member continue game
     */
    CardsHero.prototype.continueGame = function (uid) {
        var _self = this;
        if (_self.players[this.playerInGame.top].status == _self.CONNECTED && _self.players[this.playerInGame.bottom].status == _self.CONNECTED) {
            if (_self.gameStatus == "pause") {
                _self.clearDestroyTimeout(uid);
                _self.sendToGame("continue", {i: _self.uid});
            } else if (_self.gameStatus == "wait") {
                _self.sendToGame("start", {i: _self.uid});
            }
            _self.setGameStatus("run");
        }
        return this;
    };

    /**
     *Save socket id for future send events to players about "move", "start game"
     */
    CardsHero.prototype.setSocketLink = function (uid, socketId) {
        var _self = this;
        this.players[uid].socketId = socketId;
        this.players[uid].status = this.CONNECTED;
        this.sendToGame("connect", {cr: _self.players[uid].color});
        return this;
    };


    CardsHero.prototype.createGame = function () {
        var _self = this;
        _self.once("start:game", function (startResult) {
            if (startResult) {
                _self.setGameStatus("wait").sendToGame("play", {d: _self.uid}, _self.tempSockets);
            }
        });
    };


    /**
     * Create new CardsHero object and save it in game class
     * @param memberConfig
     * @return {Hero}
     */
    CardsHero.prototype.addPlayer = function (memberConfig) {
        var _self = this,
            hero = new Hero({login: memberConfig.u, uid: memberConfig.uid, socketId: memberConfig.id});

        //set pointer to game object for reversed hierarhy calls
        hero.gameLink = this;

        if (!_self.playerInGame.top || !_self.players[_self.playerInGame.top]) {
            _self.playerInGame.top = memberConfig.uid;
            hero.color = "top";
            hero.oponentColor = "bottom";
        } else {
            _self.playerInGame.bottom = memberConfig.uid;
            hero.color = "bottom";
            hero.oponentColor = "top";
        }
        _self.players[memberConfig.uid] = hero;

        //subscribe once for start game event
        if (hero.color == "bottom") {
            //call start game
            _self.startGame();
        }
        return hero;
    };


    /**
     * Method for start game and send dignal to players to start
     * @return {CardsHero} this return pointer to itself for chain call
     */
    CardsHero.prototype.startGame = function () {
        var _self = this,
            playerInGame = _self.playerInGame,
            randomedELem;
        //generate cards
        _self.desk = new Desk();
        _self.desk.assignCards(_self.players[playerInGame.top], _self.players[playerInGame.bottom]);

        //getrandom amount of elements
        randomedELem = _self.desk.randomElements();
        //assign random elements
        _self.players[playerInGame.top].setElements(randomedELem["1"]);
        _self.players[playerInGame.bottom].setElements(randomedELem["2"]);

        //emit start game
        _self.emit("start:game", true);
        _self.gameStartTime = (new Date()).valueOf();

        //change manager counters
        _self.manager.counter.pending--;
        _self.manager.counter.progress++;
        return this;
    };

    /**
     * Check if player already connected to game
     * to avoid duplicates sockets to one game from one player
     * @param uid
     * @return {*}
     */
    CardsHero.prototype.inGame = function (uid) {
        return this.players[uid];
    };

    /**
     * Hold player and pause game if socket disconnected for example.
     * @param {String} uid Member UID
     */
    CardsHero.prototype.holdPlayer = function (uid) {
        var _self = this;
        if (this.players[uid] && this.players[uid].status === this.CONNECTED) {
            this.players[uid].status = this.DISCONNECTED;
            this.players[uid].socketId = false;
            _self.setGameStatus("pause")
                .sendToGame("disconnect", {cr: this.players[uid].color, uid: uid});

            //@TODO: GET BACK THIS CODE
            //launch timer before loose
            //_self.setDestroyInterval(uid);
        }
    };


    /**
     * Normal end of the game
     */
    CardsHero.prototype.endGame = function () {
        var _self = this;
        setTimeout(function () {
            if (_self.manager) {
                _self.manager.destroyGame(_self.uid);
            }
        }, 5000);
    };


    /**
     * Utilite method to destroy the object
     */
    CardsHero.prototype.destroyGame = function () {
    };

    /**
     * Send commet message to member via socket.io
     * @param {String} action Action name on client
     * @param {Object} data Data to send in JSON format
     * @param {String|Array} [targetMember] Target member socket ID or array of sockets
     */
    CardsHero.prototype.sendToGame = function (action, data, targetMember) {
        var _self = this, targetSockets = [], regMember = _self.playerInGame;

        if (util.isArray(targetMember)) {
            targetSockets = targetMember;
        } else if (targetMember && _self.players[targetMember]) {
            targetSockets.push(_self.players[targetMember].socketId);
        } else {
            if (regMember.top) {
                targetSockets.push(_self.players[regMember.top].socketId);
            }
            if (regMember.bottom) {
                targetSockets.push(_self.players[regMember.bottom].socketId);
            }
        }
        data.a = action;
        if (targetSockets.length > 0) {
            targetSockets.forEach(function (recipient) {
                if (global.io.sockets.sockets.hasOwnProperty(recipient)) {
                    global.io.sockets.sockets[recipient].send(JSON.stringify(data));
                }
            });
        }
    };

    /**
     * Return oponent object
     * @param {String} id User id
     * @return {Hero}
     */
    CardsHero.prototype.getOponent = function (id) {
        var color = (this.playerInGame.top == id) ? "bottom" : "top";
        return this.players[this.playerInGame[color]];
    };


    /**
     * Check if gameStatus is valid and set it
     * @param {String} gameStatus Status of the game
     * Default - "pending" - set to game after object create
     * Other statuses:
     * - "run" - game in progress
     * - "pause"- when some one disconencted from the game
     * - "wait" - when game waiting players to connect
     * - "finished" - game finished
     */
    CardsHero.prototype.setGameStatus = function (gameStatus) {
        if ({pending: 1, pause: 1, wait: 1, finished: 1, run: 1}[gameStatus]) {
            this.gameStatus = gameStatus;
        } else {
            console.trace("wrong game status");
            throw Error("Attempt to set wrong game status");
        }
        return this;
    };


    CardsHero.prototype.isFinished = function () {
        return this.gameStatus === "finished";
    };


    /**
     * New method to gather data for template
     * @param playerUid
     */
    CardsHero.prototype.gatherGameInfo = function (playerUid) {
        var _self = this, itself = {};

        for (var itin in _self) {
            if (_self.hasOwnProperty(itin)) {
                itself[itin] = _self[itin];
            }
        }

        return {
            gameID: _self.uid,
            me: _self.getPlayerObj(playerUid)._serialize(),
            oponent: _self.getPlayerObj(playerUid).oponent()._serialize(),
            itself: _self._serialize()
        }
    };


    /**
     * Return player game object
     * @param {String} uid User ID
     * @return {Boolean|Hero}
     */
    CardsHero.prototype.getPlayerObj = function (uid) {
        return (this.players[uid]) ? this.players[uid] : false;
    };


    /**
     * Return count of players registered in the game
     * @return {Number}
     */
    CardsHero.prototype.countPlayers = function () {
        return Object.keys(this.players).length;
    };


    return CardsHero;
});

