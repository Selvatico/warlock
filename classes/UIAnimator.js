(function (root, factory) {
    // Node.js
    if (typeof exports !== 'undefined') {

        module.exports = factory({});

    } else if (typeof define === 'function' && define.amd) {
        //client side require.js definition
        define(['service/mediator'], function (Mediator) {
            root.UIAnimator = factory(Mediator);
            return root.UIAnimator;
        });
    }
})(this, function (Mediator) {

    Array.prototype.remove = function (from, to) {
        var rest = this.slice((to || from) + 1 || this.length);
        this.length = from < 0 ? this.length + from : from;
        return this.push.apply(this, rest);
    };

//P = (1−t)2*P1 + 2(1−t)tP2 + t2P3
    var Point = function (cord) {
        this.x = cord.x;
        this.y = cord.y;
    };

    Point.prototype = {
        constructor: Point,
        addPoints: function (points) {
            var _self = this;
            if (points && points.length > 0) {
                points.forEach(function (pointAdd) {
                    if (pointAdd.x && pointAdd.y) {
                        _self.x += pointAdd.x;
                        _self.y += pointAdd.y;
                    }
                });

            }
            return this;

        },
        multiple: function (numberM) {
            this.x *= numberM;
            this.y *= numberM;
            return this;
        },
        val: function () {
            var _self = this;
            return {x: _self.x, y: _self.y}
        }
    };


    var CanvasAnimation = {
        playerCanvas: null,
        playersContext: null,
        boardCanvas: null,
        boardContext: null,
        coordinatesPlayer: {
            topDeck: {
                health: {x: 67, y: 69},
                elements: {
                    water: {x: 648, y: 32},
                    fire: {x: 708, y: 32},
                    air: {x: 770, y: 32},
                    earth: {x: 831, y: 32},
                    life: {x: 890, y: 32},
                    death: {x: 953, y: 32}
                }
            },
            bottomDeck: {
                health: {x: 931, y: 417},
                elements: {
                    water: {x: 47, y: 454},
                    fire: {x: 106, y: 454},
                    air: {x: 166, y: 454},
                    earth: {x: 222, y: 454},
                    life: {x: 287, y: 454},
                    death: {x: 344, y: 454}
                }
            }
        },
        coordinatesBattle: {
            itemH: 150,
            itemW: 115,
            topDeck: {
                baseY: 81,
                baseCenterY: 160,
                baseBotY: 200,
                "1": {x: 185, center: {x: 243}},
                "2": {x: 320, center: {x: 378}},
                "3": {x: 455, center: {x: 513}},
                "4": {x: 590, center: {x: 648}},
                "5": {x: 725, center: {x: 783}}
            },
            bottomDeck: {
                baseY: 248,
                baseCenterY: 327,
                baseBotY: 300,
                "1": {x: 186, center: {x: 243}},
                "2": {x: 321, center: {x: 378}},
                "3": {x: 456, center: {x: 513}},
                "4": {x: 591, center: {x: 648}},
                "5": {x: 726, center: {x: 783}}
            }
        },
        init: function () {
            if (tpl == "game_play") {
                this.playerCanvas = document.getElementById("playerAnimate");
                this.playersContext = this.playerCanvas.getContext("2d");

                this.boardCanvas = document.getElementById("battleAnimate");
                this.boardContext = this.playerCanvas.getContext("2d");
            }

        },
        canvaShow: function () {
            this.playerCanvas.style.display = "block";
        },
        canvaHide: function () {
            this.playerCanvas.style.display = "none";
        },
        _getCtx: function () {
            return this.playersContext;
        },
        drawCircle: function (coord, radius) {
            var ctx = this._getCtx();

            ctx.beginPath();
            var gradient = ctx.createRadialGradient(coord.x, coord.y, 0, coord.x, coord.y, radius);
            gradient.addColorStop(0, "#FFFF00");
            gradient.addColorStop(0.4, "red");
            gradient.addColorStop(1, "white");
            ctx.fillStyle = gradient;
            ctx.arc(coord.x, coord.y, 10, 0, Math.PI * 2, false);
            ctx.fill();
        },
        calculateBezier: function (start, control, endpoint, current) {
            var startObj = new Point(start);
            var secondObj = new Point(control);
            var thirdObj = new Point(endpoint);

            //calculate bezier curve
            return startObj
                .multiple((Math.pow(1 - current, 2)))
                .addPoints([secondObj.multiple(2 * (1 - current) * current), thirdObj.multiple(Math.pow(current, 2))]);
        },
        demonConvert: function (config, func) {
            if (config.i) {
                var _self = this,
                    coordHolder = (config.uid == CardsUI.myID) ? "bottomDeck" : "topDeck",
                    startX = _self.coordinatesBattle[coordHolder][config.i].center.x,
                    startY = _self.coordinatesBattle[coordHolder].baseCenterY,
                    targetEarth = _self.coordinatesPlayer[coordHolder].elements.earth,
                    targetFire = _self.coordinatesPlayer[coordHolder].elements.fire,
                    controlPointEarth = (coordHolder == "bottomDeck") ? {x: 300, y: 300} : {x: 875, y: 131},
                    controlPointFire = (coordHolder == "bottomDeck") ? {x: 200, y: 300} : {x: 875, y: 131},
                    currentEarth = 0,
                    earthStep = 0.05,
                    currentFire = 0,
                    fireStep = 0.05,
                    radius = 10,
                    ctx = _self._getCtx();

                //show canvas
                _self.canvaShow();

                //draw trail
                function goEarth() {
                    //clear previsious draw
                    ctx.clearRect(0, 0, 1000, 478);
                    //if at earth position  change direction and go to fire
                    if (currentEarth >= 1) {
                        UIAnimator.setResourcesIncr({el: "earth", cnt: config.earth, id: config.uid});
                        goFire();
                        return true;
                    }
                    //calculate position
                    var drawCirle = _self.calculateBezier({x: startX, y: startY}, controlPointEarth, targetEarth, currentEarth);
                    //draw trail point
                    _self.drawCircle(drawCirle, radius);
                    //increment step
                    currentEarth += earthStep;
                    requestAnimFrame(function () {
                        goEarth()
                    });
                }

                function goFire() {
                    //clear previsious draw
                    ctx.clearRect(0, 0, 1000, 478);

                    //if at earth position  change direction and go to fire
                    if (currentFire >= 1) {
                        console.log("FINISHED");
                        UIAnimator.setResourcesIncr({el: "fire", cnt: config.fire, id: config.uid});
                        _self.canvaHide();
                        return true;
                    }
                    //calculate position
                    var drawCirle = _self.calculateBezier(targetEarth, controlPointFire, targetFire, currentFire);
                    //draw trail point
                    _self.drawCircle(drawCirle, radius);
                    //increment step
                    currentFire += fireStep;
                    requestAnimFrame(function () {
                        goFire();
                    });
                }

                goEarth();
            }
        },
        firelordIncrease: function (config, func) {
            var _self = this,
                coordHolder = (config.uid == CardsUI.myID) ? "bottomDeck" : "topDeck",
                startX = _self.coordinatesBattle[coordHolder][config.source].center.x,
                baseCenterY = _self.coordinatesBattle[coordHolder].baseCenterY,
            //move particle to this point on x axis
                targetCenterX = _self.coordinatesBattle[coordHolder][config.target].center.x,
            //left corner of target item  X axis
                targetX = _self.coordinatesBattle[coordHolder][config.target].x,
            //left corner of target item Y axis
                targetYRect = _self.coordinatesBattle[coordHolder].baseY,
                trailStep = 20,
                currentX = startX,
                heightRect = 20,
                maxheight = _self.coordinatesBattle.itemH,
                targetY = baseCenterY,
                ctx = _self._getCtx();

            _self.canvaShow();

            //console.log(coordHolder, startX, )

            function movePointer() {
                ctx.clearRect(0, 0, 1000, 478);

                if (currentX >= targetCenterX) {
                    morphAnimate()
                    return true;
                }
                _self.drawCircle({x: currentX, y: targetY}, 5);
                currentX += trailStep;

                requestAnimFrame(function () {
                    movePointer()
                });
            }

            var step = 14;

            function morphAnimate() {

                if (heightRect >= maxheight) {
                    step *= -1;
                } else if (heightRect < 20) {
                    _self.canvaHide();
                    func();
                    return true;
                }

                ctx.clearRect(0, 0, 1000, 478);
                ctx.globalAlpha = 0.5;
                ctx.beginPath();
                ctx.rect(targetX, targetYRect, 114, heightRect);
                var grd = ctx.createRadialGradient(targetX, targetYRect, 30, targetX + 114, targetYRect + heightRect, heightRect);
                grd.addColorStop(0, 'white');
                grd.addColorStop(0.2, 'red');
                grd.addColorStop(0.6, 'orange');
                grd.addColorStop(1, 'orange');
                ctx.fillStyle = grd;
                ctx.fill();

                //increase rectangle height step by step
                heightRect += step;

                requestAnimFrame(function () {
                    morphAnimate();
                });
            }

            movePointer()
        },
        fireBall: function (config, func) {
            this.canvaShow();
            var _self = this;

            //get canvas
            var ctx = this._getCtx(),
                startPoint,
                battleCord = _self.coordinatesBattle,
                startHolder = (config.uid == CardsUI.myID) ? "bottomDeck" : "topDeck",
                oponentHolder = (config.uid == CardsUI.myID) ? "topDeck" : "bottomDeck",
                baseControlPoint,
                xcontrolOffset;

            //holder for particles for each fireball
            var particlesObj = {1: [], 2: [], 3: [], 4: [], 5: []};
            //var particles = [];
            var W = 1000,
                H = 478;

            baseControlPoint = (startHolder == "bottomDeck") ? {x: 200, y: 200} : {x: 700, y: 200};
            xcontrolOffset = (startHolder == "bottomDeck") ? 100 : -100;

            if (config.source == "hero") {
                startPoint = _self.coordinatesPlayer[startHolder].health;
            } else {
                startPoint = { x: _self.coordinatesBattle[startHolder][config.i].center.x, y: _self.coordinatesBattle[startHolder].baseCenterY};
            }


            //var destinationPoint = {x : 243, y: 160};
            var destinationPoints = {
                "1": { x: battleCord[oponentHolder][1].center.x, y: battleCord[oponentHolder].baseBotY},
                "2": { x: battleCord[oponentHolder][2].center.x, y: battleCord[oponentHolder].baseBotY},
                "3": { x: battleCord[oponentHolder][3].center.x, y: battleCord[oponentHolder].baseBotY},
                "4": { x: battleCord[oponentHolder][4].center.x, y: battleCord[oponentHolder].baseBotY},
                "5": { x: battleCord[oponentHolder][5].center.x, y: battleCord[oponentHolder].baseBotY}
            };

            var currentFires = {
                "1": 0, "2": 0, "3": 0, "4": 0, "5": 0
            };

            var stopAnimations = {
                "1": false, "2": false, "3": false, "4": false, "5": false
            };


            //var stopAnimation = false;
            var fireStep = 0.03;
            //var currentFire = 0;

            //Lets create some particles now
            var particle_count = 15;
            for (var nn = 1; nn < 6; nn++) {
                for (var i = 0; i < particle_count; i++) {
                    particlesObj[nn].push(new particle(startPoint));
                }
            }

            function particle(point, n) {

                this.speed = {x: -2.5 + Math.random() * 5, y: -15 + Math.random() * 10};
                if (point && point.x && point.y) {
                    this.location = {x: point.x, y: point.y};
                } else {
                    //this.location = {x:W / 2, y:H / 2};
                    this.location = _self.calculateBezier(startPoint, {x: baseControlPoint.x + ((n - 1) * xcontrolOffset), y: baseControlPoint.y}, destinationPoints[n], currentFires[n]);
                    //console.log(plocation)
                    currentFires[n] += fireStep;
                    if (currentFires[n] >= 1) {
                        stopAnimations[n] = true;
                    }
                }
                this.radius = 10 + Math.random() * 20;
                this.startRadius = this.radius;
                this.life = 20 + Math.random() * 10;
                this.remaining_life = this.life;
                //colors
                this.r = 245;
                this.g = 61;
                this.b = 0;
            }

            function drawFire() {
                var particles;
                ctx.globalCompositeOperation = "source-over";
                ctx.clearRect(0, 0, W, H);
                ctx.globalCompositeOperation = "lighter";

                if (Object.keys(particlesObj).length == 0) {
                    func();
                    _self.canvaHide();
                    return;
                }
                ;
                //iterate through 5 fireballs
                for (var ind in particlesObj) {
                    particles = particlesObj[ind];
                    for (var i = 0; i < particles.length; i++) {
                        if (particles[i] != null) {
                            var p = particles[i];
                            ctx.beginPath();
                            //changing opacity according to the life.
                            p.opacity = Math.round(p.remaining_life / p.life * 100) / 100
                            //a gradient instead of white fill
                            var gradient = ctx.createRadialGradient(p.location.x, p.location.y, 0, p.location.x, p.location.y, p.radius);
                            gradient.addColorStop(0, "rgba(" + p.r + ", " + p.g + ", " + p.b + ", " + p.opacity + ")");
                            gradient.addColorStop(0.5, "rgba(" + p.r + ", " + p.g + ", " + p.b + ", " + p.opacity + ")");
                            gradient.addColorStop(1, "rgba(" + p.r + ", " + p.g + ", " + p.b + ", 0)");
                            ctx.fillStyle = gradient;
                            ctx.arc(p.location.x, p.location.y, p.radius, 0, Math.PI * 2, false);
                            ctx.fill();

                            //lets move the particles
                            p.remaining_life--;
                            p.radius--;
                            p.location.x += p.speed.x;
                            p.location.y += p.speed.y;


                            //regenerate particles
                            if (p.remaining_life < 0 || p.radius < 0) {
                                if (!stopAnimations[ind]) {
                                    particlesObj[ind][i] = new particle(false, ind);
                                } else {
                                    particlesObj[ind].remove(i);
                                    if (particlesObj[ind].length == 0) {
                                        delete particlesObj[ind];
                                    }
                                }
                            }
                        }
                    }
                }
                requestAnimFrame(function () {
                    drawFire()
                });
            }

            drawFire();
        },
        animateBall: function (startX, startY, controlPoint, target, radius, step, func) {
            var _self = this,
                current = 0,
                ctx = _self._getCtx();
            ;
            //draw trail
            function goHit() {
                //clear previsious draw
                ctx.clearRect(0, 0, 1000, 478);
                //if at earth position  change direction and go to fire
                if (current >= 1) {
                    func();
                    _self.canvaHide();
                    return true;
                }
                //calculate position
                var drawCirle = _self.calculateBezier({x: startX, y: startY}, controlPoint, target, current);
                //draw trail point
                _self.drawCircle(drawCirle, radius);
                //increment step
                current += step;
                requestAnimFrame(function () {
                    goHit();
                });
            }

            goHit();
        },
        flammingArrow: function (config, func) {
            var _self = this,
                coordHolder = (config.uid == CardsUI.myID) ? "bottomDeck" : "topDeck",
                opHolder = (config.uid == CardsUI.myID) ? "topDeck" : "bottomDeck",
                startX = _self.coordinatesPlayer[coordHolder].health.x,
                startY = _self.coordinatesPlayer[coordHolder].health.y,
                target = _self.coordinatesPlayer[opHolder].health,
                controlPoint = {x: 300, y: 300},
                step = 0.05,
                radius = 10;


            //show canvas
            _self.canvaShow();
            _self.animateBall(startX, startY, controlPoint, target, radius, step, func);


        }
    };

    if (typeof window != "undefined") {
        window.requestAnimFrame = (function (callback) {
            return window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.oRequestAnimationFrame || window.msRequestAnimationFrame ||
                function (callback) {
                    window.setTimeout(callback, 1000 / 60);
                };
        })();
    }


    /**
     * Add animation to fight
     * @type {Object}
     */
    var UIAnimator = {
        enabled: (function () {
            if ((typeof window != "undefined")) {
                CanvasAnimation.init();
                return true;
            } else {
                return false;
            }
        })(),
        canvas: null,
        coordinates: {
            top: {
                1: { x: 100, y: 150},
                2: { x: 250, y: 150},
                3: { x: 360, y: 150},
                4: { x: 500, y: 150},
                5: { x: 640, y: 150},
                "hero": {x: 340, y: 15}
            },
            bottom: {
                1: { x: 100, y: 300},
                2: { x: 250, y: 300},
                3: { x: 360, y: 300},
                4: { x: 500, y: 300},
                5: { x: 640, y: 300},
                "hero": {x: 340, y: 370}
            }
        },
        leftOffsets: {
            "1": 10,
            "2": 145,
            "3": 280,
            "4": 415,
            "5": 550
        },
        emptyField: '<li class="deckItem empty"  style="left:%1px" data-cell="%2"><span class="clickHere">Click here to call monster</span></li>',
        animateCanvasPoint: function (from, to, callback) {
        },
        /**
         * Proxy method for all animate methods
         * @param {String} type Name of the actiob from UIAnimator object
         * @param {Object} config Arguments to animate method
         */
        animate: function (type, config) {
            if (this.enabled && this.hasOwnProperty(type)) {
                //console.warn("UIAnimate action -- " + type, config);
                console.log('Event transfered to mediator with args: ', type, config);
                Mediator.trigger(type, config);
                //this[type](config);
            } else {
                //throw Error("wrong animate params");
            }
        },
        /**
         * Use canvas to show cast effect
         * @param type
         * @param config
         * @param func
         */
        effectAnimate: function (type, config, func) {
            if (this.enabled && CanvasAnimation.hasOwnProperty(type)) {
                //console.warn("UIAnimate action -- " + type, config);
                CanvasAnimation[type](config, func);
            } else {
                func();
            }
        },
        startMove: function (data) {

        },
        efreetBackDamage: function () {

        },
        satyrDie: function (data) {

        },
        centaurShot: function (data) {

        },
        elfShoot: function (data) {

        },
        fairyCast: function () {

        },
        paladinKill: function (data) {

        },
        demonConvert: function (data) {

        },
        assasinStrike: function (data) {

        },
        lichStrike: function (data) {

        },
        grimreaperCatch: function (data) {

        },
        vulcanoExplose: function (data) {

        },
        removeBuff: function (buffname) {

        },
        lightningbolt: function (sender) {

        }
    };

    return UIAnimator;
});


