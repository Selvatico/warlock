/**
 * Define Life creatures module
 * @export Life
 */
(function (root, factory) {
    // Node.js
    if (typeof exports !== 'undefined') {
        var mediator = {
            trigger: function () {}
        };
        module.exports = factory(root, require("./UIAnimator"), require("./Creature").Creature, exports, mediator);
    } else if (typeof define === 'function' && define.amd) {
        define(['cls/UIAnimator', 'cls/Creature', 'exports', 'service/mediator'], function (UIAnimator, Creature, exports, Mediator) {
            root.Life = factory(root, UIAnimator, Creature, exports, Mediator);
            return root.Life;
        });
    }
})(this, function (root, UIAnimator, Creature, Life, Mediator) {


    Life = {
        /**
         * Constructors for creatures
         */
        cards: {},

        baseConfig: {}
    };

    Life.baseConfig = {
        magichealer: {
            baseParams: { lives: 12, costs: 3, element: "life", attackPower: 2},
            override: {
                afterCall: function () {
                    this.owner.addPreAction("receiveDamage", "healerProtect");
                },
                afterDie: function () {
                    this.owner.removePreAction("receiveDamage", "healerProtect");
                }
            }
        },
        priest: {
            baseParams: { lives: 9, costs: 4, element: "life", attackPower: 1},
            override: {
                startMove: function () {
                    this.owner.changeResource({life: 2, death: -1});
                    this.owner.addPreAction("beforeCast", "priestDeathCast");
                },
                afterCall: function () {
                    this.owner.removePreAction("beforeCast", "priestDeathCast");
                }
            }
        },
        swordsman: {
            baseParams: { lives: 10, costs: 4, element: "life", attackPower: 3},
            override: {
                defend: function (damage, attackCreature) {
                    attackCreature.defend(2, this);
                    this.ancestor(this, "defend", [damage, attackCreature]);
                },
                strike: function (opCreature, opPlayer) {
                    if (!opCreature || !opCreature.element == "life") {
                        this.ancestor(this, "strike", [opCreature, opPlayer]);
                    }
                }
            }
        },
        apostale: {
            baseParams: { lives: 14, costs: 5, element: "life", attackPower: 4,
                abilityCost: 0, abilityElement: "life", castTarget: "none",
                castFunction: function () {
                    var _self = this,
                        owner = _self.owner,
                        indexBefore = _self.boardIndex;

                    _self.die();
                    owner.cards['banshee'] = "death";
                    owner.callCard("banshee", indexBefore, false, {boardIndex: indexBefore, creatureName: "banshee"});
                    var newCreature = owner.getCreature(indexBefore);
                    newCreature.changeHitPoints(-6, true);
                    newCreature.justCalled = false;
                }
            },
            override: {
                startMove: function () {
                    this.owner.changeResource({life: -2, death: 1});
                }
            }
        },
        pegasus: {
            baseParams: { lives: 15, costs: 6, element: "life", attackPower: 6,
                abilityCost: 2, abilityElement: "life", castTarget: "enemy",
                /**
                 *
                 * @param {Creature} creature
                 */
                castFunction: function (creature) {
                    //holly strike deals 5 damage to target . if it is undead creature pegasus suffers 3 damage
                    creature.defend(5, this);
                    if (creature.element == "death") {
                        this.defend(3, false);
                    }
                }
            },
            override: {
                afterCall: function () {
                    //when sumoned health all creatures for 3 and remove negative buffs
                    this.owner.applyToAll(function pegasusHeal(creature) {
                        creature.changeHitPoints(3).removeBuffs(false, false, -1);
                    });
                }
            }
        },
        monk: {
            baseParams: { lives: 16, costs: 7, element: "life", attackPower: 1,
                abilityCost: 1, abilityElement: "life", castTarget: "enemy",
                /**
                 * @param {Creature} creature
                 */
                castFunction: function (creature) {
                    //enemy creature reduce attack by 2 permanently, not less than 1
                    var currentAttack = creature.attackPower;
                    creature.attackPower = (currentAttack > 3) ? currentAttack - 2 : 1;
                    Mediator.trigger("monk-reduce-attack", creature);
                }
            },
            override: {
                startMove: function () {
                    //heals friendly creatures for 1 each turn and owner +2
                    this.owner.applyToAll(function monkHeal(creature) {
                        creature.changeHitPoints(1);
                    });
                    this.owner.heal(2);
                }
            }
        },
        paladin: {
            baseParams: { lives: 20, costs: 8, element: "life", attackPower: 4,
                abilityCost: 1, abilityElement: "life", castTarget: "custom:death:enemy",
                /**
                 * @param {Creature} creature
                 */
                castFunction: function (creature) {
                    //destroy any undead but suffer 10 damage
                    if (creature.element == "death") {
                        creature.die();
                        this.defend(10, false);
                        UIAnimator.animate("paladinKill", {cr: creature});
                    }
                }
            },
            override: {
                strike: function (opCreature, opPlayer) {
                    //increase attack if undeath
                    if (opCreature.element == "death") {
                        this.attackPower *= 3;
                    }
                    //usual strike
                    this.ancestor(this, "strike", [opCreature, opPlayer]);

                    //back attack to normal
                    if (opCreature.element == "death") {
                        this.attackPower /= 3;
                    }
                }
            }
        },
        valor: {
            baseParams: { lives: 14, costs: 8, element: "life", attackPower: 14,
                abilityCost: 3, abilityElement: "life",
                castFunction: function (player) {

                }
            },
            override: {
                strike: function (opCreature, opPlayer) {
                    if (opCreature) {
                        if (opCreature.element == "death") {
                            this.heal(4, true);
                        }
                        var oldPower = this.attackPower;
                        this.attackPower = Math.ceil(this.attackPower / 2);
                        this.ancestor(this, "strike", [opCreature, opPlayer]);
                        this.attackPower = oldPower;
                    } else {
                        this.ancestor(this, "strike", [opCreature, opPlayer]);
                    }
                },
                startMove: function () {
                    this.changeHitPoints(1);
                }
            }
        },
        hermit: {
            baseParams: { lives: 8, costs: 6, element: "life", attackPower: 2,
                abilityCost: 4, abilityElement: "life", castTarget: "friend",
                castFunction: function (target) {
                    if (target && target.element && !target.magicImune) {
                        target.setCreatureBuff("die", "hermitBack");
                        this.die(false);
                    }
                }
            },
            override: {
                strike: function (opCreature, opPlayer) {
                    if (opCreature && opCreature.level() == 5) {
                        opCreature.die(this);
                    } else {
                        this.ancestor(this, "strike", [opCreature, opPlayer]);
                    }
                }
            }
        },

        unicorn: {
            baseParams: { lives: 25, costs: 9, element: "life", attackPower: 8,
                abilityCost: 2, abilityElement: "life", castTarget: "friend",
                /**
                 * @param {Creature} creature
                 */
                castFunction: function (creature) {
                    creature.removeBuffs(false, false, -1);
                }
            },
            override: {
                afterCall: function (opCreature, opPlayer) {
                    //reduce damage by magic for owner creatures for 50%
                },
                afterDie: function () {
                    this.ancestor(this, "afterDie", []);
                }
            }
        },
        chimera: {
            baseParams: { lives: 30, costs: 13, element: "life", attackPower: 10},
            override: {
                afterCall: function () {
                    this.owner.addPreAction("beforeCast", "himeraBeforeCast");
                    this.owner.addPreAction("callCard", "himeraHealCall");
                },
                afterDie: function () {
                    this.owner.removePreAction("beforeCast", "himeraBeforeCast");
                    this.owner.removePreAction("callCard", "himeraHealCall");
                    this.ancestor(this, "afterDie", []);
                }
            }
        },
        etherial: {
            baseParams: { lives: 9, costs: 7, element: "life", attackPower: 2, physicalImune: true},
            override: {
                afterCall: function () {
                    this.owner.addPreAction("afterCall", "etherialHeal");
                },
                afterDie: function () {
                    this.owner.removePreAction("afterCall", "etherialHeal");
                },
                /**
                 * Can be hurt only by spell and not by creature ability
                 */
                defendMagic: function (magicDamage, type, fromAbility) {
                    if (!fromAbility) {
                        this.ancestor(this, "defendMagic", arguments);
                    }
                }
            }
        },
        //SPELLS
        armorofgod: {
            type: "spell", target: "none", costs: 9, element: "life",
            castSpell: function () {
                var _self = this;
                _self.applyToAll(function armorOfGod(creature) {
                    if (creature.magicImune) return true;
                    creature.setAttackPower((creature.attackPower + 1), true)
                        .changeHitPoints(6)
                        .removeBuffs(false, false, -1);
                    return true;
                });
            }
        },
        /**
         * All owner's creatures $Blessed: ^receive $+1 ^to attack, restore $1 ^point of health every time they are hit. Undead creatures cannot be $blessed ^and suffer $10 ^damage instead.
         */
        bless: {
            type: "spell", target: "none", costs: 5, element: "life",
            castSpell: function () {
                var _self = this;
                _self.applyToAll(function bless(creature) {
                    if (creature.magicImune) return true;
                    if (creature.element != "death") {
                        creature.setAttackPower((creature.attackPower + 1), true);
                    } else {
                        creature.defend(10);
                    }
                });
            }
        },
        lifesacrifice: {
            type: "spell", target: "none", costs: 8, element: "life",
            castSpell: function () {
                var _self = this,
                    lifeRes = _self.getElement("life");

                _self.receiveDamage(lifeRes);
                _self.oponent().receiveDamage((lifeRes * 2));

            }
        },
        godswrath: {
            type: "spell", target: "none", costs: 10, element: "life",
            castSpell: function () {
                var _self = this;
                _self.applyToAllInBattle(function godswrath(monster) {
                    if (monster.magicImune) return true;
                    if (monster.element == "death") {
                        monster.die();
                        _self.changeResource("life", 3)
                            .heal(1);
                    }
                });

            }
        },
        rejuvenation: {
            type: "spell", target: "none", costs: 6, element: "life",
            castSpell: function () {
                var _self = this;
                _self.heal((_self.getElement("life") * 3));
                _self.changeResource("life", -_self.getElement("life"));
            }
        },
        revival: {
            type: "spell", target: "none", costs: 5, element: "life",
            castSpell: function () {
                var _self = this,
                    countToHeal = 0;
                _self.applyToAll(function revival(monster) {
                    if (!monster.magicImune) {
                        monster.changeHitPoints(4);
                    }
                    countToHeal += 2;
                });
                _self.heal(countToHeal);
            }
        }
    };

    Creature.inherit(Life);
    return Life;
});