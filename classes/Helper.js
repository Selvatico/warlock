/**
 * Object with Usefull methods and tricks
 * @type {Object}
 */
module.exports = {
    htmlEnteties : function (msg) {
        return String(msg).replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
    }
};
