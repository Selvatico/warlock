(function () {
    var root = this;

    var GameActions = {
        /**
         * @this Creature
         * @mixin
         * @implements Creature
         */
        CREATURE: {
            /**
             * Injure creature when efreet applies shield to it
             */
            efreetInjure: function () {
                var _self = this;
                _self.defendMagic(2, "fire");
            },
            /**
             * Injure attack creature when we have shild from efreet cast
             * @param {Object} config Config of deffence
             * @param {Creature} config.at Creature which attacked efreet casted
             */
            efreetDefend: function (config) {
                if (config && config.at && config.at.defendMagic) {
                    var creature = config.at,
                        backDamage = Math.ceil(config.damage / 2);
                    creature.defendMagic(backDamage, "fire");
                }
            },
            /**
             * Works when firelord increase creature attack
             * @this Creature
             * @mixines
             */
            firelordIncrease: function () {
                this.defendMagic(2, "fire");
            },
            /**
             * Reduce incoming damage for 5
             * @param {Number} damage
             */
            airShield: function (damage) {
                return (damage >= 2) ? (damage - 2) : 0;
            },
            deathrage: function () {
                this.die();
            },
            dragonPoison: function () {
                this.defend(2, false)
            },
            hermitBack: function (data) {
                var _self = this;
                _self.removeBuffs("die", "hermitBack");
                _self.changeHitPoints(9999);
                return false;
            }
        },
        /***
         * @type {HERO}
         */
        PLAYER: {
            /**
             * Reduce damage received by owner when ice defender present on the field
             * @param data
             */
            iceDefenderDefend: function (data) {
                if (data.dmg) {
                    data.dmg = Math.floor((data.dmg / 2));
                }
            },
            /**
             * Receive all damage directed to owner
             * @param data
             * @return {Object}
             */
            healerProtect: function (data) {
                var _self = this;
                _self.applyToAll(function magicHealerDef(creature) {
                    if (creature.creatureName == "magichealer") {
                        creature.defend(data.dmg);
                    }
                });
                return {dmg: 0};
            },
            /**
             * If sealord on the field and someone call water creature, hurts oponent and
             * heals sealord owner
             */
            sealordCheck: function (data) {
                var _self = this;
                if (data.crd) {
                    if (data.crd.element == "water") {
                        //todo finish this logic
                    }
                }
            },
            /**
             * Heal dryad +2 when owner call earth creature
             * @param data
             */
            dryadHell: function (data) {
                if (data.crd && data.crd.element == "earth") {
                    this.applyToAll(function healdarklord(creature) {
                        if (creature.creatureName == "dryad") {
                            creature.changeHitPoints(2);
                        }
                    });
                }
            },
            lordlightHeal: function () {
                //heals when owenr casts air spell
            },
            /**
             * Heal owner after call other creature for eq of called level
             * @this Hero
             * @param {Object} data
             * @param {Creature} data.crd
             */
            himeraHealCall: function (data) {
                if (data && data.crd) {
                    this.heal(data.crd.costs);
                }
            },

            himeraBeforeCast: function (data) {
                if (data.costs) {
                    //data.costs = Math.ceil(data.costs / 2);
                }
                return data;
            },
            /**
             * Heal owner for called creature leveldark lord for +2 when some creature die
             * @param {Object} data
             * @param {Creature} data.crd
             */
            darkLordHeal: function (data) {
                if (data.crd) {
                    this.applyToAllInBattle(function healdarklord(creature) {
                        if (creature.creatureName == "darklord") {
                            creature.changeHitPoints(2);
                            creature.owner.heal(3);
                        }
                    });

                }
            },
            etherialHeal: function (data) {
                if (data.crd && (data.crd.element == "earth" || data.crd.element == "life")) {
                    this.heal(3);
                }
            },
            /**
             * When some creature die greemreaper give +1 death to owner
             * @param {Object} data
             * @param {Creature} data.crd
             */
            grimreaperCatch: function (data) {
                if (data.crd) {
                    this.changeResource("death", 1);
                    this.heal(data.crd.costs);
                }
            },
            /**
             * When someone die in the battle fairy get +1 to attack
             */
            fairyIncreaseDamage: function () {
                this.applyToAll(function fairyIncrease(creature) {
                    if (creature.creatureName == "fairy") {
                        var newAttackPower = creature.attackPower + 1;
                        creature.setAttackPower(newAttackPower);
                    }
                });
            },
            /**
             * Remove absolute defence from creature
             */
            absoluteDefenceRemove: function () {
                var _self = this;
                _self.applyToAll(function removeAbsoluteDefence(creature) {
                    var immuneBefore = creature.getStoreData("physicalImune"),
                        magicImmuneBefore = creature.getStoreData("magicImune");
                    creature.setConfig({physicalImune: immuneBefore, magicImune: magicImmuneBefore});
                });
                _self.removePreAction("startMove", "absoluteDefenceRemove");
            },
            /**
             * Heal owner after call other creature for eq of called level
             * @this Hero
             * @param {Object} data
             * @param {Creature} data.crd
             */
            darkDruidHealCall: function (data) {
                if (data.crd && data.crd.element != "death") {
                    this.oponent().heal(data.crd.costs);
                }
            },
            paralize: function () {
                var _self = this;
                _self.applyToAll(function paralize(monster) {
                    monster.canFight = false;
                });
                _self.endMove();
                _self.removePreAction("startMove", "paralize");
            },
            endParalize: function () {
                this.removePreAction("startMove", "endParalize");
            },
            priestDeathCast: function (spell) {
                if (spell.element == "death") {
                    this.receiveDamage(3);
                }
                return spell;
            }
        }
    };

    if (typeof exports !== 'undefined') {
        if (typeof module !== 'undefined' && module.exports) {
            exports = module.exports = GameActions;
        }
    } else {
        root.GameActions = GameActions;
    }

    if (typeof define === 'function' && define.amd) {
        define([], function() {
            return GameActions;
        });
    }

}).call(this);
