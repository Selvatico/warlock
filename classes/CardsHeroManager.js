var events = require("events"),
    util = require('util'),
    CardsHero = require("./CardsHero").CardsHero;

/**
 *
 * @type {GameModel}
 */
var gameModel = require("../models/game.model");

/**
 *
 * @type {UsersModel}
 */
var userModel = require("../models/user.model")


/**
 *
 * @constructor
 */
function CardsHeroManager() {

    this.MULTI_GAME_ALLOWED = false;

    this.counter = {pending:0, progress:0, socket:0};
    this.CONNECTION_ERROR = -2;

    /**
     * Games storage  of CardsHero objects
     * @type {Object}
     */
    this.storage = {};


    this.chatMembers = {
        members : {},
        counter : 0,
        lastMsgs: {}
    };

    /**
     * List of running and pending games
     * @type {Object}
     */
    this.storageIds = {pending:{}, running:{}};

    /**
     *
     * @type {Object}
     */
    this.playersMap = {};
    /**
     * Step of inheritance
     */
    events.EventEmitter.call(this);

    util.log("New Manager Created!");
}

util.inherits(CardsHeroManager, events.EventEmitter);

/**
 * Decriment counter connection
 * @param {String} type
 */
CardsHeroManager.prototype.incrCon = function (type) {
    if (this.counter[type]) {
        this.counter++;
    }
};


/**
 * Decriment counter connection
 * @param {String} type
 */
CardsHeroManager.prototype.decrCon = function (type) {
    if (this.counter[type]) {
        this.counter++;
    }
};


/**
 * Add game to manager
 */
CardsHeroManager.prototype.createGameObject = function () {
    var _self = this,
        game = new CardsHero();

    if (!_self.storage[game.uid]) {
        game.manager = this;
        _self.storage[game.uid] = game;
        _self.storageIds.pending[game.uid] = 1;
        _self.counter.pending++;
        return game.uid;
    } else {
        throw Error("Game with same UID in storage already");
    }
};

/**
 * Return counter of games
 * @param {String} [type]
 * @return {Object}
 */
CardsHeroManager.prototype.getCountGames = function (type) {
    return (type) ? this.counter['type'] : this.counter;
};

/**
 *
 * @param {Object} clientSocket Player config
 * @param {Object} clientSocket.memberId Registered player memberId
 * @param {Object} clientSocket.id SocketId to send commet messages
 * @param {Object} [data] Id game to connect
 */
CardsHeroManager.prototype.joinPlayer = function (data, clientSocket) {
    var _self = this,
        playerUid = clientSocket.session.member.uid,
        gameObject,
        gameId = data.id;

    if (playerUid && _self.storage[gameId]) {
        gameObject = _self.storage[gameId];

        //if game in pending status and waiting players
        if (gameObject.inGame(playerUid) && !gameObject.isConnected(playerUid)) {
            return gameObject.setSocketLink(clientSocket.session.member.uid, clientSocket.id).continueGame(playerUid);
        } else {
            return _self.CONNECTION_ERROR;
        }

    } else {
        throw Error("Join Player Error. Wrong Game UID");
    }
    return _self.CONNECTION_ERROR;
};





/**
 * Method to execute when game was found
 * @param {Object} member Member session
 * @param {String} gameId ID of gme to connect
 * @param {Socket} socket Member socket.io object
 */
CardsHeroManager.prototype.registerInGame = function (member, gameId, socket) {
    var _self = this,
        playerUid = member.uid,
        gameObject;

    if (playerUid && _self.storage[gameId]) {
        gameObject = _self.storage[gameId];
        if (gameObject.isPending() && gameObject.hasFreeSeats()) {
            if (!gameObject.inGame(playerUid)) {

                //save temp socket id to send start game command
                gameObject.tempSockets.push(socket.id);

                //acreate and add player to game
                gameObject.addPlayer(member);

                //add member to game map
                if (!_self.playersMap[playerUid]) {
                    _self.playersMap[playerUid] = {};
                }
                _self.playersMap[playerUid][gameId] = 1;
            } else {
                gameObject.tempSockets.push(socket.id);
            }
        }

        if (_self.storageIds.pending[gameId] && !gameObject.hasFreeSeats()) {
            //delete game from pending storage
            delete _self.storageIds.pending[gameId];
            _self.storageIds.running[gameId] = 1;
        }
    }

};

/**
 * Main action to help member find game.
 * In method first we check if we have avalaible free pending games.
 * If found connect member to them, if not create new game object and save member to it
 * @param {Object} [data] emoty data
 * @param {Object} client Client socket with session data
 * @param {Object} client.session Client socket with session data
 */
CardsHeroManager.prototype.findGame = function (data, client) {
    var _self = this,
        gameId;

    if (_self.counter.pending > 0) {
        gameId = Object.keys(_self.storageIds.pending)[0];
    } else {
        gameId = _self.createGameObject();
    }
    if (!!gameId) {
        _self.registerInGame(client.session.member, gameId, client);
    }
};


/**
 * Stop searching the game. Actions indetical to disconnect action
 * @param data
 * @param client
 */
CardsHeroManager.prototype.stopFind = function (data, client) {
     this.disconnect(client);
};


/**
 * Check if have this running game and return it, otherwise FALSE
 * @param gameId
 * @return {Boolean|CardsHero}
 */
CardsHeroManager.prototype.getRunningGame = function (gameId) {
    var _self = this;
    if (gameId && _self.storageIds.running[gameId]) {
        return _self.storage[gameId];
    } else {
        return false;
    }
};


CardsHeroManager.prototype.leavePlayer = function (uid) {};


/**
 * Internal method for getting member game list
 * @param {String} uid Member UID
 * @return {Array} List of game where member now in
 */
CardsHeroManager.prototype._memberGameList = function (uid) {
    var _self = this;
    if (!this.playersMap[uid]) return [];
    if (this.MULTI_GAME_ALLOWED) {
        //@todo add multi games support
    } else {
        return Object.keys(this.playersMap[uid]);
    }
};

/**
 * Fires after socket.io socket disconnect or member stop searching the game
 * First we take the game list where member registered.
 * If member registered in pending game - we destroy the game.
 * If member registered in running game - hold game and give member time to back.
 * @param client
 */
CardsHeroManager.prototype.disconnect = function (client) {
    var _self = this,
        uid = client.session.member.uid,
        gameList = _self._memberGameList(uid);

    gameList.forEach(function (game) {
        if (_self.storage[game]) {
            if (_self.storage[game].countPlayers() == 1) {
                _self.destroyGame(game);
            } else {
                if (_self.storage[game].gameStatus == "run") {
                    _self.storage[game].holdPlayer(uid);
                }
            }
        }
    });
};

/**
 * Proxy method to all game actions
 * @param {String} gameId
 * @param {String} playerUid
 * @param {String} action
 * @param {Object} data
 * @param {Object} [response]
 * @return {Object}
 */
CardsHeroManager.prototype.performGameAction = function (gameId, playerUid, action, data, response) {
    var _self = this,
        player,
        gameObject = _self.getRunningGame(gameId);

    if (gameObject) {
        player = gameObject.getPlayerObj(playerUid);
        if (gameObject.currentTurn != player.color) {
            return {r:false, fr:"wrong:turn"};
        }
        if (player && player[action]) {
            return {r:true, d:player[action].apply(player, data)};
        } else {
            return {r:false, fr:"wrong:session"};
        }
    } else {
        return {r:false, fr:"wrong:session"};
    }
};


/**
 * Convert sent data to raguments array
 * @param {object} data
 * @return {Array}
 */
CardsHeroManager.prototype.dataToParams = function (data) {
    var response = [];
    if (data && typeof data == "object") {
        for (var param in data) {
            if (data.hasOwnProperty(param)) {
                response.push(data[param])
            }
        }
    }
    return response;
};


/**
 * Fires when member on game page but tryied to reconnect
 * due for example bad internet connection etc
 * @param {Object} data Client data
 * @param {Object} data.game Game ID where trying to reconnect
 * @param client
 */
CardsHeroManager.prototype.reconnect = function (data, client) {
    var _self = this,
        uid = client.session.uid,
        gameList = _self._memberGameList(uid);

    if (data.game) {
        _self.joinPlayer(client, {id:data.game});
    } else {
        throw Error("Reconnect without GameID");
    }
};


/**
 * Destroy game objects and set other params. Change rating.
 * @param {Number} gameId Game id to destroy
 */
CardsHeroManager.prototype.destroyGame = function (gameId) {
    var _self = this,
        objectToDestroy,
        gameStatsData,
        playersIds,
        loser;

    console.log("DESTROY LAUNCHED");



    if (_self.storage[gameId]) {

        //get reference copy to game object
        objectToDestroy = _self.storage[gameId];
        playersIds = objectToDestroy.playerInGame;
        loser = (objectToDestroy.winner == playersIds.top) ? playersIds.bottom : playersIds.top;




        //change rating if game was ended
        if (objectToDestroy.gameStatus != "pending") {

            //generate stat data
            gameStatsData = {
                gmId : objectToDestroy.uid,
                pl1  : {
                    un : objectToDestroy.players[playersIds.top].login,
                    id : objectToDestroy.players[playersIds.top].uid
                },
                pl2  : {
                    un : objectToDestroy.players[playersIds.bottom].login,
                    id : objectToDestroy.players[playersIds.bottom].uid
                },
                startTime : objectToDestroy.gameStartTime,
                endTIme   : (new Date()).valueOf(),
                ranksDiff : 4,
                winner    : objectToDestroy.winner

            };
            gameModel.saveGameStats(gameStatsData);


            var usersRating = {};
            usersRating[objectToDestroy.winner] = {rating : 7, wins : 1, lose : 0};
            usersRating[loser] = {rating : -7, wins : 0, lose : 1};
            userModel.changeRating(usersRating);
        }


        //remove pointer copy to game object
        for (var player in objectToDestroy.players) {
            if (objectToDestroy.players.hasOwnProperty(player)) {
                delete objectToDestroy.players[player].boardHolder;
                if (Object.keys(_self.playersMap[player]).length > 0) {
                    delete _self.playersMap[player][gameId];
                } else {
                    delete _self.playersMap[player];
                }
                objectToDestroy.players[player].gameLink = null;
            }
        }

        //delete pointers to Hero objects
        objectToDestroy.players[objectToDestroy.playerInGame.top] = false;
        objectToDestroy.players[objectToDestroy.playerInGame.bottom] = false;

        //delete players holder
        delete objectToDestroy.players;

        if (_self.storage[gameId].gameStatus == "pending") {
            _self.counter.pending--;
        } else {
            _self.counter.progress--;
        }

        //delete game object itself
        delete _self.storage[gameId];

        //decrement counter


        if (_self.storageIds.running[gameId]) {
            delete _self.storageIds.running[gameId];
        }

        if (_self.storageIds.pending[gameId]) {
            delete _self.storageIds.pending[gameId];
        }

    }
};


/**
 *
 * @param action
 * @param data
 * @param target
 */
CardsHeroManager.prototype.socketSend = function (action, data, target) {
    var targetSockets = !util.isArray(target) ? [target] : target;

    if (targetSockets.length > 0) {
        targetSockets.forEach(function (recipient) {
            if (global.io.sockets.sockets.hasOwnProperty(recipient)) {
                global.io.sockets.sockets[recipient].send(JSON.stringify(data));
            }
        });
    }
};

/**
 *
 * @type {CardsHeroManager}
 */
module.exports.manager = new CardsHeroManager();


