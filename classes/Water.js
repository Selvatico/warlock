/**
 /**
 * Define Earth creatures module module
 */
(function (root, factory) {
    // Node.js
    if (typeof exports !== 'undefined') {
        module.exports = factory(root, require("./UIAnimator"), require("./Creature").Creature, exports);
    } else if (typeof define === 'function' && define.amd) {
        define(['cls/UIAnimator', 'cls/Creature', 'exports'], function (UIAnimator, Creature, exports) {
            root.Water = factory(root, UIAnimator, Creature, exports);
            return root.Water;
        });
    }
})(this, function (root, UIAnimator, Creature, Water) {


    Water = {cards: {}};


    Water.baseConfig = {
        /**
         * Nixie config. Water element that can cast fire=>water convertion and make double damadge
         * @type {Object}
         */
        nixie: {
            baseParams: { lives: 10, costs: 4, element: "water", attackPower: 4,
                abilityCost: 1, abilityElement: "fire", castTarget: "none",
                castFunction: function () {
                    this.owner.changeResource({water: 2});
                }
            },
            override: {
                strike: function (opCreature, opPlayer) {
                    var _self = this,
                        attackPower = this.attackPower;
                    if (opCreature.element == "fire") {
                        this.attackPower *= 2;
                    }
                    _self.ancestor(this, "strike", arguments);
                    this.attackPower = attackPower;
                }
            }
        },
        /**
         * Ice defender config. Reduce on 50% damadge taken by owner when defender present on board
         * Receive twice more damadge from fire creatures and fire spells
         * @type Creature.prototype
         */
        iceDefender: {
            baseParams: { lives: 19, costs: 5, element: "water", attackPower: 4 },
            override: {
                defend: function (damadge, attackCreature) {
                    var _self = this;
                    if (attackCreature.element == "fire") {
                        damadge *= 2;
                    }
                    return _self.ancestor(this, "defend", [damadge, attackCreature]);
                },
                defendMagic: function (magicDamage, type) {
                    var _self = this;
                    if (type == "fire") {
                        magicDamage *= 2;
                    }
                    return _self.ancestor(this, "defendMagic", [magicDamage, type]);
                },
                afterCall: function () {
                    this.owner.addPreAction("receiveDamage", "iceDefenderDefend");
                },
                afterDie: function () {
                    this.owner.removePreAction("receiveDamage", "iceDefenderDefend");
                    this.ancestor(this, "afterDie", []);
                }
            }
        },
        /**
         * sealordConfig (ex Poseidon) damadge oponent for -3 and heals +3 owner when someone call water creature.
         * Give immune for poison storm
         * @type {Object}
         */
        sealord: {
            baseParams: { lives: 25, costs: 8, element: "water", attackPower: 3 },
            override: {
                /**
                 * @param {Hero} playerOwner
                 * @param {Hero} playerOponent
                 */
                afterCall: function (playerOwner, playerOponent) {
                    playerOwner.addPreAction("callCard", "sealordCheck");
                    playerOponent.addPreAction("callCard", "sealordCheck");
                },
                afterDie: function () {
                    this.owner.removePreAction("callCard", "sealordCheck");
                    this.owner.oponent().removePreAction("callCard", "sealordCheck");
                    this.ancestor(this, "afterDie", []);
                }
            }
        }
        /**
         * ex leviafan. Strikes oposite creature for 6 and all other
         * oponent creature for 1 damage
         */,
        seadragon: {
            baseParams: { lives: 37, costs: 11, element: "water", attackPower: 6 },
            override: {
                strike: function (opCreature, opPlayer) {
                    var _self = this,
                        tempDamage = _self.attackPower;

                    _self.ancestor(this, "strike", [opCreature, opPlayer]);
                    _self.setConfig({attackPower: 1});
                    _self.owner.oponent().applyToAll(function (creature) {
                        if (creature.boardIndex != opCreature.boardIndex) {
                            _self.ancestor(_self, "strike", [creature, false]);
                        }
                    });
                    _self.setConfig({attackPower: tempDamage});
                }
            }
        },
        elemental: {
            baseParams: { lives: 33, costs: 9, element: "water", attackPower: 1 },
            override: {

                startMove: function () {
                    this.setAttackPower(this.owner.getElement("water"), true);
                }
            }
        },
        /**
         * Strikes oposite and nearest creatures
         */
        gydra: {
            baseParams: { lives: 37, costs: 13, element: "water", attackPower: 6 },
            override: {
                startMove: function () {
                    var _self = this;
                    _self.owner.changeResource("water", -2);
                },
                strike: function (opCreature, opPlayer) {
                    var _self = this,
                        myIndex = _self.boardIndex,
                        oponentHolder = _self.owner.oponent().boardHolder;

                    //strike oposite creature
                    _self.ancestor(this, "strike", [opCreature, opPlayer]);

                    //find indexes and try to strike nearest creatures
                    if (myIndex > 1) {
                        if (myIndex < 5) {
                            _self.ancestor(this, "strike", [oponentHolder[(myIndex - 1)], false]);
                            _self.ancestor(this, "strike", [oponentHolder[(myIndex + 1)], false]);
                        } else {
                            _self.ancestor(this, "strike", [oponentHolder[4], false]);
                        }
                    } else {
                        _self.ancestor(this, "strike", [oponentHolder[2], false]);
                    }
                }
            }
        },
        icewizzard: {
            baseParams: { lives: 22, costs: 11, element: "water", attackPower: 4 },
            override: {
                defend: function (damage, attackCreature) {
                    var _self = this;
                    if (attackCreature.element == "water") {
                        damage = 1;
                    } else if (attackCreature.element == "fire") {
                        damage *= 2;
                    }
                    return _self.ancestor(this, "defend", [damage, attackCreature]);
                },
                startMove: function () {
                    this.owner.changeResource("water", 2);
                }
            }
        },
        /**
         * ex черепаха
         */
        snowbear: {
            baseParams: { lives: 20, costs: 8, element: "water", attackPower: 3 },
            override: {
                defend: function (damage, attackCreature) {
                    var _self = this;
                    damage -= 1;
                    _self.ancestor(this, "defend", [damage, attackCreature]);
                },
                strike: function (opCreature, opPlayer) {
                    if (opCreature.lives < 10) {
                        opCreature.die();
                    } else {
                        this.ancestor(this, "strike", [opCreature, opPlayer]);
                    }
                }
            }
        },
        /**
         * ex ocean lord. Can call mini coctopus and after die call octopus
         */
        /*    octopusman:{
         baseParams:{ lives:25, costs:11, element:"water", attackPower:5,
         abilityCost:0, abilityElement:"water", castTarget : "call",
         castFunction:function (player) {
         }
         },
         override:{
         afterDie:function () {

         }
         }
         },*/
        /**
         * ex ice troll
         */
        icewarrior: {
            baseParams: { lives: 16, costs: 8, element: "water", attackPower: 4,
                abilityCost: 1, abilityElement: "water", castTarget: "none",
                castFunction: function () {
                    var oposite = this.owner.oponent().getCreature(this.boardIndex);
                    if (oposite) {
                        oposite.defendMagic(2, "water", true)
                    }
                }
            },
            override: {
                strike: function (opCreature, opPlayer) {
                    var _self = this,
                        attackBefore = _self.attackPower;
                    if (opCreature.costs > 5) {
                        _self.attackPower *= 2;
                    }
                    this.ancestor(this, "strike", [opCreature, opPlayer]);
                    _self.setAttackPower(attackBefore, false);
                },
                endMove: function () {
                    this.attackPower = 4;
                }
            }
        },
        tryton: {
            baseParams: { lives: 16, costs: 7, element: "water", attackPower: 4,
                abilityCost: 0, abilityElement: "water", castTarget: "none",
                castFunction: function () {
                    var _self = this;
                    _self.owner.applyToAll(function trytonCast(monster) {
                        monster.changeHitPoints(_self.lives);
                    });
                    _self.die();
                }
            },
            override: {
                startMove: function () {
                    this.changeHitPoints(-2);
                    this.setAttackPower(false, true, 1);
                }
            }
        },
        icewall: {
            baseParams: { lives: 16, costs: 6, element: "water", attackPower: 0,
                abilityCost: 0, abilityElement: "water", castTarget: "friend",
                castFunction: function (player) {
                    //todo add cal of octopus
                }
            },
            override: {
                strike: function () {
                },
                defend: function (damage, attackCreature) {
                    if (attackCreature) {
                        attackCreature.defend(2, this);
                    }
                },
                startMove: function () {
                    this.defend(2, false);
                }
            }
        },
        //SPELLS
        acidstorm: {
            type: "spell", target: "none", costs: 9, element: "water",
            /**
             * @this {Hero}
             * @implements Hero
             * @mixin
             */
            castSpell: function () {
                var _self = this;
                if (_self.creatureInField("sealord").count == 0) {
                    _self.applyToAll(function acidStormCast(creature) {
                        creature.defendMagic(16, "water");
                    });
                }

                if (_self.oponent().creatureInField("sealord").count == 0) {
                    _self.oponent().applyToAll(function acidStormCast(creature) {
                        creature.defendMagic(16, "water");
                    });
                }


            }
        },
        icebolt: {
            type: "spell", target: "none", costs: 7, element: "water",
            castSpell: function () {
                var _self = this,
                    damage = 10 + Math.ceil(_self.getElement("water") / 2);
                _self.oponent().receiveDamage(damage);
                _self.receiveDamage(6);
            }
        },
        paralize: {
            type: "spell", target: "none", costs: 10, element: "water",
            castSpell: function () {
                var _self = this;
                _self.oponent().addPreAction("startMove", "paralize");
                //_self.oponent().addPreAction("endMove", "endParalize");
            }
        },
        tsunami: {
            type: "spell", target: "none", costs: 7, element: "water",
            castSpell: function () {
                var _self = this;
                _self.oponent().applyToAll(function tsunami(monster) {
                    monster.defendMagic(monster.level(), "water");
                });
            }
        },
        solitude: {
            type: "spell", target: "none", costs: 4, element: "water",
            castSpell: function () {
                var _self = this,
                    count = 0,
                    index = 0;
                _self.applyToAll(function solitude(monster) {
                    count++;
                    index = monster.boardIndex;
                });

                if (count === 1) {
                    _self.getCreature(index).setAttackPower(false, true, 5);
                }
            }
        },
        wavepower: {
            type: "spell", target: "none", costs: 7, element: "water",
            castSpell: function () {
                var _self = this,
                    indexFirst = 0,
                    indexSecond = 0;
                _self.oponent().applyToAll(function wavepower(monster) {
                    if (monster.level() < 6) {
                        if (indexFirst == 0) {
                            indexFirst = monster.boardIndex;
                        }
                        if (indexSecond == 0) {
                            indexSecond = monster.boardIndex;
                        }
                    }
                });
                if (indexFirst > 0 && indexFirst < 6) {
                    _self.oponent().getCreature(indexFirst).die();
                }

                if (indexSecond > 0 && indexSecond < 6) {
                    _self.oponent().getCreature(indexSecond).die();
                }
            }
        },
        seastrike: {
            type: "spell", target: "none", costs: 3, element: "water",
            castSpell: function () {
                var _self = this;
                _self.oponent().applyToAll(function seastrike(monster) {
                    monster.defend((monster.attackPower - 1), false);
                });
            }
        }

    };

    Creature.inherit(Water);
    return Water;

});


