/**
 * Define Earth creatures module module
 */
(function (root, factory) {
    // Node.js
    if (typeof exports !== 'undefined') {
        module.exports = factory(root, require("./UIAnimator"), require("./Creature").Creature, exports);
    } else if (typeof define === 'function' && define.amd) {
        define(['cls/UIAnimator', 'cls/Creature', 'exports'], function (UIAnimator, Creature, exports) {
            root.Earth = factory(root, UIAnimator, Creature, exports);
            return root.Earth;
        });
    }
})(this, function (root, UIAnimator, Creature, Earth) {

    Earth = {cards: {}};

    Earth.baseConfig = {
        satyr: {
            baseParams: { lives: 10, costs: 2, element: "earth", attackPower: 3,
                abilityCost: 0, abilityElement: "earth", castTarget: "none",
                /**
                 * @param {Hero} player
                 */
                castFunction: function (player) {
                    var _self = this,
                        oponent = _self.owner.oponent();
                    //strike oposite for 5
                    if (oponent.boardHolder[_self.boardIndex]) {
                        oponent.boardHolder[_self.boardIndex].defend(5, _self);
                    } else {
                        oponent.receiveDamage(5);
                    }

                    //animate this
                    UIAnimator.animate("satyrDie", {satyrPosition: _self.boardIndex, position: _self.owner.color});

                    //die
                    _self.die();
                }
            },
            override: {
                startMove: function () {
                    this.owner.changeResource("earth", 1);
                }
            }
        },
        forestspirit: {
            baseParams: { lives: 3, costs: 3, element: "earth", attackPower: 2,
                abilityCost: 2, abilityElement: "earth", castTarget: "none",
                castFunction: function () {
                    this.owner.heal(5);
                }
            },
            override: {
                defend: function (damadge, attackCreature) {
                    var _self = this;
                    return _self.ancestor(this, "defend", [1, attackCreature]);
                }
            }
        },
        dryad: {
            baseParams: { lives: 12, costs: 4, element: "earth", attackPower: 4},
            override: {
                afterCall: function () {
                    var _self = this,
                        myIndex = _self.boardIndex,
                        indexesToChange = [];

                    if (myIndex > 1) {
                        if (myIndex < 5) {
                            indexesToChange.push((myIndex - 1), (myIndex + 1));
                        }
                        else {
                            indexesToChange.push(4);
                        }
                    } else {
                        indexesToChange.push(2);
                    }

                    _self.owner.applyToAll(function dryadIncrease(creature) {
                        if (indexesToChange.indexOf(Number(creature.boardIndex)) != -1) {
                            var increaseCount = (creature.element == "earth") ? 2 : 1;
                            creature.setAttackPower((creature.attackPower += increaseCount), true);
                        }
                    });
                    _self.owner.addPreAction("callCard", "dryadHell");
                },
                afterDie: function () {
                    var _self = this,
                        myIndex = _self.boardIndex,
                        indexesToChange = [];

                    if (myIndex > 1) {
                        if (myIndex < 5) {
                            indexesToChange.push((myIndex - 1), (myIndex + 1));
                        }
                        else {
                            indexesToChange.push(4);
                        }
                    } else {
                        indexesToChange.push(2);
                    }

                    _self.owner.applyToAll(function dryadIncrease(creature) {
                        if (indexesToChange.indexOf(Number(creature.boardIndex)) != -1) {
                            var increaseCount = (creature.element == "earth") ? 2 : 1;
                            creature.setAttackPower(((creature.attackPower > increaseCount) ? creature.attackPower - increaseCount : 1), true);
                        }
                    });
                    this.ancestor(this, "afterDie", []);
                }
            }
        },
        dispercer: {
            baseParams: { lives: 13, costs: 5, element: "earth", attackPower: 1},
            override: {
                afterDie: function () {
                    this.owner.increaseElememnts();
                    this.ancestor(this, "afterDie", []);
                }
            }
        },
        golem: {
            baseParams: { lives: 15, costs: 5, element: "earth", attackPower: 4},
            override: {
                startMove: function () {
                    var _self = this;
                    if (_self.owner.canPay("earth", 4)) {
                        this.changeHitPoints(3);
                    } else {
                        _self.defend(3, false);
                    }
                }
            }
        },
        /**
         * Block all damage less than 4
         */
        kobold: {
            baseParams: { lives: 11, costs: 6, element: "earth", attackPower: 5},
            override: {
                defend: function (damage, attackCreature) {
                    if (damage > 4) {
                        return this.ancestor(this, "defend", [damage, attackCreature]);
                    }
                }
            }
        },
        /**
         * Can strike right after call
         * @this Creature
         */
        centaur: {
            baseParams: { lives: 14, costs: 6, element: "earth", attackPower: 5,
                abilityCost: 1, abilityElement: "earth", castTarget: "none",
                castFunction: function () {
                    var _self = this;
                    UIAnimator.animate("centaurShot", {self: _self});
                    _self.owner.oponent().receiveDamage(3);

                }
            },
            /**
             * @augments Creature
             * @extends Creature
             * @this Creature
             */
            override: {
                /**
                 * @this Creature
                 */
                afterCall: function () {
                    this.justCalled = false;
                }
            }
        },
        /**
         * Can cast attack of all creatures for 1, but loose 3 hitpoints.
         * When attack oposite creature, attack
         */
        ent: {
            baseParams: { lives: 22, costs: 7, element: "earth", attackPower: 3,
                abilityCost: 0, abilityElement: "earth", castTarget: "none",
                castFunction: function () {
                    var _self = this;
                    _self.owner.oponent().applyToAll(function entStrike(creature) {
                        creature.defend(1, _self);
                    });
                    _self.defend(3, false);
                }
            },
            override: {
                /**
                 * @param {Creature} opCreature
                 * @param {Hero} opPlayer
                 */
                strike: function (opCreature, opPlayer) {
                    opPlayer.receiveDamage(this.attackPower);
                    this.ancestor(this, "strike", [opCreature, opPlayer]);
                }
            }
        },
        /**
         * Can't fight? but can shoot any creature and gives to owner +1 earth each move
         */
        elf: {
            baseParams: { lives: 18, costs: 7, element: "earth", attackPower: 0,
                abilityCost: 0, abilityElement: "earth", castTarget: "enemy",
                /**
                 * Make a shot
                 * @param {Creature} target
                 */
                castFunction: function (target) {
                    var _self = this;
                    UIAnimator.animate("elfShoot", {target: target, elf: _self});
                    target.defend(6, this);
                }
            },
            override: {
                setAttackPower: function () {
                    return true;
                }

            }
        },
        /**
         * Can't fight? but can shoot any creature and gives to owner +1 earth each move
         */
        spider: {
            baseParams: { lives: 16, costs: 6, element: "earth", attackPower: 3,
                abilityCost: 2, abilityElement: "earth", castTarget: "enemy",
                /**
                 * Make a shot
                 * @param {Creature} target
                 */
                castFunction: function (target) {
                    target.canFight = false;
                }
            },
            override: {
                afterCall: function () {
                    var _self = this,
                        addAttack = false;

                    _self.owner.applyToAll(function spiderCHeck(monster) {
                        if (monster.element == "earth" && monster.boardIndex != _self.boardIndex) {
                            addAttack = true;
                        }
                    });

                    if (addAttack) {
                        _self.setAttackPower(false, true, 3);
                    }
                }
            }
        },
        /**
         * ex echidna
         */
        aciddragon: {
            baseParams: { lives: 26, costs: 10, element: "earth", attackPower: 7},
            override: {
                strike: function (oponentCreature, oponentPlayer) {
                    if (oponentCreature) {
                        oponentCreature.setCreatureBuff("startMove", "dragonPoison");
                    }
                    this.ancestor(this, "strike", arguments);
                }
            }
        },
        /**
         * ex elemental
         * @this Creature
         * @mixin
         */
        woodbeast: {
            baseParams: { lives: 45, costs: 13, element: "earth", attackPower: 1,
                abilityCost: 0, abilityElement: "earth", castTarget: "friend",
                /**
                 * @param {Creature} target
                 */
                castFunction: function (target) {
                    target.physicalReduction += 1;
                }
            },
            override: {
                startMove: function () {
                    var _self = this,
                        earthCnt = (_self.owner.getElement("earth") - 3);
                    _self.setAttackPower(((earthCnt > 1) ? earthCnt : 1), true)
                },
                defendMagic: function (magicDamage, type) {
                    if (type == "fire") {
                        magicDamage += 10;
                    }
                    this.ancestor(this, "defendMagic", [magicDamage, type]);
                }
            }
        },
        tigerspirit: {
            baseParams: { lives: 20, costs: 8, element: "earth", attackPower: 4,
                abilityCost: 0, abilityElement: "earth", castTarget: "enemy",
                /**
                 * @param {Creature} target
                 */
                castFunction: function (target) {
                    if (target && target.element) {
                        if (target.lives <= 5) {
                            target.die();
                        }
                    }
                }
            },
            override: {
                strike: function (opCreature, opPlayer) {
                    if (opCreature && opCreature.attackPower > 2) {
                        opCreature.setAttackPower(false, true, -1);
                    }
                    this.ancestor(this, "strike", [opCreature, opPlayer]);
                }
            }
        },
        enchanter: {
            baseParams: { lives: 12, costs: 7, element: "earth", attackPower: 5,
                abilityCost: 2, abilityElement: "earth", castTarget: "friend",
                castFunction: function () {
                    var _self = this;
                    _self.changeHitPoints(4);
                }
            },
            override: {
                startMove: function () {
                    var _self = this;
                    _self.owner.applyToAll(function enchanterHeal(monster) {
                        if (monster.element == "earth" && monster.boardIndex != _self.boardIndex) {
                            monster.changeHitPoints(1);
                        }
                    });
                },
                defend: function (damadge, attackCreature) {
                    var _self = this;
                    _self.ancestor(this, "defend", [damadge, attackCreature]);
                    if (_self.lives == 1) {
                        _self.die();
                        _self.owner.applyToAll(function enchanterDieHeal(monster) {
                            monster.changeHitPoints(5);
                        });
                    }

                }
            }
        },

        //SPELLS
        absolutedefence: {
            type: "spell", target: "none", costs: 7, element: "earth",
            castSpell: function () {
                var _self = this;
                _self.applyToAll(function castAbsoluteDefence(creature) {
                    creature.saveInStore("physicalImune", creature.physicalImune)
                        .saveInStore("magicImune", creature.magicImune);

                    creature.setConfig({physicalImune: true, magicImune: true});
                });
                _self.addPreAction("startMove", "absoluteDefenceRemove");
            }
        },
        /**
         * Increases owner's $Earth ^by $8, ^decreasing other elements as much as $1 ^of each.
         */
        earthchant: {
            type: "spell", target: "none", costs: 5, element: "earth",
            castSpell: function () {
                var _self = this;
                _self.changeResource({air: -1, water: -1, fire: -1, earth: 8, life: -1, death: -1});
            }
        },
        earthquake: {
            type: "spell", target: "none", costs: 10, element: "earth",
            castSpell: function () {
                var _self = this,
                    earthShake = function earthShakeFunc(creature) {
                        creature.defendMagic(15, "earth");
                    };

                if (_self.canPay("earth", 3)) {
                    _self.oponent().applyToAll(earthShake);
                } else {
                    _self.applyToAllInBattle(earthShake);
                }
            }
        },
        /**
         * Permanently increases attack of your creatures by $2.
         */
        empower: {
            type: "spell", target: "none", costs: 4, element: "earth",
            castSpell: function () {
                var _self = this;
                _self.applyToAll(function empower(monster) {
                    if (monster.magicImune) return true;
                    monster.setAttackPower(false, true, 2);
                    return true;
                });
            }
        },
        /**
         * Deals damage to each enemy creature equal to half of it's remaining health. No effect to Earth creatures.
         */
        forceofnature: {
            type: "spell", target: "none", costs: 8, element: "earth",
            castSpell: function () {
                var _self = this;
                _self.oponent().applyToAll(function forceofnature(monster) {
                    if (monster.element != "earth") {
                        monster.defendMagic((Math.ceil(monster.lives / 2)), "earth");
                    }
                });
            }
        },
        polymorph: {
            type: "spell", target: "none", costs: 10, element: "earth",
            castSpell: function () {
                var _self = this,
                    oponent = _self.oponent();
                oponent.applyToAll(function polymorph(monster) {
                    if (monster.magicImune) return true;
                    var dataBefore;
                    if (monster.level() < 8) {
                        dataBefore = {index: monster.boardIndex, health: monster.lives, attackPower: monster.attackPower};
                        monster.die();
                        oponent.cards['satyr'] = "earth";
                        oponent.callCard("satyr", dataBefore.index, false, {boardIndex: dataBefore.index, creatureName: "satyr"});
                        var newCreature = oponent.getCreature(dataBefore.index)
                        newCreature.setAttackPower(dataBefore.attackPower, true)
                        newCreature.changeHitPoints((dataBefore.health - 10), true);
                    }
                    return true;
                });
            }
        },
        quicksands: {
            type: "spell", target: "none", costs: 6, element: "earth",
            castSpell: function () {
                var _self = this,
                    oponent = _self.oponent();
                oponent.applyToAll(function quicksands(monster) {
                    if (monster.magicImune) return true;
                    if (monster.level() < 5) {
                        monster.die();
                    }
                    return true;
                });
            }
        },
        restructure: {
            type: "spell", target: "none", costs: 6, element: "earth",
            castSpell: function () {
                var _self = this;
                _self.applyToAll(function quicksands(monster) {
                    if (monster.magicImune) return true;
                    monster.maxHealthPoint += 3;
                    monster.changeHitPoints(6);
                });
            }
        }
    };

    Creature.inherit(Earth);

    return Earth;


});
