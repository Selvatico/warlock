/**
 * Define Death creatures module module
 */
(function (root, factory) {
    // Node.js
    if (typeof exports !== 'undefined') {
        module.exports = factory(root, require("./UIAnimator"), require("./Creature").Creature, exports);
    } else if (typeof define === 'function' && define.amd) {
        define(['cls/UIAnimator', 'cls/Creature', 'exports'], function (UIAnimator, Creature, exports) {
            root.Death = factory(root, UIAnimator, Creature, exports);
            return root.Death;
        });
    }
})(this, function (root, UIAnimator, Creature, Death) {

    Death = {cards: {}};

    Death.baseConfig = {
        skeleton: {
            baseParams: { lives: 8, costs: 3, element: "death", attackPower: 4},

            override: {
                startMove: function () {
                    this.changeHitPoints(2);
                },
                /**
                 * @param {Creature} opCreature
                 * @param {Hero} ophero
                 */
                strike: function (opCreature, ophero) {
                    if (opCreature.lives < 7) {
                        this.attackPower += 2;
                    }
                    this.ancestor(this, "strike", arguments);
                    if (opCreature.lives < 7) {
                        this.attackPower -= 2;
                    }
                }
            }
        },
        ghost: {
            baseParams: { lives: 10, costs: 4, element: "death", attackPower: 4,
                abilityCost: 0, abilityElement: "death", castTarget: "none",
                castFunction: function () {
                    this.owner.receiveDamage(5);
                    this.owner.changeResource("death", 1);
                }
            },
            override: {
                defend: function (damage, attackCreature) {
                    var halfDamage = Math.ceil(damage);
                    this.owner.receiveDamage(halfDamage);
                    this.ancestor(this, "defend", [halfDamage, attackCreature]);
                },
                defendMagic: function (magicDamage, type) {
                    this.ancestor(this, "defendMagic", [(magicDamage * 2), type]);
                }
            }
        },
        zombie: {
            baseParams: { lives: 11, costs: 4, element: "death", attackPower: 3},
            override: {
                afterKill: function () {
                    this.maxHealthPoint += 2;
                    this.changeHitPoints(999);
                }
            }
        },
        shadow: {
            baseParams: { lives: 10, costs: 4, element: "death", attackPower: 4,
                abilityCost: 0, abilityElement: "death", castTarget: "friend",
                castFunction: function (creature) {
                    creature.maxHealthPoint += 10;
                    creature.changeHitPoints(10);
                }
            },
            override: {
                defend: function (damage, attackCreature) {
                    var partDamage = (attackCreature.element == "life") ? damage += 5 : Math.ceil(damage / 2);
                    this.ancestor(this, "defend", [partDamage, attackCreature]);
                }
            }
        },
        evilsoccer: {
            baseParams: { lives: 14, costs: 6, element: "death", attackPower: 5,
                abilityCost: 1, abilityElement: "death", castTarget: "none",
                castFunction: function () {
                    var _self = this,
                        oponentCreature = _self.owner.oponent().getCreature(_self.boardIndex);
                    if (oponentCreature) {
                        oponentCreature.defendMagic(2, "death", true)
                    }
                }
            },
            override: {
                strike: function (opCreature, ophero) {
                    if (opCreature) {
                        opCreature.defendMagic(5, "death", true);
                    } else {
                        ophero.receiveDamage(5);
                    }
                },
                defendMagic: function (magicDamage, type) {

                }
            }
        },
        werewolf: {
            baseParams: { lives: 16, costs: 6, element: "death", attackPower: 6,
                abilityCost: 3, abilityElement: "death", castTarget: "none",
                castFunction: function () {
                    var _self = this;
                    _self.saveInStore("attackBefore", _self.attackPower).setAttackPower((this.attackPower *= 2), true);
                }
            },
            override: {
                startMove: function () {
                    var _self = this,
                        attackBefore = _self.getStoreData("attackBefore"),
                        attackToSet = (attackBefore) ? attackBefore : 6;

                    _self.setAttackPower(attackToSet, true);
                },
                die: function () {
                    var ind = this.boardIndex,
                        owner = this.owner;

                    //wervolf shoud to die
                    this.ancestor(this, "afterDie", []);
                    this.ancestor(this, "die", arguments);

                    //ghost shoud be born
                    owner.cards["ghost"] = "death";
                    owner.callCard("ghost", ind, false, {boardIndex: ind, creatureName: "ghost"});
                    owner.getCreature(ind).setConfig({justCalled: false});

                }
            }
        },
        banshee: {
            baseParams: { lives: 12, costs: 7, element: "death", attackPower: 5},
            override: {
                afterCall: function () {
                    this.owner.oponent().receiveDamage(8);
                },
                /**
                 * @param {Creature} opCreature
                 * @param {Hero} opPlayer
                 */
                strike: function (opCreature, opPlayer) {
                    if (!opCreature) {
                        opPlayer.receiveDamage((this.attackPower + 10));
                        this.die();
                    } else {
                        this.ancestor(this, "strike", arguments);
                    }
                }
            }
        },
        assasin: {
            baseParams: { lives: 17, costs: 7, element: "death", attackPower: 3,
                abilityCost: 4, abilityElement: "death", castTarget: "enemy",
                castFunction: function (creature) {
                    if (creature.lives < 10) {
                        creature.die();
                    }
                }
            },
            override: {
                strike: function (opCreature, ophero) {
                    this.attackPower += (opCreature) ? 4 : 0;
                    this.ancestor(this, "strike", arguments);
                    this.attackPower += (opCreature) ? -4 : 0;
                    UIAnimator.animate("assasinStrike", {target: opCreature});
                }
            }
        },
        darklord: {
            baseParams: { lives: 22, costs: 9, element: "death", attackPower: 6,
                abilityCost: 1, abilityElement: "death", castTarget: "enemy",
                castFunction: function (creature) {
                    //todo add steal buffs
                }
            },
            override: {
                afterCall: function () {
                    var owner = this.owner;
                    owner.addPreAction("afterDie", "darkLordHeal");
                    owner.oponent().addPreAction("afterDie", "darkLordHeal");
                },
                afterDie: function () {
                    var owner = this.owner;
                    owner.removePreAction("afterDie", "darkLordHeal");
                    owner.oponent().removePreAction("afterDie", "darkLordHeal");
                    this.ancestor(this, "afterDie", []);
                }
            }
        },
        vampire: {
            baseParams: { lives: 16, costs: 9, element: "death", attackPower: 6},
            override: {
                strike: function (opCreature, ophero) {
                    var healAmount = Math.ceil(this.attackPower / 2),
                        _self = this;
                    _self.ancestor(this, "strike", arguments);
                    if (opCreature && !opCreature.physicalImune) {
                        _self.changeHitPoints(healAmount);
                    }
                },
                afterCall: function () {
                    this.maxHealthPoint = 30;
                }
            }
        },
        leach: {
            baseParams: { lives: 18, costs: 10, element: "death", attackPower: 7,
                abilityCost: 5, abilityElement: "death", castTarget: "none",
                castFunction: function () {
                    var owner = this.owner;
                    owner.oponent().receiveDamage(7);

                    if (!owner.canPay("death", 6)) {
                        owner.receiveDamage(10);
                    }
                }
            },
            override: {
                afterCall: function () {
                    var _self = this,
                        myIndex = parseInt(_self.boardIndex, 10),
                        indexesToChange = [myIndex];

                    if (myIndex > 1) {
                        if (myIndex < 5) {
                            indexesToChange.push((myIndex - 1), (myIndex + 1));
                        }
                        else {
                            indexesToChange.push(4);
                        }
                    } else {
                        indexesToChange.push(2);
                    }

                    _self.owner.oponent().applyToAll(function leachStrike(creature) {
                        if (indexesToChange.indexOf(Number(creature.boardIndex)) != -1) {
                            creature.defendMagic(10, "death", true);
                        }
                    });
                },
                strike: function (opCreature, opHero) {
                    this.attackPower += (opCreature.element == "life") ? 5 : 0;
                    this.ancestor(this, "strike", arguments);
                    this.attackPower += (opCreature.element == "life") ? -5 : 0;
                    UIAnimator.animate("lichStrike", {target: opCreature});
                }
            }
        },
        grimreaper: {
            baseParams: { lives: 25, costs: 11, element: "death", attackPower: 7,
                abilityCost: 3, abilityElement: "death", castTarget: "enemy",
                castFunction: function (creature) {
                    if (creature.costs <= 3) {
                        creature.die();
                    }
                }
            },
            override: {
                afterCall: function () {
                    this.owner.addPreAction("afterDie", "grimreaperCatch");
                },
                afterDie: function () {
                    this.owner.removePreAction("afterDie", "grimreaperCatch");
                    this.ancestor(this, "afterDie", []);
                }
            }
        },
        darkdruid: {
            baseParams: { lives: 29, costs: 11, element: "death", attackPower: 6},
            override: {
                afterCall: function () {
                    this.owner.addPreAction("afterDie", "darkDruidHealCall");
                },
                afterDie: function () {
                    this.owner.removePreAction("afterDie", "darkDruidHealCall");
                    this.ancestor(this, "afterDie", []);
                }
            }
        }/*,
         necromancer : {
         baseParams:{ lives:20, costs:8, element:"death", attackPower:3,
         abilityCost:2, abilityElement:"death", castTarget:"call",
         castFunction:function (index) {

         }
         },
         override:{
         afterCall:function () {
         this.owner.addPreAction("afterDie", "grimreaperCatch");
         },
         afterDie : function () {
         this.owner.removePreAction("afterDie", "grimreaperCatch");
         this.ancestor(this, "afterDie", []);
         }
         }
         }*/,
        //SPELLS
        chaosvortex: {
            type: "spell", target: "none", costs: 13, element: "death",
            castSpell: function () {
                var _self = this;
                _self.applyToAllInBattle(function chaosvortex(creature) {
                    creature.die();
                    _self.changeResource("death", 1);
                });
            }
        },
        /**
         * All living creatures suffer $13 ^damage. All undead creatures heal for $5.
         */
        coverofdarkness: {
            type: "spell", target: "none", costs: 11, element: "death",
            castSpell: function () {
                var _self = this;
                _self.applyToAllInBattle(function chaosvortex(creature) {
                    if (creature.element == "death") {
                        creature.heal(5);
                    } else {
                        creature.defendMagic(13, "death");
                    }

                });
            }
        },
        /**
         * Reduces all enemy elements by $1.
         */
        curse: {
            type: "spell", target: "none", costs: 4, element: "death",
            castSpell: function () {
                var _self = this;
                _self.oponent().changeResource({air: -1, water: -1, fire: -1, earth: -1, life: -1, death: -1});
            }
        },
        deathrage: {
            type: "spell", target: "none", costs: 8, element: "death",
            castSpell: function () {
                var _self = this;
                _self.applyToAll(function deathrage(creature) {
                    creature.setAttackPower((creature.attackPower * 2), true)
                        .setCreatureBuff("startMove", "deathrage");
                });
            }
        },
        unholyword: {
            type: "spell", target: "none", costs: 9, element: "death",
            castSpell: function () {
                var _self = this;
                _self.applyToAllInBattle(function unholyword(monster) {
                    var owner = monster.owner;
                    if (monster.level() > 6 && monster.element != "death") {
                        monster.die();
                        owner.cards['zombie'] = "death";
                        owner.callCard("zombie", monster.boardIndex);
                    }
                });
            }
        },
        /**
         * If owner's Death less than 8, steals 5 health from enemy player. Otherwise steals Death + 5.
         */
        steallife: {
            type: "spell", target: "none", costs: 6, element: "death",
            castSpell: function () {
                var _self = this,
                    amounOfSteal,
                    deathCount = _self.getElement("death");
                if (deathCount < 2) {
                    amounOfSteal = 5;
                } else {
                    amounOfSteal = (deathCount + 6) + 5;
                }
                _self.heal(amounOfSteal).oponent().receiveDamage(amounOfSteal);

            }
        },
        totalweakness: {
            type: "spell", target: "none", costs: 8, element: "death",
            castSpell: function () {
                var _self = this;
                _self.oponent().applyToAll(function useWickness(monster) {
                    if (!monster.magicImune) {
                        var originalAttack = monster.attackPower;
                        monster.setAttackPower(Math.floor(originalAttack / 2), true);
                    }
                });
            }
        },
        undeadstrike: {
            type: "spell", target: "none", costs: 4, element: "death",
            castSpell: function () {
                var _self = this,
                    oponent = _self.oponent();
                _self.applyToAll(function useWickness(monster) {
                    if (monster.element == "death") {
                        monster.strike(oponent.getCreature(monster.boardIndex), oponent);
                        UIAnimator.animate("strike", {id: _self.uid, ind: monster.boardIndex});
                    }
                });
            }
        },
        damnation: {
            type: "spell", target: "none", costs: 6, element: "death",
            castSpell: function () {
                var _self = this;
                _self.oponent().applyToAll(function damnation(monster) {
                    monster.physicalReduction += -2;
                });
            }
        }

    };

    Creature.inherit(Death);

    return Death;


});
