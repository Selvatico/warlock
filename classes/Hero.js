
(function (root, factory) {
    // Node.js
    if (typeof exports !== 'undefined') {

        var elements = {
            water  : require("./Water"),
            fire   : require("./Fire"),
            earth  : require("./Earth"),
            air    : require("./Air"),
            life   : require("./Life"),
            death  : require("./Death")
        },
        mediator = {
            trigger: function () {}
        };

        module.exports.Hero = factory(root, require("./GameActions"), elements, mediator);

    } else if (typeof define === 'function' && define.amd) {
        //client side require.js definition
        define(
            [
                'cls/GameActions',
                'cls/Water',
                'cls/Fire',
                'cls/Air',
                'cls/Earth',
                'cls/Life',
                'cls/Death',
                'service/mediator'
            ], function (GameActions, Water, Fire, Air, Earth, Life, Death, Mediator) {

            var elements = {
                water  : Water,
                fire   : Fire,
                earth  : Earth,
                air    : Air,
                life   : Life,
                death  : Death
            };

            root.Hero = factory(root, GameActions, elements, Mediator);
            return root.Hero;
        });
    }
})(this, function (root, GameActions, Elements, Mediator) {

    var CardsFactory = {
        /**
         * Cards constructor holder
         */
        water  : Elements.water,
        fire   : Elements.fire,
        earth  : Elements.earth,
        air    : Elements.air,
        life   : Elements.life,
        death  : Elements.death,
        /**
         * Return instance of new creatures
         * @param {String} name Name creature
         * @param {String} element Name of element
         * @return {Creature}
         */
        getNewCard: function (name, element) {
            if (this[element] && this[element].cards && this[element].cards[name]) {
                return new this[element].cards[name]();
            } else {
                throw Error("Wrong card name and element " + name + " " + element);
            }
        },
        /**
         * Get price of creature ro check before call
         * @param {String} name Name creature
         * @param {String} element Name of element
         * @return {*}
         */
        getPrice: function (name, element) {
            return this[element].baseConfig[name].baseParams.costs;
        },
        isSpell: function (name, element) {
            if (this[element] && this[element].cards && this[element].cards[name]) {
                var card = this[element].cards[name];
                return (card.type && card.type == "spell") ? card : false;
            }
            return false;
        }
    };

    /**
     * Calss to operate with Player game object
     * @class
     * @constructor
     */
    function Hero(config) {

        this.CONNECTED = 1;

        this.DISCONNECTED = -1;

        /**
         * Unique ID
         * @type {Boolean|String}
         */
        this.uid = false;

        /**
         * Connected or disconected
         * @type {Number}
         */
        this.status = this.DISCONNECTED;


        /**
         * SocketId
         * @type {Boolean}
         */
        this.socketId = false;

        /**
         * Member Name
         * @type {Boolean}
         */
        this.login = false;

        /**
         * Hero heakth points
         * @type {Number}
         */
        this.healthPoints = 50;

        /**
         * Fileds on the desc
         * @type {Object}
         */
        this.boardHolder = {1: false, 2: false, 3: false, 4: false, 5: false};


        /**
         * Member cards
         * @type {Object}
         */
        this.cards = {};

        /**
         * Top or bottom
         * @type {String}
         */
        this.color = "";

        /**
         * Oponent color for quick access
         * @type {String}
         */
        this.oponentColor = "";

        /**
         * All card of player sorted by type
         * @type {Object}
         */
        this.elementsResource = {water: 10, earth: 10, fire: 10, death: 10, life: 10, air: 10};


        /**
         * Map of actions wich executs beforemove, aftermove, after receive damage etc...
         * @type {{}}
         */
        this.preActions = {};

        /**
         * Pointer to game object
         * @type {null}
         */
        this.gameLink = null;


        /**
         * Check if call creature or cast spell during current move
         * @type {Boolean}
         */
        this.alreadyCast = false;

        //set config of class
        if (config && Object.keys(config).length > 0) {
            for (var k in config) {
                if (config.hasOwnProperty(k) && this.hasOwnProperty(k)) {
                    this[k] = config[k];
                }
            }
        }
    }

    /**
     * Serilize to json player data to load it on client side
     * @returns {{elementsResource: Object, color: String, cards: Object, login: Boolean, status: Number, oponentColor: String, boardHolder: {}, uid: Boolean, healthPoints: Number}}
     * @private
     */
    Hero.prototype._serialize = function () {
        var _self = this,
            tempBoardHolder = {},
            dataToGo;

        for (var ind in _self.boardHolder) {
            if (_self.boardHolder.hasOwnProperty(ind)) {
                tempBoardHolder[ind] = ( _self.boardHolder[ind]) ? _self.boardHolder[ind]._serialize() : false;
            }
        }

        dataToGo = {
            elementsResource: _self.elementsResource,
            color: _self.color,
            cards: _self.cards,
            login: _self.login,
            status: _self.status,
            oponentColor: _self.oponentColor,
            boardHolder: tempBoardHolder,
            uid: _self.uid,
            healthPoints: _self.healthPoints
        };

        return (dataToGo);
    };


    Hero.prototype.callCardProxy = function () {
        //to prevent hack and skip check of card
        this.callCard(arguments[0], arguments[1]);
    };
    /**
     * Place card to board
     * @param {String} name Creature prog name
     * @param {Number} index Index on board to place
     * @param {Hero|boolean} [playerOponent] Oponent player
     * @param {Boolean|Object} [sync] Sey if need perform some checks
     * @return {Creature|Object|Boolean}
     */
    Hero.prototype.callCard = function (name, index, playerOponent, sync) {
        var _self = this,
            cards = _self.cards,
            newCreature,
            price,
            element,
            config = (sync) ? sync : {boardIndex: index, creatureName: name},
            spell;


        if (!sync && _self.alreadyCast) {
            return {resp: false, reason: "casted"};
        }



        //check if card name is valid
        if (cards.hasOwnProperty(name) || sync) {

            element = cards[name];
            spell = CardsFactory.isSpell(name, element);

            if (!sync) {
                //logging of main actions
                _self.gameLink.gameLog.push({
                    who: _self.login,
                    do: "card",
                    time: (new Date()).valueOf(),
                    what: { spell: !!spell, name: name, element: element }
                });
            }


            if (spell !== false) {
                return _self.castSpell(spell, name);
            }

            //this fields is busy
            if (_self.boardHolder[index] && !sync) {
                return {resp: false, reason: "busy"};
            }

            //get price to pay
            price = CardsFactory.getPrice(name, element);

            //check if we can pay creature cost
            if (!_self.canPay(element, price) && !sync) {
                return {resp: false, reason: "resource"};
            } else {
                //get elements from pool
                _self.changeResource(element, -((!sync) ? price : 0));
            }

            //get new Creature instance
            newCreature = CardsFactory.getNewCard(name, element);

            //set on board field
            _self.boardHolder[index] = newCreature;

            //set name and index on board
            config.owner = _self;
            newCreature.setConfig(config);

            //after call action for inherited classes


            //execute after calls
            _self.runPreAction("callCard", {crd: newCreature});

            if (!sync) {
                newCreature.afterCall(_self, _self.oponent());
            }

            //tell to oponent about card call to sync objects state
            _self.syncDataSend("syncGame", {i: _self.uid, sync: "callCard", args: [newCreature.creatureName, newCreature.boardIndex]});

            //prevent other casting during current turn
            if (!sync) {
                _self.alreadyCast = true;
            }

            newCreature.skipLoging = true;

            //animate call
            Mediator.trigger('call-card', newCreature);


            return true;
        }
        return false;
    };


    /**
     * Send move details to oponent
     */
    Hero.prototype.syncDataSend = function (action, data) {
        var _self = this;
        process.nextTick(function () {
            _self.gameLink.sendToGame(action, data, _self.oponent().uid);
        });
        return this;
    };


    /**
     * Set elements counter
     * @param {Object} elements
     */
    Hero.prototype.setElements = function (elements) {
        this.elementsResource = elements;
    };


    /**
     * Executes before magic
     */
    Hero.prototype.beforeCast = function (magic) {
        //
        //return magic;
    };


    /**
     * Cast spell card
     */
    Hero.prototype.castSpell = function (spell, name) {
        console.log("CAST SPELL");
        var _self = this;
        if (!_self.canPay(spell.element, spell.costs)) {
            return {resp: false, reason: "resource"};
        } else {
            spell.costs = this.runPreAction("beforeCast", spell).costs;
            spell.castSpell.apply(this);
            _self.changeResource(spell.element, -spell.costs);
            _self.alreadyCast = true;
            _self.syncDataSend("syncGame", {i: _self.uid, sync: "callCard", args: [name, 1]});
            Mediator.trigger("cast-spell", {uid: _self.uid, spell: spell, name: name, login: _self.login});
            return true;
        }
    };


    /**
     * Proxy method to cast player creature ability
     * @param {Object} data Move data
     * @param {Number} data.ind Index of creature on the board
     * @param {Number} data.target Index of creature on the oponent board, if it was selected as a target
     * @returns {Boolean}
     */
    Hero.prototype.castCreateAbility = function (data) {
        var _self = this,
            result = true,
            tempTarget = data.target || false;

        if (data.ind && data.ind > 0 && data.ind < 6 && _self.boardHolder[data.ind]) {
            result = _self.boardHolder[data.ind].castAbility(data);
            if (result.result) {
                data.target = tempTarget;
                _self.syncDataSend("syncGame", {i: _self.uid, sync: "castCreateAbility", args: [data]});
            }
            return result.result;
        }
        return false;
    };


    /**
     * Action to surrender and edn game
     */
    Hero.prototype.surrender = function () {
        var _self = this;
        this.die("surrender");
        this.syncDataSend("syncGame", {i: _self.uid, sync: "surrender", args: []});
    };


    /**
     * Receive physical damage
     * @param {Number} damage
     */
    Hero.prototype.receiveDamage = function (damage) {

        var decoratorDamage = this.runPreAction("receiveDamage", {dmg: damage}),
            _self = this;

        if (decoratorDamage && decoratorDamage.dmg >= 0) {
            damage = decoratorDamage.dmg;
        }
        if (damage > 0) {
            this.healthPoints -= damage;
            //animate loose health
            Mediator.trigger("change-hitpoints:" + _self.uid, {hp: _self.healthPoints, dmg: damage, login: _self.login});
        }

        //sorry friend but you lose
        if (this.healthPoints <= 0) {
            _self.die();
        }
    };


    /**
     * Die, stop game and send signal about win to oponent
     */
    Hero.prototype.die = function (reason) {
        var _self = this,
            gameLink = this.gameLink,
            reasonSend = reason || "lose";

        if (!gameLink.isFinished()) {
            gameLink.setGameStatus("finished").getOponent(_self.uid).becomeWinner(reasonSend);
        }
    };

    /**
     * Apply some action to all creatures of player
     * @param {Function} applyFunction Function to apply for each creature
     */
    Hero.prototype.applyToAll = function (applyFunction) {
        var _self = this;

        //it was made in such way to avoid hasOwnProperty check and create for .. in .. loop
        //because we alway know what to check and count of fields

        if (_self.boardHolder["1"]) applyFunction(_self.boardHolder["1"]);
        if (_self.boardHolder["2"]) applyFunction(_self.boardHolder["2"]);
        if (_self.boardHolder["3"]) applyFunction(_self.boardHolder["3"]);
        if (_self.boardHolder["4"]) applyFunction(_self.boardHolder["4"]);
        if (_self.boardHolder["5"]) applyFunction(_self.boardHolder["5"]);
    };


    /**
     * Set the winner ID
     */
    Hero.prototype.becomeWinner = function (reason) {
        var _self = this;
        //show msgs to winner and loser
        Mediator.trigger("win", {uid: _self.uid, reason: reason});

        _self.gameLink
            .setGameStatus("finished")
            .setWinner(_self.uid)
            .endGame();
    };


    /**
     * Add preAction for some action like "after move". Useffule when some
     * spells strikes and make buffs for every turn ex. "poison storm"
     * @param {Function|String} preAction Function to execute
     * @param {String} step Event type "endMove", "startMove"
     */
    Hero.prototype.addPreAction = function (step, preAction) {
        var _self = this,
            preActionType = "string";

        if (typeof preAction == preActionType && GameActions.PLAYER[preAction]) {
            if (!_self.preActions.hasOwnProperty(step)) {
                _self.preActions[step] = {};
            }
            _self.preActions[step][preAction] = 1;
        }
        return this;
    };


    /**
     * Execure decoartors
     * @param {String} step
     * @param {Object} [config]
     */
    Hero.prototype.runPreAction = function (step, config) {
        var _self = this,
            preActions = _self.preActions,
            actions = (preActions[step]) ? Object.keys(preActions[step]) : [];

        if (actions.length > 0) {
            actions.forEach(function (action) {
                if (typeof GameActions.PLAYER[action] == "function") {
                    config = GameActions.PLAYER[action].call(_self, config);
                }
            });
        }
        return config;
    };

    /**
     * Remove decoartor
     * @param {String} step decoartor method name
     * @param {String} preAction Name of decoartor added by
     */
    Hero.prototype.removePreAction = function (step, preAction) {
        var _self = this;

        if (_self.preActions[step] && _self.preActions[step][preAction]) {
            delete _self.preActions[step][preAction];
            if (Object.keys(_self.preActions[step]).length == 0) {
                delete _self.preActions[step];
            }
        }
    };


    /**
     * Execute after player press skip or pressed fight button
     * Executes all creatures and move abilities, and apply and move buffs
     */
    Hero.prototype.endMove = function () {

        var _self = this,
            oponent = _self.oponent();

        _self.applyToAll(function endMoveIterate(fightCreature) {
            if (fightCreature.canFight && !fightCreature.justCalled) {
                Mediator.trigger("creature-attack", {id: _self.uid, ind: fightCreature.boardIndex});
                fightCreature.strike(oponent.boardHolder[fightCreature.boardIndex], oponent);
            }
            fightCreature.justCalled = false;
            fightCreature.canFight = true;
            fightCreature.endMove();
            fightCreature.useBuffs("endMove");
        });

        //change current turn
        _self.gameLink.currentTurn = oponent.color;
        //reset already cast identifier
        oponent.alreadyCast = false;

        //start move increase resources
        oponent.startMove();

        _self.runPreAction("endMove");
        //send data to sync
        _self.syncDataSend("syncGame", {i: _self.uid, sync: "endMove", args: []});
    };


    /**
     * Make +1 for each element resource
     */
    Hero.prototype.increaseElememnts = function () {
        this.changeResource({air: 1, water: 1, fire: 1, earth: 1, life: 1, death: 1});
        return this;
    };

    /**
     * Increase element for particular count
     * @param {String|Object} element Element Name
     * @param {Number} [count] Can be negative or positive
     * @param {Boolean|Number} [animate] Show change in UI
     * @return {Hero}
     */
    Hero.prototype.changeResource = function (element, count, animate) {
        var _self = this;

        animate = animate || 2;

        count = count || 0;
        if (typeof element == "object") {
            for (var elementName in element) {
                if (element.hasOwnProperty(elementName)) {
                    this.changeResource(elementName, element[elementName]);
                }
            }
            return this;
        }
        if (this.elementsResource.hasOwnProperty(element)) {
            this.elementsResource[element] += count;
            if (this.elementsResource[element] < 0) {
                this.elementsResource[element] = 0;
            }
            if (animate == 2) {
                Mediator.trigger('set-resource:' + _self.uid, {el: element, cnt: this.elementsResource[element]}, this.elementsResource);
            }
        }
        return this;
    };

    /**
     * Return creature instance specified by index
     * @param index
     * @return {Creature|Boolean}
     */
    Hero.prototype.getCreature = function (index) {
        if (index > 0 && index < 6) {
            return this.boardHolder[index];
        }
        return false;
    };


    /**
     * Start move method, executes after each player start move
     * and iterate through all creatures and executes buffs, and pre start move creature abilities
     */
    Hero.prototype.startMove = function () {
        var _self = this;
        _self.increaseElememnts()
            .applyToAll(function startMoveIterator(creature) {
                creature.startMove(_self, _self.oponent());
                creature.useBuffs("startMove");
                creature.alreadyCast = false;
            });
        _self.runPreAction("startMove");
    };


    /**
     * Check and count selected creature on the field or selected elemets
     * @param {String} [creatureName] Name of creature to find
     * @param {String} [element] Creature elements
     */
    Hero.prototype.creatureInField = function (creatureName, element) {
        var data = {count: 0},
            checkField = false,
            checkValue = false;
        if (creatureName && creatureName.length > 0) {
            checkField = "creatureName";
            checkValue = creatureName;
        } else if (element) {
            checkField = "element";
            checkValue = element;
        }

        if (checkField && checkValue) {
            if (this.boardHolder[1] && this.boardHolder[1][checkField] === checkValue) data.count++;
            if (this.boardHolder[2] && this.boardHolder[2][checkField] === checkValue) data.count++;
            if (this.boardHolder[3] && this.boardHolder[3][checkField] === checkValue) data.count++;
            if (this.boardHolder[4] && this.boardHolder[4][checkField] === checkValue) data.count++;
            if (this.boardHolder[5] && this.boardHolder[5][checkField] === checkValue) data.count++;
        }

        return data;
    };


    /**
     * Apply spells to all cards in the battle
     * @param {Function} applyFunction
     */
    Hero.prototype.applyToAllInBattle = function (applyFunction) {
        this.oponent().applyToAll(applyFunction);
        this.applyToAll(applyFunction);
    };


    /**
     * Heal player
     * @param {Number} amount
     */
    Hero.prototype.heal = function (amount) {
        var _self = this;
        if (amount > 0) {
            this.healthPoints += amount;
            Mediator.trigger("change-hitpoints:" + _self.uid, {hp: _self.healthPoints, add: amount, login: _self.login});
        }
        return this;
    };


    /**
     * Check if member can pay some quantity
     * @param {String|Object}element
     * @param [cost]
     * @return {Boolean}
     */
    Hero.prototype.canPay = function (element, cost) {

        var canPay = false;
        if (typeof cost == "object") {
            if (cost.earth) {
                canPay = this.canPay('earth', cost.earth);
            }

            if (cost.air) {
                canPay = this.canPay('air', cost.air);
            }

            if (cost.water) {
                canPay = this.canPay('water', cost.water);
            }

            if (cost.death) {
                canPay = this.canPay('death', cost.death);
            }

            if (cost.life) {
                canPay = this.canPay('life', cost.life);
            }

            if (cost.fire) {
                canPay = this.canPay('fire', cost.fire);
            }

            return canPay;
        }

        return this.elementsResource[element] >= cost;
    };

    /**
     * Return resource count
     * @param nameres
     */
    Hero.prototype.getElement = function (nameres) {
        if (this.elementsResource.hasOwnProperty(nameres)) {
            return this.elementsResource[nameres];
        } else {
            throw Error("Wrong name resource " + nameres);
        }
    };


    /**
     *
     */
    Hero.prototype.getCreature = function (index) {
        return this.boardHolder[index];
    };


    /**
     * Proxy method to get oponent
     * @return {Hero}
     */
    Hero.prototype.oponent = function () {
        return this.gameLink.getOponent(this.uid);
    };

    return Hero;
});
