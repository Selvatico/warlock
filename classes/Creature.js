(function (root, factory) {
    // Node.js
    if (typeof exports !== 'undefined') {

        var mediator = {
            trigger: function () {}
        };
        module.exports.Creature = factory(mediator, require("./GameActions"), require("util"));

    } else if (typeof define === 'function' && define.amd) {
        //client side require.js definition
        define(
            [
                'service/mediator',
                'cls/GameActions',
                'service/helper'
            ], function (Mediator, GameActions, Helper) {

                root.Creature = factory(Mediator, GameActions, Helper);
                return root.Creature;

            });
    }
})(this, function (Mediator, GameActions, util) {


    /**
     * Base class for creatures describes their interface
     * @constructor
     * @class Represents base creature funcs
     */
    function Creature(config) {

        /**
         * Show damage reduction from spells
         * @type {Number}
         */
        this.magicReduction = 0;


        /**
         * Indicate if creature receive damage from spells
         * @type {Boolean}
         */
        this.magicImune = false;

        /**
         * Show damage reduction from other creatures attacks
         * @type {Number}
         */
        this.physicalReduction = 0;

        /**
         * Indicate if creature receive damage from attacks
         * @type {Boolean}
         */
        this.physicalImune = false;

        /**
         * buffs for each step of move
         * @type {Object}
         */
        this.buffsList = {};

        /**
         * Number of hitpoints
         * @type {Number}
         */
        this.lives = 0;


        /**
         * Strange of atack
         * @type {Number}
         */
        this.attackPower = 0;

        /**
         * How many resources need to call
         * @type {Number}
         */
        this.costs = 0;

        /**
         * Water, fire, death etc...
         * @type {String}
         */
        this.element = false;

        /**
         * Creature ability element for cast
         * @type {String} water, death, fire etc...
         */
        this.abilityElement = false;

        /**
         * Cost of element to cast ability
         * @type {Number}
         */
        this.abilityCost = 0;

        /**
         * Indicate if can cast special abilities
         * @type {Boolean}
         */
        this.canCast = false;

        /**
         * Description of targeting cast funtion
         * @type {String}
         */
        this.castTarget = false;

        /**
         * Cast specail spell ability
         * @type {Function}
         */
        this.castFunction = null;

        /**
         * Index on the field
         * @type {Boolean}
         */
        this.boardIndex = false;

        /**
         * Type of card in the desk for creature default "creature"
         * @type {String}
         */
        this.type = "creature";

        /**
         * Indicates if creture not paralyzed and can fight. Archer for example can't fight too.
         * @type {Boolean}
         */
        this.canFight = true;

        /**
         * pointer to owner Hero class object
         * @type {Hero|Boolean}
         */
        this.owner = null;

        /**
         * CReature name
         * @type {String}
         */
        this.creatureName = "";


        /**
         * Creature can't fight just after call
         * @type {Boolean}
         * @default true
         */
        this.justCalled = true;


        /**
         * Indicates if creature already casted during this move
         * @type {Boolean}
         */
        this.alreadyCast = false;


        /**
         * Set config from constructor args
         */
        this.setConfig(config);

        /**
         * Save max hitPoints for heal prupose
         * @type {*|Number}
         */
        this.maxHealthPoint = this.lives;


        this.defaultMaxHealthPoint = this.maxHealthPoint;


        /**
         * Save it for restore on case of removing buffs
         * @type {Number}
         */
        this.defaultMagicReduction = this.magicReduction;


        /**
         * Save it for restore on case of removing buffs
         * @type {Number}
         */
        this.defaultPhysicalReduction = this.physicalReduction;


        /**
         * Sabe it to restore
         * @type {Number}
         */
        this.defaultAttackPower = this.attackPower;


        /**
         * To store any data between moves
         * @type {Object}
         */
        this.tempDataStore = {};


        this.animateR = 2;
    }

    /**
     * Set properties
     * @param config
     */
    Creature.prototype.setConfig = function (config) {
        if (config && Object.keys(config).length > 0) {
            for (var k in config) {
                if (config.hasOwnProperty(k) && this.hasOwnProperty(k)) {
                    this[k] = config[k];
                }
            }
        }
    };


    /**
     * Method to save data in store
     * @param {String} key
     * @param {Object|string|number}data
     */
    Creature.prototype.saveInStore = function (key, data) {
        this.tempDataStore[key] = data;
        return this;
    };


    /**
     * Method to save data in store
     * @param {String} key
     * @param {Boolean} [dataStay]
     */
    Creature.prototype.getStoreData = function (key, dataStay) {
        var data = false;
        if (this.tempDataStore[key]) {
            data = this.tempDataStore[key];
            if (!dataStay) {
                delete this.tempDataStore[key];
            }
        }
        return data;

    };


    /**
     * Serialize only important params to sync and restore data
     * @private
     */
    Creature.prototype._serialize = function () {
        var _self = this,
            dataToGo = {};
        for (var field in _self) {
            if (_self.hasOwnProperty(field) && field != "owner") {
                dataToGo[field] = _self[field];
            }
        }
        return dataToGo;
    };

    /**
     * Action to perform before move begin.
     * Will be ovverrided with some other creatures
     */
    Creature.prototype.startMove = function () {
    };

    /**
     * Action to perform at the end of player move
     * Will be ovverrided with some other creatures
     */
    Creature.prototype.endMove = function () {
    };


    /**
     * Action to perform before removing card from the field
     */
    Creature.prototype.afterDie = function () {
        var _self = this;
        this.owner.runPreAction("afterDie", {crd: _self, die: true});
    };


    /**
     * Executes when one creature killed another
     * @param {Creature} [creature] Killed creature
     */
    Creature.prototype.afterKill = function (creature) {
    };

    /**
     * Funtion to call after creature was placed to desk
     * @param {Hero} playerOwner Creature callee
     * @param {Hero} playerOposite Oponent
     */
    Creature.prototype.afterCall = function (playerOwner, playerOposite) {

    };


    /**
     * Executes before creature cast spell
     */
    Creature.prototype.beforeCast = function () {
        return !this.alreadyCast;
    };

    /**
     * Executes right after creature cast spell
     */
    Creature.prototype.afterCast = function () {
        this.alreadyCast = true;
    };


    /**
     * Set new attack power for creature
     * @param {Number|Boolean} attackPower
     * @param {Boolean} [animate] Change or not visual part
     * @param {Number} [diff]
     * @return {Creature}
     */
    Creature.prototype.setAttackPower = function (attackPower, animate, diff) {
        var _self = this;
        if (!attackPower && diff != 0) {
            attackPower = _self.attackPower + diff;
            if (attackPower < 0) attackPower = 0;
        }

        if (attackPower >= 0) {
            this.attackPower = attackPower;
            if (animate) {

                Mediator.trigger("set-creature-attack",
                    {id: _self.owner.uid, ind: _self.boardIndex, atk: _self.attackPower});
            }
        }
        return this;
    };

    /**
     * Cast method to execute all pre-, after- and cast actions;
     * @param {Object} [config]
     * @param {Object} [config.target] Target for casting
     */
    Creature.prototype.castAbility = function (config) {
        var checkPayResult = false,
            owner = this.owner,
            priceToPay = {},
            casted = false;

        config = config || {target: false};

        if (config.target && (config.target > 0 && config.target < 6) && this.castTarget != "none") {
            if (this.castTarget == "friend") {
                config.target = owner.boardHolder[config.target];
            } else {
                config.target = owner.oponent().boardHolder[config.target];
            }
        }

        if (this.beforeCast()) {
            //if we need pay several elements
            if (typeof this.abilityCost == "object") {
                for (var el in this.abilityCost) {
                    checkPayResult = owner.canPay(el, this.abilityCost[el]);
                    priceToPay[el] = this.abilityCost[el] * -1;
                }
            } else {
                //if we need to pay only one element
                priceToPay[this.abilityElement] = this.abilityCost * -1;
                checkPayResult = owner.canPay(this.abilityElement, this.abilityCost);
            }

            //if we have enough to pay
            if (checkPayResult) {
                casted = this.castFunction(config.target);
                if (typeof casted == "undefined" || casted === true) {
                    owner.changeResource(priceToPay, false, this.animateR);
                    this.afterCast();
                }
                return {result: true};
            } else {
                return {result: false, reason: "elements"};
            }
        } else {
            return {result: false, reason: "already"};
        }
    };


    /***
     * Method to strike opponent creature or opponent player with
     * physical damage
     * @param {Creature} opCreature Object of opponent creature
     * @param {Hero} opPlayer Object of opponent player
     */
    Creature.prototype.strike = function (opCreature, opPlayer) {
        var _self = this,
            livesLeft = 0;
        //if we have creature in opposite field
        if (opCreature && opCreature.lives > 0) {
            if (opCreature.lives > 0) {
                opCreature.defend(_self.attackPower, _self);
            } else {
                opCreature.die();
            }
        } else {
            //if there no oposite creature and provided oponent object
            if (opPlayer) {
                opPlayer.receiveDamage((_self.attackPower - livesLeft));
            }
        }

    };

    /**
     * Receive damage
     * @param {Number} damage Atack damage of opponent creature
     * @param [attackCreature]
     * @return {Number} Number of left health points
     */
    Creature.prototype.defend = function (damage, attackCreature) {
        var _self = this,
            livesBefore = this.lives,
            damageToGive = _self.useBuffs("reduceDamage", (damage - this.physicalReduction));


        //if we have immune to physical
        if (this.physicalImune) {
            return this.lives;
        }

        //take damage
        this.lives = this.lives - damageToGive;

        if (this.lives <= 0) {
            if (attackCreature) {
                attackCreature.afterKill(_self);
            }
            _self.die(attackCreature);
        } else {

            Mediator.trigger("creature-defend", {cr: _self, diff: (_self.lives - livesBefore), atacker: attackCreature});

            _self.useBuffs("defend", {at: attackCreature, damage: damageToGive});
        }
        return this.lives;
    };

    /**
     * Defend from magic abilities and spells
     * @param {Number} magicDamage points of magic damage
     * @param {String} type Type of magic "fire", "water" etc...
     * @param {Boolean} [fromAbility] Indicates if this is monster ability of spell from hero
     * @return {Number} Number of left health points
     */
    Creature.prototype.defendMagic = function (magicDamage, type, fromAbility) {
        var _self = this,
            livesBefore = this.lives,
            damageToGive = (magicDamage - this.magicReduction);

        if (this.magicImune) {
            return this.lives;
        }

        //apply damage
        this.lives = this.lives - damageToGive;

        //UI animate
        if (this.lives <= 0) {
            _self.die({element: type });
        } else {
            _self.useBuffs("creature-defend-magic", {magic: magicDamage, type: type, ability: fromAbility});
            Mediator.trigger("defend", {cr: _self, diff: (_self.lives - livesBefore)});
        }
        return this.lives;
    };

    /**
     * Creature die. Remove creature from the field
     * @param {Creature|Object} [attackCreature] Creature whick kill it
     */
    Creature.prototype.die = function (attackCreature) {
        var _self = this,
            die = _self.useBuffs("die", true);

        _self.afterDie();

        //execure buffs and decorators
        if (die) {
            //remove from field in UI
            Mediator.trigger("creature-die", {id: _self.owner.uid, ind: _self.boardIndex, creature: _self});

            //remove it from field in the object
            _self.owner.boardHolder[_self.boardIndex] = false;

            //remove pointer to owner object
            _self.owner = false;
        }
    };

    /**
     * Perform creature special abilities
     * @param {Hero} player Owner of the creature
     */
    Creature.prototype.cast = function (player) {
        if (player.elementsResource[this.abilityElement] >= this.abilityCost) {
            if (typeof this.castFunction == "function") {
                this.castFunction.apply(this, arguments);
            }
        }
    };


    /**
     * Add buff for some action like "after move". Useffule when some
     * spells strikes and make buffs for every turn ex. "poison storm"
     * @param {Function} actionName Function to execute
     * @param {String} [step] Event type "endMove", "startMove"
     * @param {Number} [type]Buff type - Negative = "-1", Positive = "1"
     */
    Creature.prototype.setCreatureBuff = function (step, actionName, type) {
        var _self = this,
            buffType = "string",
            setType = type || 1;

        //do not apply buffs if creature has magic immune
        if (_self.magicImune) return false;

        if (typeof actionName == buffType && GameActions.CREATURE[actionName]) {
            if (!_self.buffsList.hasOwnProperty(step)) {
                _self.buffsList[step] = {};
            }
            _self.buffsList[step][actionName] = setType;
        }
        return this;
    };


    /**
     * Execute buffs regarding move steps
     * @param {String} step Step of move
     * @param {Object|Boolean|Number} [config]
     */
    Creature.prototype.useBuffs = function (step, config) {
        var _self = this,
            response = config;

        if (_self.buffsList[step]) {
            Object.keys(_self.buffsList[step]).forEach(function (buffName) {
                if (GameActions.CREATURE[buffName]) {
                    response = GameActions.CREATURE[buffName].call(_self, config)
                }
            });
        }
        return response;
    };

    /**
     * Remove buffs from one step of particular kind
     * @param {String} [step] Name of buffing step e.x "startMove"
     * @param {Number} [removeType] Type of buff to remove. Can be 1, -1, 0
     * @private
     */
    Creature.prototype._deleteOneBuff = function (step, removeType) {
        var _self = this;
        Object.keys(_self.buffsList[step]).forEach(function (buff) {
            if (_self.buffsList[step][buff] === removeType) {
                delete _self.buffsList[step][buff];
            }
        });
    };

    /**
     * Remove list of buffs regarding parameters
     * @param {String|boolean} [step] Name of buffing step e.x "startMove"
     * @param {String|boolean} [actionName] Name of buff to remove
     * @param {Number} [removeType] Type of buff to remove, in case when not provided step or actionName. Can be 1, -1, 0
     */
    Creature.prototype.removeBuffs = function (step, actionName, removeType) {
        var _self = this;
        //remove all buffs at all if step not provided
        if (!step) {
            if (removeType) {
                Object.keys(_self.buffsList).forEach(function (stepName) {
                    _self._deleteOneBuff(stepName, removeType)
                });
            } else {
                _self.buffsList = {};
            }
        } else if (actionName) {
            //remove buffs for particular step
            if (_self.buffsList[step] && _self.buffsList[step][actionName]) {
                delete _self.buffsList[step][actionName];
            }
        } else {
            if (_self.buffsList[step]) {
                //remove only buffs with concrete flag
                if (removeType !== 0) {
                    _self._deleteOneBuff(step, removeType);
                } else {
                    delete _self.buffsList[step];
                }
            }
        }
    };

    /**
     * Call method of parent
     * @param {Object} context Aplly context fot this
     * @param {String} name Name of method
     * @param {Arguments|Array} args Function argument
     * @return {*}
     */
    Creature.prototype.ancestor = function (context, name, args) {
        if (Creature.prototype[name] && typeof Creature.prototype[name] == "function") {
            return Creature.prototype[name].apply(context, args)
        }
    };


    /**
     * To construct dynamically created creatures
     */
    Creature.inherit = function (element) {
        for (var creatureName in element.baseConfig) {
            if (element.baseConfig.hasOwnProperty(creatureName)) {
                defineCreature(element.cards, creatureName, element.baseConfig[creatureName]);
                if (element.baseConfig[creatureName].baseParams) {
                    element.baseConfig[creatureName].baseParams.name = creatureName;
                    element.baseConfig[creatureName].baseParams.type = 'creature';
                } else {
                    element.baseConfig[creatureName].name = creatureName;
                }
            }
        }
    };


    /**
     *
     * @param {Number} hitPoints Number of hitpoint add or remove
     * @param {Boolean} [skipLimit]
     * @return {Creature}
     */
    Creature.prototype.changeHitPoints = function (hitPoints, skipLimit) {
        var _self = this,
            currentLives = _self.lives,
            newLives,
            maxHp = _self.maxHealthPoint;

        if (skipLimit) {
            newLives = currentLives + hitPoints;
        } else {
            newLives = (((currentLives + hitPoints) > maxHp ) ? maxHp : (currentLives + hitPoints))
        }

        //save value
        _self.lives = newLives;

        //change UI part
        Mediator.trigger("creature-change-lives", {cr: this, added: hitPoints, diff: (newLives - currentLives)});

        return this;
    };


    /**
     * Highlight targets for creatures cast
     */
    Creature.prototype.getCastSelector = function () {
        var selector = "",
            target,
            deck;

        if (this.castTarget) {
            switch (this.castTarget) {
                case "enemy":
                    selector = ".deckOne li:not(.empty)";
                    break;

                case "friend":
                    selector = ".deckTwo li:not(:eq(" + (this.boardIndex - 1) + ")):not(.empty)";
                    break;

                case "call":
                    selector = ".deckTwo li.empty";
                    break;

                default :
                    if (~this.castTarget.indexOf("custom")) {
                        target = this.castTarget.split(":")[1];
                        deck = (this.castTarget.split(":")[2] == "friend") ? "deckTwo" : "deckOne";
                        selector = "." + deck + " li." + target;
                    }
                    break;
            }
        }
        return selector;
    };

    /**
     * Check immune to physical or spells
     * @param [type]
     * @return {Boolean}
     */
    Creature.prototype.notIsImmune = function (type) {
        type = type || "magic";
        return (this[type]) ? this[type] : false;
    };

    Creature.prototype.level = function () {
        return this.costs;
    };


    /**
     * Define a creature inside parent block
     * @param {Object} parentObj
     * @param {String} name Name of creature
     * @param {Object} config Any params regarding creature and methods to override
     * @param {Object} config.baseParams Basically params of creature like "hitpoints" etc..
     * @param {Object} config.override Methods to override in base class
     */
    function defineCreature(parentObj, name, config) {
        if (!parentObj[name] && config.baseParams) {
            parentObj[name] = function () {
                Creature.call(this, config.baseParams);
            };

            //inherit from base class
            util.inherits(parentObj[name], Creature);

            for (var overMethod in config.override) {
                if (config.override.hasOwnProperty(overMethod) && typeof config.override[overMethod] == "function") {
                    parentObj[name].prototype[overMethod] = config.override[overMethod];
                }
            }
        } else if (config.type && config.type == "spell") {
            parentObj[name] = config;
        } else {
            throw Error("Duplicate creature");
        }
    }

    return Creature;
});