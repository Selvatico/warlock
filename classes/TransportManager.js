var util = require('util');
var cardsManager = require("./CardsHeroManager").manager;
var helper = require("./Helper");


module.exports = function TransportManager(socketServer, memoryStore) {

    var actionMapping = {
        fg:"findGame",
        jp:"joinPlayer",
        sf: "stopFind"
    };

    socketServer.configure(function () {
        socketServer.set('authorization', function (data, accept) {
            if (!data.headers.cookie)
                return accept('No cookie transmitted.', false);

            data.cookie = memoryStore.parseCookie(data, {}, function () {});

            data.sessionID = data['signedCookies']['cd.sid'];

            memoryStore.load(data.sessionID, function (err, session) {
                if (err || !session) return accept('Error', false);
                data.session = session;
                return accept(null, true);
            });
        });
    });

    socketServer.sockets.on('connection', function (client) {


        util.log("Connection set");
        cardsManager.incrCon("socket");
        client.session = client.manager.handshaken[client.id].session;

        //simple json event
        client.on('message', function (res) {
            util.log("MESSAGE EVENT");
            try {
                var data = JSON.parse(res);
                if (data.a && actionMapping[data.a]) {
                    if (typeof cardsManager[actionMapping[data.a]] == "function") {
                        cardsManager[actionMapping[data.a]](data, client);
                    } else {
                        util.log('Unsuported action called ' + data.a + " of " + actionMapping[data.action]);
                    }
                }
            } catch (e) {
                console.log(e);
            }
        });

        //"joinchat" event
        client.on("join_chat", function (data) {
            var sessMember = client.session.member;

            if (!cardsManager.chatMembers.members.hasOwnProperty(sessMember.uid)) {
                cardsManager.chatMembers.members[sessMember.uid] = sessMember.u;
                cardsManager.chatMembers.counter ++;
            }
            //emit "chat joined"
            client.emit("chat_joined");

            //join room
            client.join("find_page");

            //send to other members about new chat member
            socketServer.sockets.in('find_page').emit('new_user', {u : client.session.member.u});
        });

        //"chatmessage" event
        client.on("chat_msg", function (data) {
            var dataToSend;
            if (data.msg) {
                dataToSend = {msg : helper.htmlEnteties(data.msg), u : client.session.member.u};
                socketServer.sockets.in('find_page').emit('new_msg', dataToSend);
            }
        });


        client.on('disconnect', function () {
            var sessMember = client.session.member;
            cardsManager.decrCon("socket");
            cardsManager.disconnect(client);

            //remove from find page chat
            if (cardsManager.chatMembers.members.hasOwnProperty(sessMember.uid)) {
                delete cardsManager.chatMembers.members[sessMember.uid];
                cardsManager.chatMembers.counter--;
                client.broadcast.to('find_page').emit('user_left', {u : client.session.member.u});
            }
        });

    });
};
