(function (root, factory) {
    // Node.js
    if (typeof exports !== 'undefined') {

        var elements = {
            water : require("./Water"),
            fire  : require("./Fire"),
            earth : require("./Earth"),
            air   : require("./Air"),
            life  : require("./Life"),
            death : require("./Death")
        };

        module.exports.Desk = factory(elements, require("util"));

    } else if (typeof define === 'function' && define.amd) {
        //client side require.js definition
        define(
            [
                'service/helper',
                'cls/Water',
                'cls/Fire',
                'cls/Air',
                'cls/Earth',
                'cls/Life',
                'cls/Death'
            ], function (Helper, Water, Fire, Air, Earth, Life, Death) {

                var elements = {
                    water : Water,
                    fire  : Fire,
                    earth : Earth,
                    air   : Air,
                    life  : Life,
                    death : Death
                };

                root.Desk = factory(elements, Helper);
                return root.Desk;
            });
    }
})(this, function (Elements, util) {


    /**
     * To create random sets of cards
     * @class to manipulate random desks create and assigm them to players
     * @constructor
     */
    function Desk() {
        this.elements = {
            water  : Elements.water,
            fire   : Elements.fire,
            earth  : Elements.earth,
            air    : Elements.air,
            life   : Elements.life,
            death  : Elements.death
        };
        this.randomCards();
    }

    /**
     * Get random cards from each elements
     */
    Desk.prototype.randomCards = function () {
        var _self = this, tempVar;
        for (var element in _self.elements) {
            if (_self.elements.hasOwnProperty(element) && _self.elements[element].cards) {
                tempVar = Object.keys(_self.elements[element].cards);
                if (tempVar.length > 0) {
                    _self.elements[element] = _self.shuffleCards(tempVar, 3).slice(0, 14);
                }
            }
        }
    };


    /**
     * SHuffle elements between members
     */
    Desk.prototype.randomElements = function () {
        var elementsCounter = {
                "1": {water: 3, earth: 3, fire: 3, death: 3, life: 3, air: 3},
                "2": {water: 3, earth: 3, fire: 3, death: 3, life: 3, air: 3}
            },
            elements = {'1': "water", '2': "earth", '3': "fire", '4': "death", '5': "life", '6': "air"},
            elementsTo = 12,
            elemeInd;

        for (var oi = 1; oi <= 2; oi++) {
            for (var ei = 1; ei <= elementsTo; ei++) {
                elemeInd = this._randomFromTo(1, 6);
                elementsCounter[oi][elements[elemeInd]]++;
            }
        }
        return elementsCounter;
    };

    /**
     * Util method for generating numbers between form to
     * @param from
     * @param to
     * @return {Number}
     * @private
     */
    Desk.prototype._randomFromTo = function (from, to) {
        return Math.floor(Math.random() * (to - from + 1) + from);
    };


    /**
     * Assign randomed cards to players
     * @param {Hero} player1
     * @param {Hero} player2
     */
    Desk.prototype.assignCards = function (player1, player2) {
        var _self = this, elementsList = _self.elements;
        for (var element in elementsList) {
            if (elementsList.hasOwnProperty(element) && util.isArray(elementsList[element])) {
                elementsList[element].forEach(function (val, index) {
                    if (index % 2 == 0) {
                        player1.cards[val] = element;
                    } else {
                        player2.cards[val] = element;
                    }
                });
            }
        }
    };


    /**
     * TO shuffle cards
     * @param {Array} theArray
     * @param {Number} iteration
     * @return {Array} Shuffled cards of each elements
     */
    Desk.prototype.shuffleCards = function (theArray, iteration) {
        var len = theArray.length;
        for (var it = 0; it < iteration; it++) {
            var i = len;
            while (i--) {
                var p = parseInt(Math.random() * len);
                var t = theArray[i];
                theArray[i] = theArray[p];
                theArray[p] = t;
            }
        }
        return theArray;
    };


    return Desk;
});







