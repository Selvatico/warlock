define([
    'jquery',
    'underscore',
    'backbone',
    'models/card'
], function ($, _, Backbone, CardModel) {
    // The collection of todos is backed by *localStorage* instead of a remote
    // server$(this.el).html(this.template(this.model.toJSON()));
    var CardsList = Backbone.Collection.extend({

        // Reference to this collection's model.
        model: CardModel,

        url: function () {

        }
    });

    // Create our global collection of **TodosCollection**.
    return new CardsList;
});