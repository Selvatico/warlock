define([
    'service/helper',
    'service/mediator'
], function (Helper, Mediator){
    return {
        socket   : null,
        connect  : function(func) {
            var _self = this;
            if (_self.socket == null) {
                _self.socket = io.connect("http://" + document.location.host +"/", {"max reconnection attempts":5, 'flash policy port':10843});

                _self.socket.on('connect', function () {
                    _self.connected = true;
                    if (func) { func();}
                });

                _self.socket.on("disconnect", function () {
                    _self.connected = false;
                    Mediator.trigger('connection-lost');
                })

                _self.socket.on('reconnect', function () {
                    Mediator.trigger('connection-restored');
                    //refresh page because we don't know the reason of the lost
                    setTimeout(function(){
                        document.location.reload();
                    }, 4000);
                });
                _self.socket.on('message', function (data) {
                    if (data) {
                        data = JSON.parse(data);
                        if (data && data.a) {
                            var actionName = data.a + "Action";
                            Mediator.trigger('socket-data', actionName, data);
                        }
                    }
                });
            } else if (!_self.socket.socket.connected) {
                _self.socket.socket.reconnect();
            }
        },
        _sendSocket:function (action, data) {
            data.a = action;
            this.socket.send(JSON.stringify(data));
            return this;
        }

    }
});