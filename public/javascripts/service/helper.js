/**
 * Implements node.js server side analogs, which required by server classes
 * @export Helper
 */
define(['underscore', 'service/mediator'], function (_, Mediator) {
    return {
        inherits: (function () {
            if (Object.create) {
                return function (ctor, superCtor) {
                    ctor.super_ = superCtor;
                    ctor.prototype = Object.create(superCtor.prototype, {
                        constructor: {
                            value: ctor,
                            enumerable: false,
                            writable: true,
                            configurable: true
                        }
                    });
                };
            } else {
                return function (ctor, ctor2) {
                    function F() {
                    }

                    F.prototype = ctor2.prototype;
                    ctor.prototype = new F();
                }
            }
        })(),
        isArray: function (obj) {
            return Object.prototype.toString.call(obj) === '[object Array]';
        },
        /**
         * One point for all Ajax requests
         * @param url
         * @param params
         * @param [callback]
         * @param [type]
         */
        ajax:function ajaxHelper(url, params, callback, type) {
            $.post(url, params, callback, ((!type) ? "json" : type))
                .error(function() {})
                .complete(function(data, textStatus) {
                    if (textStatus == "error") {
                        var msg = JSON.parse(data.responseText);
                        if(msg) {
                            msg = "<span style='font-size: 20px;'>SERVER ERROR OCURED. Please report about it to <a href='https://github.com/Selvatico/warlock-issue/issues'>" +
                                "Bug tracker</a><br>"
                                + 'If you are on game page please find game page and start new once.<br></span> Error stack trace: '
                                + msg.error.message + "\n" + msg.error.stack + "\n Please send this info to admins";
                            console.error("====SERVER ERROR====", msg);
                            //TODO: Add call of popup
                            //Mediator.trigger('log-entry', ({html:msg}));
                        }
                    }
                });
        },
        /**
         *
         * @param selector
         * @param [func]
         * @returns {*}
         */
        getTemplate: function (selector, func) {
            var $templateHtml = $(selector),
                tplSettings   = {
                    interpolate : /\{\{(.+?)\}\}/g,
                    evaluate    : /\{%(.+?)%\}/g,
                    escape      : /\{-(.+?)-\}/g
                };
            if ($templateHtml.length > 0) {
                return (!func) ? $templateHtml.html() : _.template($templateHtml.html(), false, tplSettings);
            } else {
                throw Error("Template holder with selector '" + selector +"' not found");
            }
        },
        sortCards: function(me, Elements) {
           return Object.keys(me.cards).sort(function (a, b) {
                var aData = Elements[me.cards[a]].baseConfig[a],
                    bData = Elements[me.cards[b]].baseConfig[b];

                aData = (aData.baseParams) ? aData.baseParams : aData;
                bData = (bData.baseParams) ? bData.baseParams : bData;

                //if (aData.costs == bData.costs) return 0;
                if (aData.costs < bData.costs) {
                    return -1;
                }
                if (aData.costs > bData.costs) {
                    return 1;
                }
                return 0;
            });
        },
        _getLogNameLink:function (n, rn) {
            return '<a style="text-decoration: underline" class="logName" data-realname="' + n + '">' + rn + '</a>';
        },
        msgFactory: function(data){
            var color = "",
                msg = "",
                now =  (new Date()).toLocaleTimeString();

            if (data) {
                if (data.type) {
                    if (data.time) {msg = (new Date(data.time)).toLocaleTimeString() + ": ";}
                    switch (data.type) {

                        case "error":
                            if (data.msg) {msg +=now  + ": <span style='color: red;'>" + data.msg + "</span>";}
                            break;

                        case "info":
                            if (data.msg) { msg += now + ": <span style='color: green;'>" + data.msg + "</span>";}
                            break;

                        case "game":
                            if (data.actor) {
                                if (!data.time) {msg += now + ": ";}

                                if (data.actor == "player") {msg += "Player <i>" + data.actorName + "</i> ";}
                                else if (data.actor == "monster") {msg += '<a style="text-decoration: underline" class="logName" data-realname="' + data.internalName + '">' + data.showName + '</a>';}

                                if (data.action == "callMonster") { msg += 'call <a style="text-decoration: underline" class="logName" data-realname="' + data.internalName + '">' + data.showName + '</a> to battle';}
                                else if (data.action == "castSpell") {msg += 'casted <a style="text-decoration: underline" class="logName" data-realname="' + data.internalName + '">' + data.showName + '</a>';}
                                else if (data.action == "deal") {msg += this._getLogNameLink(data.attack.n, data.attack.rn) + " deal <span style='color:red;'>" + data.dmg + "</span> points of damage to " + this._getLogNameLink(data.internalName, data.showName); }
                                else if (data.action == "damaged") {msg += this._getLogNameLink(data.internalName, data.showName) + " received <span style='color:red;'>" + data.dmg + "</span> points of damage";}
                                else if (data.msg) {msg += data.msg;}
                            }
                            break;
                    }
                }
            }
            return msg;
        }
    }
});