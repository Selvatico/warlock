/**
 * Imports node.js Event emitter enalog
 * @export Events
 */
define([], function () {
    return { EventEmitter: ((typeof io != "undefined") ? io.EventEmitter : {})};
});
