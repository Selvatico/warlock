/**
 * View for log bar user
 */
define([
    'jquery',
    'underscore',
    'backbone',
    'service/helper',
    'service/mediator'
], function ($, _, Backbone, Helper, Mediator) {

    return Backbone.View.extend({

        tagName: 'div',

        // Cache the template function for a single item.
        template: Helper.getTemplate('#log-tpl'),

        playerData: {},

        // The DOM events specific to an item.
        events: {
            "click .log .show_log": "toggleExpand"
        },

        // The TodoView listens for changes to its model, re-rendering.
        initialize: function (config) {
            Mediator.on('log-entry', this.appendLogEntry, this);
        },
        toggleExpand: function (event) {
            var $btn = this.$(event.target);
            $btn.toggleClass('expanded');
            var block = this.$('.log ul');
            if ($btn.hasClass('expanded')) {
                block.css('overflow-y','auto').animate({ height:98 },'normal');
            } else {
                block.css('overflow-y','hidden').animate({ height:32 },'normal', function(){
                    block.scrollTop(9999);
                });
            }
            return false;
        },
        /**
         * Add new log entry to holder
         * @param {Object} data
         */
        appendLogEntry: function(data) {
            var msg = Helper.msgFactory(data);
            if (msg != '') {
                this.$(".log ul").append('<li>' + msg + '</li>');
                this.$(".log ul").scrollTop(9999);
            }
        }
    });
});
