/**
 * View for log bar user
 */
define([
    'jquery',
    'underscore',
    'backbone',
    'service/helper',
    'service/mediator'
], function ($, _, Backbone, Helper, Mediator) {

    return Backbone.View.extend({

        // Cache the template function for a single item.
        template    : Helper.getTemplate('#battle-tpl'),

        cardTemplate: Helper.getTemplate('#card-tpl', true),

        emptyField: '<li class="deckItem empty"  style="left:%1px" data-cell="%2"><span class="clickHere">Click here to call monster</span></li>',

        cardData    : {},

        catchClick  : false,

        collection  : null,

        boards: {},

        leftOffsets: {
            "1": 10,
            "2": 145,
            "3": 280,
            "4": 415,
            "5": 550
        },

        // The DOM events specific to an deck.
        events: {
            'click .empty.active' : 'callCard',
            'click a.spell'       : 'creatureCast'
        },

        initialize: function () {
            //common events for game and creatures
            Mediator.on('click-cast',     this.showFreeSeats,    this);
            Mediator.on('call-card',      this.animateCallCard,  this);
            Mediator.on('cast-spell',     this.animateCastSpell, this);
            Mediator.on('die-card',       this.animateDieCard,   this);
            Mediator.on('start-round',    this.startRound,       this);
            Mediator.on('end-round',      this.endRound,         this);
            Mediator.on('creature-attack',this.animateAttack,    this);
            Mediator.on('win',            this.showWin,          this);

            //deck card animation
            Mediator.on('set-creature-attack',   this.setCreatureAttack,    this);
            Mediator.on('creature-defend',       this.animateDefend,        this);
            Mediator.on('creature-defend-magic', this.animateDefend,        this);
            Mediator.on('creature-die',          this.creatureDie,          this);
            Mediator.on('creature-change-lives', this.animateCreatureLives, this);

            //custorm creature animate events
            Mediator.on('manticora-cast',        this.manticoraCast,      this);
            Mediator.on('monk-reduce-attack',    this.monkReduceAttack,   this);


        },
        /**
         * Save references to boards of players
         */
        initBoards: function() {
            var _self = this;
            _.each(this.$(".board"), function(value) {
                _self.boards[$(value).data('uid')] = $(value);
            });
        },

        /**
         * Remove spell button from manticora target
         * @param {Creature} creature Target creature object
         */
        manticoraCast: function(creature) {
            this.boards[creature.owner.uid].find("li:eq(" + (creature.boardIndex - 1) + ")").find("a.spell").remove();
        },

        /**
         * Reduce attack of target creature by One
         * @param {Creature} target
         */
        monkReduceAttack: function(target) {
            this.setCreatureAttack({id: target.owner.uid, ind: target.boardIndex, atk: target.attackPower});
        },
        /**
         * Animate change of creature hit points
         * @param data
         */
        animateCreatureLives: function (data) {
            var diff = data.diff;
            data = data.cr;
            if (data.owner.uid && data.boardIndex) {
                if (diff !== 0) {
                    var poinToInsert = this.boards[data.owner.uid].find("li:eq(" + (data.boardIndex - 1) + ")").find(".monsterHealth");
                    setTimeout(function () {
                        $('<div class="monsterCh badge"></div>')
                            .insertAfter(poinToInsert)
                            .html(((diff < 0) ? "" : "+") + diff)
                            .removeClass("badge-success").removeClass("badge-important")
                            .addClass(((diff < 0) ? "badge-important" : "badge-success"))
                            .animate({opacity: 1, left: "50px"}, 1500).animate({left: "30px", opacity: 0}, 1500);
                    }, (Math.random() * 1000));

                    /*if (diff < 0) {
                        Mediator.trigger('log-entry', {
                            type: "game",
                            action: (originalData.atacker) ? 'deal' : 'damaged',
                            attack: {n: originalData.atacker.creatureName, rn:this.collection.get(originalData.atacker.creatureName).attributes.realName },
                            dmg: diff * -1,
                            actor: "1",
                            internalName: data.creatureName,
                            showName: this.collection.get(data.creatureName).attributes.realName
                        });
                    }*/
                }
                this.boards[data.owner.uid].find("li:eq(" + (data.boardIndex - 1) + ")").find(".monsterHealth").html(data.lives);
            }
        },
        /**
         * Set html attack power of creature
         * @param {Object} data
         */
        setCreatureAttack: function(data) {
            if (data.id && data.ind) {
                this.boards[data.id].find("li:eq(" + (data.ind - 1) + ")") .find(".atack") .html(data.atk);
            }
        },
        creatureDie: function(data){
            if (data.id && data.ind) {
                this.boards[data.id].find("li:eq(" + (data.ind - 1) + ")").animate({opacity: 0}, 1500)
                    .replaceWith(this.emptyField.replace("%1", this.leftOffsets[data.ind]).replace("%2", data.ind));

                Mediator.trigger('log-entry', {type: "game", actor: "monster", internalName: data.creature.creatureName,
                    msg: " died", showName: this.collection.get(data.creature.creatureName).attributes.realName
                });
            }
        },
        animateDefend: function(data) {
            if (data.cr) {

                this.animateCreatureLives(data);

                data = data.cr;

                var $liElement       = this.boards[data.owner.uid].find("li:eq(" + (data.boardIndex - 1) + ")"),
                    leftOffsetBefore = parseInt($liElement.css("left"), 10),
                    topBefore        = parseInt($liElement.css("top"), 10);

                $liElement
                    .animate({top : (topBefore + 6)}, 10)
                    .animate({top : (topBefore - 6)}, 10)
                    .animate({top : topBefore}, 10)
                    .animate({left: (leftOffsetBefore - 5)}, 10)
                    .animate({left: (leftOffsetBefore + 5)}, 10)
                    .animate({left: leftOffsetBefore}, 10);
            }
        },
        animateAttack: function(data){
            if (data.id && data.ind) {
                var $liElement      = this.boards[data.id].find("li:eq(" + (data.ind - 1) + ")"),
                    topOffsetBefore = parseInt($liElement.css("top"), 10),
                    moveTo          = (this.boards[data.id].data('owner') == 'playerTwo') ? -80 : 80;
                $liElement.animate({top: (topOffsetBefore + moveTo) + "px"}).animate({top: topOffsetBefore + "px"});
            }
        },
        showWin: function (data) {
            var msg, msgTitle, me;
            if (data.uid) {
                me =(this.boards[data.uid].data('owner') == 'playerTwo');
                msg = (me) ? "You win the battle. You gain +7 to your rating" : "Sorry, but you Lose! You lost -7 from your rating!";
                msgTitle = (me) ? "Congratulations" : "Lose!";
                if (me && data.reason == "surrender") {
                    msg = "Opponent surrendered! " + msg;
                }
                Mediator.trigger('switch-overlay', true, msgTitle + "<br />" + msg, true);
            }
        },
        setCollection: function (collection) {
            this.collection = collection;
            return this;
        },
        /**
         * Show free slots for creature call
         * @param {Boolean} toggle Show or hide free slots
         * @param {Object} [cardData] Data for call card
         */
        showFreeSeats: function (toggle, cardData) {
            if(toggle) {
                this.cardData   = cardData;
                this.catchClick = true;
                this.$("#board_" + GameData.me.uid).find(".empty").addClass("active").find(".clickHere").show();
            } else {
                this.cardData = {};
                this.catchClick = false;
                this.$("#board_" + GameData.me.uid).find(".empty").removeClass("active").find(".clickHere").hide();
            }
        },
        /**
         * Place creature html to deck
         */
        animateCallCard: function(data) {

            var $html, logMsg, infoData, liHolder;

            if (data.creatureName && data.element && data.boardIndex && this.collection.get(data.creatureName)) {
                infoData = _.extend(this.collection.get(data.creatureName).toJSON(), data);
                $html    = this.cardTemplate(infoData);

                this.boards[data.owner.uid].find("li:eq(" + (data.boardIndex - 1) + ")").replaceWith($html);;
                liHolder = this.boards[data.owner.uid].find("li:eq(" + (data.boardIndex - 1) + ")");
                liHolder.css("left", this.leftOffsets[data.boardIndex]).data("index", data.boardIndex);
                liHolder.find('.disabledCard').remove();
                if (!infoData.abilityElement || data.owner.uid != GameData.me.uid) {
                    liHolder.find('a.spell').remove();
                }

                if (data.alreadyCast) {
                    liHolder.find('a.spell').hide();
                }

                liHolder.animate({"opacity": 1}, 1500);
                if (!data.skipLoging) {
                    logMsg = { type: "game", actor: "player", action: "callMonster",
                        actorName: data.owner.login, internalName: data.creatureName,
                        showName: infoData.realName, time: (new Date()).valueOf()
                    };
                    Mediator.trigger('log-entry', logMsg);
                }

                //hide green slots
                this.showFreeSeats(false);

            }
        },
        /**
         * Animate card moving through the board after cast spell
         */
        animateCastSpell: function(data) {
            var stylesToApply = {
                my      : {left: 884, opacity: 0, top: 355},
                oponent : {left: 16,  opacity: 0}
            };


            var styleKey   = (this.boards[data.uid].data('owner') == 'playerTwo') ? "my" : "oponent";

            $("#pageLayout").append(this.cardTemplate(this.collection.get(data.name).toJSON()))
                .css(stylesToApply[styleKey])
                .animate({left: 400, top: 150, opacity: 1, "z-index": 9999}, 1500, function (data) {
                    setTimeout(function () {
                        //$htmlSpell.html("").remove();
                    }, 1000)
                });

            /*var logMsg = { type: "game", actor: "player", action: "castSpell", actorName: data.login,
                internalName: data.name,
                showName: this.collection.get(data.name).toJSON().realName,
                time: (new Date()).valueOf()
            }
            Mediator.trigger('log-entry',logMsg);*/

            Mediator.trigger('log-entry', data.login + " casted " + data.magic);

        },
        animateDieCard: function() {

        },
        /**
         * When creature cast  button shows
         */
        creatureCast: function() {
            console.log('creature-cast');
        },
        /**
         * Performs html manipulations at the each step begining
         */
        startRound: function(){

        },
        endRound: function(me) {
            if(!me) {
                this._showAllSpellBtns();
            }
        },
        _showAllSpellBtns: function() {
            this.$('a.spell').show();
        },
        /**
         * Fire when member trying to call card to free slot
         * @param {Object} event
         */
        callCard : function(event) {
            if(this.catchClick) {
                var index = this.$(event.currentTarget).data('cell');
                Mediator.trigger('choose-slot', _.extend(this.cardData, {index: index}), this.$(event.currentTarget).data('cell'))
            }
        }
    });
});
