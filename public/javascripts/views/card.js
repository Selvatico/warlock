/**
 * View for log bar user
 */
define([
    'jquery',
    'underscore',
    'backbone',
    'service/helper',
    'service/mediator'
], function ($, _, Backbone, Helper, Mediator) {

    return Backbone.View.extend({


        // Cache the template function for a single item.
        template   : Helper.getTemplate('#card-tpl'),

        playerData : {},

        cardData: {},

        // The DOM events specific to an item.
        events: {},

        /**
         * @param config
         * @param config.card Data about card
         */
        initialize: function (config) {
            if (config.card) {
                this.cardData = config.card;
            }
        },
        serialize: function(){
            return this.cardData;
        }
        /*,
        render: function() {
            $(this.el).html(this.template(this.cardData));
            return this;
        }*/
    });
});
