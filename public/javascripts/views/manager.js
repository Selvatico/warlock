define([
    'jquery',
    'underscore',
    'layoutManger',
    'service/mediator'
], function ($, _, LayoutManager, Mediator) {

    return LayoutManager.extend({

        template: _.template($("#layout-tpl").html()),

        events: {
            "click .atack": "endMove",
            "click .surrender": "surrender"
        },
        beforeRender: function () {
            //console.log('before:render', arguments);
        },
        initialize: function () {
            Mediator.on('switch-overlay',      this.switchHoverOverlay,        this);
            Mediator.on('connection-lost',     this.conenctionLostOverlay,     this);
            Mediator.on('connection-restored', this.connectionRestoredOverlay, this);
            Mediator.on('user-alert',          this.userAlert, this);
            Mediator.on('end-round',           this.switchEndRound, this);
        },
        /**
         * Show deck overlay
         * @param {Boolean} toggle Inicates if we need show or hide overlay
         * @param {String} text Text to place on overlay
         * @param {Boolean} [removeLoader] If need remove round circle
         */
        switchHoverOverlay: function(toggle, text, removeLoader){
            if(removeLoader) {
                this.$("#overLoader").remove();
                this.$("#overBtn").show();
            }
            this.$("#mainText").html(text).css({"letter-spacing" : "7px;"});
            if (toggle) {
                this.$(".sOverlay").show();
            } else {
                this.$(".sOverlay").hide();
            }
        },
        conenctionLostOverlay: function(){
            this.switchHoverOverlay(true, "Your connection to server lost. Waiting...");
        },
        connectionRestoredOverlay: function() {
            this.switchHoverOverlay(true, "Connection restored. Page will be refreshed in 3 sec.");
        },
        endMove: function () {
            Mediator.trigger('end-move');
        },
        surrender: function () {
            Mediator.trigger('surrender');
        },
        userAlert: function(msg){
            alert(msg);
        },
        switchEndRound: function(me) {
            if(!me) {
                this.$("#endMove").show();
            } else {
                this.$("#endMove").hide();
            }
        }

    });
});