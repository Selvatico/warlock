/**
 * View for single user
 */
define([
    'jquery',
    'underscore',
    'backbone',
    'service/helper',
    'service/mediator'
], function ($, _, Backbone, Helper, Mediator) {

    return Backbone.View.extend({

        tagName: 'div',

        // Cache the template function for a single item.
        template   : Helper.getTemplate('#player-tpl'),

        // The DOM events specific to an item.
        events: {
            "click .elements li": "switchElement"
        },

        // The TodoView listens for changes to its model, re-rendering.
        initialize: function () {
            Mediator.on('set-move', this.setMoveCircle, this);
            Mediator.on('end-move', this.endMove, this);
            Mediator.on('disconnect-player', this.disconnectPlayer, this);
            Mediator.on('connect-player',    this.connectPlayer, this);
            Mediator.on('set-resource:'     +  this.model.get('uid'), this.setElementCount, this);
            Mediator.on('change-hitpoints:' +  this.model.get('uid'), this.changeHitpoints, this);
        },

        serialize: function () {
           return this.model.toJSON();
        },
        endMove: function() {
           this.$(".whoMove").hide();
        },
        disconnectPlayer: function(data){
            this.$('#playerData_' + data.uid).find('.net') .removeClass("redFont greenFont").addClass("redFont").html("Disconnected");
        },
        connectPlayer: function(data){
            this.$('#playerData_' + data.uid).find('.net') .removeClass("redFont greenFont").addClass("redFont").html("Disconnected");
        },
        /**
         * Swtich elements hadler
         */
        switchElement: function(event) {
            this.$(".elements li").removeClass("active");
            var elementName = $(event.currentTarget).data('element');
            $(event.currentTarget).addClass("active");
            //say to pocket view to change displayed items
            Mediator.trigger('switch-element', elementName);
        },
        changeHitpoints: function (data) {
            var sign         = (data.dmg) ? "-" : "+",
                myId         = this.model.get('uid'),
                colorBadge   = (data.dmg) ? "badge-important" : "badge-success",
                cnt          = data.dmg || data.add,
                speed        = Math.random() * 1000,
                $playerData  = this.$("#playerData_" + myId),
                leftC        = (this.model.get('me') == 1) ? '214px' : '17px',
                direction    = (this.model.get('me') == 1) ? '-45px' : '50px',
                logMsg       = (data.dmg) ? "received " + cnt + " points of damage" : "healed for" + cnt + " points ";

            setTimeout(function () {
                $('<span class="badge playerLivesBadge">' + sign + cnt + '</span>')
                    .insertAfter("#playerData_" + myId + " .heart")
                    .addClass(colorBadge)
                    .css({"font-size" : "20px", "left" : leftC})
                    .animate({opacity: 1, top: direction}, 1500, function () {
                        $(this).remove();
                    });
            }, speed);

            //log change hp
            Mediator.trigger('log-entry', {type: "game", actor: "player", actorName: data.login, msg: logMsg});
            //set nu,ber in UI
            $playerData.find(".playerStatus span").html(data.hp);

        },
        setMoveCircle: function(turn) {
            this.$('#turn_' + turn).show();
        },
        setElementCount: function (elementData, allResources) {
            if (elementData.el && elementData.cnt >= 0) {
                this.$("li." + elementData.el + "").find(".count").html(elementData.cnt);
                Mediator.trigger('change-elements', allResources)
            }
        }
    });
});
