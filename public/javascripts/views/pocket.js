/**
 * View for user pocket
 */
define([
    'jquery',
    'underscore',
    'backbone',
    'service/helper',
    'service/mediator'
], function ($, _, Backbone, Helper, Mediator) {

    return Backbone.View.extend({

        tagName: 'div',

        // Cache the template function for a single item.
        template   : Helper.getTemplate('#pocket-tpl'),
        infoTpl    : Helper.getTemplate('#info-tpl', true),

        collection : null,

        // The DOM events specific to an item.
        events: {
            'click .cardsWrap .deckItem'       : 'toggleSelected',
            "mouseover .cardsWrap .deckList li": "switchSpellBtnOn",
            'click .cardsWrap a.spell'         : 'callCard'
        },
        setCollection: function (collection) {
            this.collection = collection;
            return this;
        },
        // The TodoView listens for changes to its model, re-rendering.
        initialize: function () {
            Mediator.on('switch-element',  this.switchElements, this);
            Mediator.on('change-elements', this.setAvalaibleCard, this);
        },
        /**
         * Action on call button click in the pocket and switch between cards call click
         * @param event
         * @returns {boolean}
         */

        //TODO: check spel click
        callCard: function (event) {
            var _self       = this,
                $target     = $(event.target),
                cardData    = $target.data("options"),
                $parentLi   = $target.parent('li'),
                $otherCards = _self.$(".cards li").not(".active"),
                //second click on this card decline selection and remove green fields from the deck
                clickSpell  = function clickSpell(event) {
                    if (event) {
                        event.stopPropagation();
                    }
                    $parentLi.off("click", ".spell", clickSpell).removeClass("active");
                    $parentLi.find(".spell").html("Call");
                    //say to deck holder to hide avaible slots
                    Mediator.trigger('click-cast', false);
                },
                //if member click on spell button of another card. Reset this card and set as acive another card
                setToAll = function setToAll(){
                    $otherCards.off("click", ".spell", setToAll);
                    clickSpell();
                };

            if (cardData && cardData.n && cardData.e && this.collection.get(cardData.n)) {
                //if card type spell just trigger event to cast it
                if (cardData.type && 'spell' == cardData.type) {
                    Mediator.trigger('cast-spell', cardData);
                    return true;
                }

                $parentLi.on("click", ".spell", clickSpell).addClass("active");
                $parentLi.find(".spell").html("Cancel");
                $otherCards.on("click", ".spell", setToAll);

                //say to deck holder to show avaible places for call
                Mediator.trigger('click-cast', true, cardData);
            }
            return true;
        },
        /**
         * Switches displyed cards in the pocket only for one element
         * @param {String} element
         */
        switchElements: function (element) {
            this.$(".cardsWrap").find(".h1d").hide();
            this.$(".cardsWrap").find("#" + element +"Slider").show();
        },
        /**
         * switch selected cards in the pocket and show info tpl
         * @param {Object} event
         */
        toggleSelected: function(event) {
            this.$('.deckItem').removeClass('selected');
            this.$(event.currentTarget).toggleClass('selected');
            this.showInfoPanel(event);
        },
        /**
         * Show info panel at the right side of the pocket
         * @param event
         */
        showInfoPanel : function(event) {
            var name = $(event.currentTarget).data('real');
            this.$("#infoHolder").html(this.infoTpl(this.collection.get(name).toJSON()));
        },
        switchSpellBtnOn: function (event) {
            this.$('.cardsWrap a.spell').hide();
            this.$(event.currentTarget).find("a.spell").show();
        },
        addCardsToPocket: function(cardsList, CardView) {
            var cardView;
            if (cardsList) {
                for (var card in cardsList) {
                    if (cardsList.hasOwnProperty(card)) {
                        cardView = new CardView({card: cardsList[card]});
                        this.$("#" + cardsList[card].element + "Slider .deckList").append(cardView.render().view.el);
                    }
                }
            }
            //show cards
            this.$(".cardsWrap").find(".h1d").hide();
            this.$(".cardsWrap").find("#waterSlider").show();

            //activate carousel
            this.$('.cards').jCarouselLite({
                btnNext: ".next",
                btnPrev: ".prev",
                mouseWheel: true,
                circular: false,
                visible: 3
            });
        },
        setAvalaibleCard: function(resources) {
            var _self = this;
            $.each(this.$(".cards li"), function(index, element){
                var $card    = _self.$(element),
                    costs    = $card.data("costs"),
                    elem     = $card.data("elem"),
                    disabler = $card.find(".disabledCard");
                if (costs && elem) {
                    if (costs <= resources[elem]) {
                        disabler.hide();
                    } else {
                        disabler.show();
                    }
                }
            });
        }
    });
});
