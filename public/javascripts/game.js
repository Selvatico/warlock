var app = app || {};
var ENTER_KEY = 13;
var process = {nextTick : function () {}};

require.config({
    baseUrl: '/javascripts/',
    paths: {
        jquery          : 'lib/jquery',
        underscore      : 'lib/underscore',
        backbone        : 'lib/backbone',
        text            : 'lib/require/require-text',
        layoutManger    : 'lib/backbone.layoutmanager',
        plugins         : 'lib/my_plugins'
    },
    shim: {
        underscore: {
            exports: '_'
        },
        backbone: {
            deps: ['underscore', 'jquery'],
            exports: 'Backbone'
        },
        layoutManger : {
            deps: ['backbone', 'jquery'],
            exports: 'Backbone.Layout'
        },
        plugins : {
            deps: ['jquery']
        },
        mainjs  : {
            deps: ['jquery']
        }
    }
});

/**
 * Pre load libs and set some service settings
 */
require(['jquery', 'underscore', 'backbone', 'layoutManger', 'plugins', 'workflow/gameplay'], function ($, _, b, LayoutManager, p, gameplay) {

    _.templateSettings = {
        interpolate : /\{\{(.+?)\}\}/g,
        evaluate    : /\{%(.+?)%\}/g,
        escape      : /\{-(.+?)-\}/g
    };
    console.log(arguments)


    LayoutManager.configure({
        // This allows for Underscore templating
        // https://github.com/tbranyen/backbone.layoutmanager#asynchronous--synchronous-fetching
        fetch: function(html) {
            'use strict';
            return _.template(html);
        },
        // This allows Views and LayoutViews to be interchangable, avoids much confusion
        // https://github.com/tbranyen/backbone.layoutmanager#structuring-a-view
        manage: true
    });

    gameplay.start(GameData);

});

