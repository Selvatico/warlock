define([
    'jquery',
    'underscore',
    'backbone'
], function ($, _, Backbone) {
    return  Backbone.Model.extend({
        idAttribute: "name",
        constructor: function () {
            var rawData = arguments[0];
            if (arguments[0].baseParams) {
                rawData = _.extend(arguments[0].baseParams, {text: arguments[0].text});
                rawData.realName = rawData.text.realName;
            }
            Backbone.Model.apply(this, [rawData, arguments[1]]);
        }
    });
});