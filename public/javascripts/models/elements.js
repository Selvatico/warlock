define([
    'cls/Water',
    'cls/Fire',
    'cls/Earth',
    'cls/Death',
    'cls/Air',
    'cls/Life',
    'langs/description'
], function (Water, Fire, Earth, Death, Air, Life, description) {
    var elements = {
            water : Water,
            fire  : Fire,
            earth : Earth,
            death : Death,
            air   : Air,
            life  : Life
        },
        baseConf;

    for (var element in elements) {
        if (elements.hasOwnProperty(element)) {
            baseConf = elements[element].baseConfig;
            for (var creature in baseConf) {
                if (baseConf.hasOwnProperty(creature)) {
                    baseConf[creature].text = description[creature];
                }
            }
        }
    }

    return elements;

});