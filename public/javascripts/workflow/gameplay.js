/**
 * Main workflow
 */
define([
    'underscore',
    'views/manager',
    'views/player',
    'views/log',
    'views/pocket',
    'views/deck',
    'service/mediator',
    'models/elements',
    'views/card',
    'collections/cards',
    'cls/CardsHero',
    'cls/Hero',
    'service/helper',
    'service/socket.io',
    'models/player'
], function(_, AppView, PlayerView, LogView, PocketView, DeckView, Mediator, Elements, CardView, CardsCollection, CardsHero, Hero, Helper, SocketManager, PlayerModel){
    var GameWorkflow =  {

        events: {
            'end-move'   : 'endMove',
            'surrender'  : 'surrender',
            'choose-slot': 'callCard',
            'socket-data': 'catchSocketData'
        },

        layoutManger: null,

        cardCollection: null,
        /**
         * @type {CardsHero}
         */
        gameObject:{},
        /**
         * @type {Hero}
         */
        myHeroObject: {},

        /**
         * @type {Number}
         */
        myID: {},


        bindEvents: function () {
            _.each(this.events, function bindEvents(value, key){
                Mediator.on(key, GameWorkflow[value], GameWorkflow);
            });
        },
        start : function  (GameData) {

            this.bindEvents();

            Mediator.trigger('start-init');

            var me           = GameData.me,
                oponent      = GameData.oponent,
                gameData     = GameData.itself,
                newcreatures = {},
                arrayToSort,
                allCards,
                tplData = {
                oponent: { className: 'playerOne', showTurn: (oponent.color != gameData.currentTurn) ? 'hide' : '', me:0, uid: oponent.uid},
                me: {className: 'playerTwo', showTurn: (me.color != gameData.currentTurn) ? 'hide' : '', me:1, uid: me.uid}
            };

            allCards = _.extend({},
                Elements.water.baseConfig,
                Elements.fire.baseConfig,
                Elements.earth.baseConfig,
                Elements.life.baseConfig,
                Elements.death.baseConfig,
                Elements.air.baseConfig
            );


            //create main layout
            var Layout = new AppView();

            //set nested views
            Layout.setView(".playerTwo", new PlayerView({model : new PlayerModel(_.extend(me, tplData.me))}), true);
            Layout.setView(".playerOne", new PlayerView({model : new PlayerModel(_.extend(oponent, tplData.oponent))}), true);
            Layout.setView("#logHolder", new LogView(), true);
            Layout.setView("#pocketHolder", new PocketView(), true);
            Layout.setView(".battleground", new DeckView(), true);

            //insert html
            Layout.$el.appendTo("#main-holder");
            Layout.render();

            this.layoutManger = Layout;
            this.cardCollection= CardsCollection;

            //sort cards by their cost
            arrayToSort = Helper.sortCards(me, Elements);

            $.each(arrayToSort, function (i, creature) {
                newcreatures[creature] = Elements[me.cards[creature]].baseConfig[creature];
                if (newcreatures[creature].baseParams) {
                    newcreatures[creature] = newcreatures[creature].baseParams;
                    newcreatures[creature].type = 'creature';
                } else {
                    newcreatures[creature].type = 'magic';
                }
                newcreatures[creature].realName = Elements[me.cards[creature]].baseConfig[creature].text.realName;
                newcreatures[creature].name = creature;
                newcreatures[creature].element  = me.cards[creature];
            });



            CardsCollection.add(_.values(allCards));

            //build player pocket
            Layout.getView('#pocketHolder')
                .setCollection(CardsCollection)
                .addCardsToPocket(newcreatures, CardView);

            Layout.getView('.battleground')
                .setCollection(CardsCollection)
                .initBoards();

            //init server side game and players objects
            this.initGameObjects();

            //set conenction to server
            SocketManager.connect(function () {
                SocketManager._sendSocket("jp", {id: GameData.gameID});
            });


        },

        checkGameStatus:function () {
            if (this.gameObject.gameStatus == "run") {return true;}
            else {Mediator.trigger('user-alert', "Game was paused. Waiting another player");return false;}
        },

        checkCanCall:function () {
            if (!this.myHeroObject.alreadyCast) {return true;}
            else {Mediator.trigger('user-alert', "You already call monster or cast a spell at this move");return false;}
        },

        checkTurn:function () {
            if (this.gameObject.currentTurn == this.myHeroObject.color) {return true;}
            else {Mediator.trigger('user-alert', "It is not your turn now!");return false; }
        },

        initGameObjects: function() {
            var _self = this,
                gamePlayers,
                tempPlayerObj,
                bHolder,
                syncConfig;

            //init game class
            _self.gameObject = new CardsHero();
            _self.gameObject.setConfig(GameData.itself);

            //init players classes
            var oponentPlayer = new Hero(GameData.oponent);
            oponentPlayer.gameLink = _self.gameObject;

            var myPlayer = new Hero(GameData.me);
            myPlayer.gameLink = _self.gameObject;

            _self.gameObject.players[oponentPlayer.uid] = oponentPlayer;
            _self.gameObject.players[myPlayer.uid] = myPlayer;

            //make copy of object poiner
            _self.myHeroObject = _self.gameObject.players[GameData.me.uid];
            _self.myID = GameData.me.uid;

            gamePlayers = _self.gameObject.players;

            //iterate through all players and place their cards to board
            for (var player in gamePlayers) {
                if (gamePlayers.hasOwnProperty(player)) {
                    for (var bIndex in gamePlayers[player].boardHolder) {
                        bHolder = gamePlayers[player].boardHolder;
                        if (bHolder.hasOwnProperty(bIndex)) {
                            if (bHolder[bIndex]) {
                                //memory shortcut
                                tempPlayerObj = _self.gameObject.getPlayerObj(player);

                                //ref to server sync config
                                syncConfig = bHolder[bIndex];
                                //emulate call card
                                tempPlayerObj.callCard(syncConfig.creatureName, bIndex, false, syncConfig);
                                //set current creature config
                                tempPlayerObj.boardHolder[bIndex].setConfig(syncConfig);
                            }
                        }
                    }
                }
            }

            //disable in the pocket card which can't be called due not enough elements
            Mediator.trigger('change-elements', _self.myHeroObject.elementsResource);

            //fill log with saved msgs
            _self.gameObject.reinitUILog();

            //show right circle with current move msg
            Mediator.trigger('set-move', _self.gameObject.currentTurn);
        },

        callCard:function (data) {
            var cardInfo,
                params = {},
                _self = this;

            if (!_self.checkTurn() || !_self.checkGameStatus() || !_self.checkCanCall()) {return false;}

            if (data.e && data.n) {
                //get card info
                cardInfo = this.cardCollection.get(data.n).toJSON();

                //spell or not a spell
                if (cardInfo.type == "spell") {
                    cardInfo.creatureName = data.n;
                }
                //make a move
                if (cardInfo) {
                    if (_self.myHeroObject.canPay(data.e, cardInfo.costs)) {
                        params.a = "cc";
                        params.d = [data.n, data.index];
                        Helper.ajax("/game/move", params, function (response) {
                            if (response.r) {
                                cardInfo = _.extend(cardInfo, {me:true, creatureName: data.n, boardIndex: data.index});
                                _self.socketAction.syncGameAction({sync : "callCard", args : [cardInfo.creatureName, data.index ], i : _self.myID });
                            } else {
                                console.error(response);
                            }
                            console.log("callCardResponse", response);
                        });
                    } else {
                        Mediator.trigger('user-alert', "You have not enoigh resources to call card!");
                    }
                }
            }
            return true;
        },
        //TODO: finsih cast creature
        /*castCreature:function (index) {

            if (!CardsUI.gameActions.checkTurn()) return false;

            var _self = CardsUI,
                castCreature = _self.myHeroObject.boardHolder[index],
                selector,
                castFuntion,
                $targetElements,
                $creatureElement = CardsUI.myBoardHolder.find("li:eq(" + (index -1) +")"),
                cancelFunc,
                classToAdd,
                boardToEvent;

            if (castCreature.alreadyCast) return false;
            classToAdd = (castCreature.castTarget.indexOf("enemy") == -1) ? "glowToCallGreen" : "glowToCallRed";
            boardToEvent = (castCreature.castTarget.indexOf("enemy") == -1) ? "myBoardHolder" : "oponentBoardHolder";

            *//**
             * Exec after selecting target
             *//*
            castFuntion = function clickCast() {
                var allowToCast,
                    config = {ind : castCreature.boardIndex};

                //UI staff
                CardsUI[boardToEvent].off("click", "." + classToAdd, castFuntion);

                //don't click if no need
                if (castCreature.castTarget != "none") {
                    $creatureElement.find("a.spell").click();
                    $creatureElement.removeClass("selected");
                }

                //perform casting
                if (castCreature.abilityCost != 0) {
                    if (castCreature.owner.canPay(castCreature.abilityElement, castCreature.abilityCost)) {
                        allowToCast = true;
                    } else {
                        CardsUI.service.alert("You have not enoigh resources to call card!");
                    }
                } else {
                    allowToCast = true;
                }

                if (arguments.length > 0) {
                    config.target = (CardsUI[boardToEvent].find("li").index($(this))) + 1;
                }

                CardsUI.ajax("/game/move", {a : "cca", d : [config]}, function (response) {
                    if (response.r) {
                        if (allowToCast) {
                            castCreature.castAbility(config);
                            $(".popover").remove()
                            $creatureElement.find("a.spell").hide()
                        }
                    }
                });
            };

            CardsUI[boardToEvent].off("click", "." + classToAdd, castFuntion);
            $("." + classToAdd).removeClass(classToAdd);

            //function to revert selection and other UI changes
            cancelFunc = function cancelCasting(event) {
                if ($targetElements) { $targetElements.removeClass(classToAdd);  }
                $creatureElement.off("click", "a.spell", cancelFunc);

                $(this).html("Cast");

                if (event) {
                    event.stopPropagation();
                }
            };

            if (castCreature.castTarget != "none") {
                selector = castCreature.getCastSelector();
                console.log("SELECTOR", selector);
                if (selector != "") {
                    $targetElements = $(selector);
                    if (selector.length > 0) {
                        $targetElements.addClass(classToAdd);
                        CardsUI[boardToEvent].on("click", "." + classToAdd, castFuntion);
                        $creatureElement.on("click", "a.spell", cancelFunc).find("a.spell").html("Cancel");
                        $creatureElement.addClass("selected");
                    }
                }
            } else {
                castFuntion();
            }
        },*/

        endMove: function() {
            var _self = this;
            Helper.ajax("/game/move", {a:'em', d: []}, function (response) {
                if (response.r) {
                    _self.socketAction.syncGameAction({sync : "endMove", args : [], i : _self.myID });
                }
            });
        },
        surrender: function(){
            var _self = this;
            Helper.ajax("/game/move", {a:'sr', d: []}, function (response) {
                if (response.r) {
                    _self.socketAction.syncGameAction({sync : "surrender", args : [], i : _self.myID });
                }
            });
        },
        /**
         * Catch all socket packets to control game
         * @param actionName
         * @param data
         */
        catchSocketData: function(actionName, data) {
            console.log('SOCKET-DATA: ', data);
            if (typeof this[actionName] == "function") {
                this[actionName](data);
            } else if (typeof this.socketAction[actionName] == "function") {
                this.socketAction[actionName](data);
            }
        },
        socketAction:{
            /**
             * Universal method to sync state between server side and client side
             * @param {Object} syncData
             */
            syncGameAction : function (syncData) {
                var playerObj;

                if (syncData.sync) {
                    playerObj =  GameWorkflow.gameObject.getPlayerObj(syncData.i);
                    playerObj[syncData.sync].apply(playerObj, syncData.args);

                    //Just in case if we need to perform some special operations
                    if (GameWorkflow.socketAction[syncData.sync + "Action"]) {
                        GameWorkflow.socketAction[syncData.sync + "Action"](syncData, playerObj);
                    }
                } else {
                    throw Error("Wrong sync params");
                }
            },
            endMoveAction:function (data, player) {
                Mediator.trigger('log-entry', {type : "game", actor : "player", actorName: player.login, msg : " end his move"});
                Mediator.trigger('set-move', GameWorkflow.gameObject.currentTurn);
                Mediator.trigger('end-round', (data.i == GameWorkflow.myID));
            },
            dieCreatureAction:function (data) {

            },
            disconnectAction:function (data) {
                Mediator.trigger('log-entry', {type : "error", msg : "Oponent disconnected. Pause the game..."});
                Mediator.trigger('disconnect-player', data);
                GameWorkflow.gameObject.gameStatus = 'pause';
            },
            connectAction:function (data) {
                Mediator.trigger('log-entry',{type : "info", msg : ((data.uid == GameWorkflow.myID) ? "Connection established. Continue game..." : "Oponent reconected. Continue game..")});
                Mediator.trigger('connect-player', data);
                GameWorkflow.gameObject.gameStatus = 'run';
            },
            continueAction:function () {
                Mediator.trigger('switch-overlay', false, '');
            },
            startMoveAction:function () {
                Mediator.trigger('switch-overlay', false, '');
            },
            startAction : function () {
                Mediator.trigger('log-entry', {type : "info", msg : "All players connected. Start the game.."});
                GameWorkflow.gameObject.gameStatus = 'run';
                Mediator.trigger('switch-overlay', false, '');
            }
        }
    };
    window.GameW = GameWorkflow;
    return GameWorkflow;
});