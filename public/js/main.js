$(document).ready(function(){
	$('.elements li, .cardsWrap .prev, .cardsWrap .next').hover(
		function(){
            if($(this).parent().parent().hasClass('playerTwo')){
                $(this).animate({ top: -3 }, 'fast');
            }else{
                $(this).animate({ top: 3 }, 'fast');
            }
        },
		function(){ $(this).animate({ top: 0 }, 'fast'); }
	);

	$('.menu > li').hover(
		function(){ $(this).animate({ top: 2 }, 'fast'); },
		function(){ $(this).animate({ top: 0 }, 'fast'); }
	);

    $('.surrender').hover(
        function(){ $(this).animate({ bottom: 232 }, 'fast'); },
        function(){ $(this).animate({ bottom: 230 }, 'fast'); }
    );
    $('.atack').hover(
        function(){ $(this).animate({ top: 208 }, 'fast'); },
        function(){ $(this).animate({ top: 210 }, 'fast'); }
    );

	$('a[href="#logIn"]').click(function() {
		$(this).parent().parent().css('visibility','hidden').animate({opacity:0});
		$('nav').animate({width:643});
		$('.formLogin').css('visibility','visible').animate({opacity:1});

		return false;
	});

	$('.log .show_log').live("click",function() {
		$(this).toggleClass('expanded');
		var block = $('.log ul');
		if ($(this).hasClass('expanded')) {
			block.css('overflow-y','auto').animate({ height:98 },'normal');
		} else {
			block.css('overflow-y','hidden').animate({ height:32 },'normal', function(){
                $(".log ul").scrollTop(9999);
            });
		}
		return false;
	});

    $('#advanced_search').click(function () {
        $(".advancedSearch").slideDown();
    });

});

