CardsUI.templates = {
    buildPocket : function (data) {
        /*var info = CardsUI.creatureInfo,
            newcreatures = {};

        var arrayToSort = Object.keys(data.cards).sort(function (a, b) {
            var aData = info[data.cards[a]][a],
                bData = info[data.cards[b]][b];

            aData = (aData.baseParams) ? aData.baseParams : aData;
            bData = (bData.baseParams) ? bData.baseParams : bData;

            if (aData.costs == bData.costs) return 0;
            if (aData.costs < bData.costs) return -1;
            if (aData.costs > bData.costs) return 1;
        });

        $.each(arrayToSort, function (i, creature) {
            newcreatures[creature] = data.cards[creature];
        });

        data.cards = newcreatures;

        var tplData = {cards : {}},
            infoShort,
            pocketPart,
            ejsTpl = new EJS(
                {
                    text:'<li class="deckItem <%= e %> <%= t %> <%= rn %>" data-real="<%= rn %>" data-costs="<%= c %>" data-elem="<%= e %>">' +
                        '<span class="image">' +
                        '<img src="<%= staticPath %>/img/cards/<%= rn %>.jpg" alt=""/></span>' +
                        '<i class="element borderRadius_5"></i>' +
                        '<i class="level"><%= c %></i>' +
                        '<i class="monsterHealth"><%= l %></i>' +
                        ' <i class="atack"><%= a %></i>' +
                        '<a class="spell" href="#" data-options=\'{"n":"<%= rn %>", "e":"<%= e %>", "type":"<%= t %>"}\'>' +
                        '<% if (t == "magic") {%> Cast <% } else { %> Call <% } %>' +
                        '</a>' +
                        '<p class="name"><%= n %></p><div class="disabledCard"></div>' +
                        '</li>'
                }
            ),
            closureList = {},
            $deckList = $(".cardsWrap").find(".deckList");

        for (var i in data.cards) {
            if (data.cards.hasOwnProperty(i)) {
                infoShort = CardsUI.creatureInfo[data.cards[i]][i];
                if (infoShort.type && infoShort.type == "spell") {
                    tplData = {
                        e:infoShort.element, c:infoShort.costs, l:"",
                        a:"", n:CardsUI.creatureInfo.TEXT_INFO[i].realName, t:"magic", rn:i
                    };
                } else {
                    tplData = {e:infoShort.baseParams.element, c:infoShort.baseParams.costs,
                        l:infoShort.baseParams.lives, a:infoShort.baseParams.attackPower,
                        n:CardsUI.creatureInfo.TEXT_INFO[i].realName, t:"", rn:i
                    };
                }
                pocketPart = ejsTpl.render(tplData);
                closureList[i] = tplData;
                $("#" + tplData.e + "Slider .deckList").append(pocketPart);

            }
        }
        $(".cardsWrap").find(".h1d").hide();
        $(".cardsWrap").find("#waterSlider").show();

        jQuery(function($) {
            $(".cards").jCarouselLite({
                btnNext: ".next",
                btnPrev: ".prev",
                mouseWheel: true,
                circular: false,
                visible: 3
            });
        });

        $(".cardsWrap").on("click",".deckItem", function () {
            if (!$(this).hasClass('selected')){
                $('.deckItem').removeClass('selected')
                $(this).addClass('selected');
            }
        });*/

        /*$("body").on("click",".deckItem", function () {

            var realName = $(this).data("real"),
                element = $(this).data("elem"),
                tplToInfo,
                $infoPanel = $(".cardInfo"),
                displayData;

            if (!realName) return true;

            if (!closureList[realName]) {
                var infoShortCl = CardsUI.creatureInfo[element][realName].baseParams;
                closureList[realName] = {
                    e:infoShortCl.element, c:infoShortCl.costs, l:infoShortCl.lives, a:infoShortCl.attackPower,
                    n:CardsUI.creatureInfo.TEXT_INFO[realName].realName, t:"", rn: realName
                }
            }

            tplToInfo = ejsTpl.render(closureList[realName]).replace("disabledCard", "");
            $infoPanel.find(".deckItem").html("").remove();
            displayData = CardsUI.creatureInfo.TEXT_INFO[realName]

            //set name and infoshow casting and abilities
            if (closureList[realName].t != "magic") {
                $infoPanel.find("h4:eq(0)").html(displayData['realName']).show();
                $infoPanel.find("p.ability").html(displayData['ability']).show();
            } else {
                $infoPanel.find("h4:eq(0)").hide();
                $infoPanel.find(".ability").hide();
            }


            //set spell
            $infoPanel.find("p.spell").html(displayData['cast']);

            //replace avatar
            $infoPanel.prepend(tplToInfo);
            $infoPanel.find("a.spell").hide();

            if (!$infoPanel.is(":visible")) {$infoPanel.show();}
        });*/
    },
    constructSingle:function (data) {
        return new EJS(
            {
                text:'<li class="deckItem <%= element %> <%= creatureName %>" data-real="<%= creatureName %>" data-elem="<%= element %>">' +
                    '<span class="image">' +
                    '<img src="<%= staticPath %>/img/cards/<%= creatureName %>.jpg" alt=""/></span>' +
                    '<i class="element borderRadius_5"></i>' +
                    '<i class="level"><%= costs %></i>' +
                    '<i class="monsterHealth"><%= lives %></i>' +
                    ' <i class="atack"><%= attackPower %></i>' +
                    '<% if (abilityElement) {%> ' +
                    '<a <% if(alreadyCast) {%> style="display:none;" <%}%> class="spell" href="#"> Cast </a>' +
                    '<% } %>' +
                    '<p class="name"><%= CardsUI.creatureInfo.TEXT_INFO[creatureName].realName %></p>' +
                    '</li>'
            }
        ).render(data);
    },
    getOneSpell : function (data) {
        return new EJS(
            {
                text:'<li class="deckItem <%= element %> magic <%= name %>" data-real="<%= name %>" data-elem="<%= element %>">' +
                    '<span class="image">' +
                    '<img src="<%= staticPath %>/img/cards/<%= name %>.jpg" alt=""/></span>' +
                    '<i class="element borderRadius_5"></i>' +
                    '<i class="level"><%= costs %></i>' +
                    '<i class="monsterHealth"></i>' +
                    ' <i class="atack"></i>' +
                    '<p class="name"><%= CardsUI.creatureInfo.TEXT_INFO[name].realName %></p>' +
                    '</li>'
            }
        ).render(data);
    },
    infoTpl : function (data) {
        return new EJS(
            {
                text:'<div class="cardInfo">' +
                    'PIC_HOLDER' +
                    '<h4><%= realName %></h4>' +
                    '<p class="ability"><%= ability %></p>' +
                    '<h4>Casting:</h4>' +
                    '<p class="spell" href="#"><%= cast %></p>' +
                    '</div>'
            }
        ).render(data);
    }
};

/*<!--<script type="text/template" id="battle-card-tpl">
 <li class="deckItem <%= element %> <%= creatureName %>" data-real="<%= creatureName %>" data-elem="<%= element %>">
 <span class="image">
 <img src="<%= staticPath %>/img/cards/<%= creatureName %>.jpg" alt=""/></span>
 <i class="element borderRadius_5"></i>
 <i class="level"><%= costs %></i>
 <i class="monsterHealth"><%= lives %></i>
 <i class="atack"><%= attackPower %></i>
 <% if (abilityElement) {%>
 <a <% if(alreadyCast) {%> style="display:none;" <%}%> class="spell" href="#"> Cast </a>
 <% } %>
 <p class="name"><%= CardsUI.creatureInfo.TEXT_INFO[creatureName].realName %></p>
 </li>
 </script>-->*/

