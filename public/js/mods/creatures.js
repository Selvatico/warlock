CardsUI.creatureInfo = {
    water: require("Water").baseConfig,
    fire: require("Fire").baseConfig,
    air:require("Air").baseConfig,
    earth:require("Earth").baseConfig,
    life:require("Life").baseConfig,
    death:require("Death").baseConfig,

    TEXT_INFO : {
        //Water
        snowbear : {
            realName : "Snow Bear",
            cast : "None",
            ability : "Squashing attack - if attacks a creature with health less than 10, kills it instantly. -1 damage from all attacks."
        },
        nixie : {
            realName : "Nixie",
            cast : "Casting Sea of Sacred increases owner's Water by 1 and reduces Fire by 1.",
            ability : "Causes 200% of damage to fire creatures. Gives owner 1 Water in the beginning of owner's turn."
        },
        iceDefender : {
            realName : "Ice Defender",
            cast : "None",
            ability : "Reduces all damage done to owner by 50%. Suffers 200% damage from fire."
        },
        sealord : {
            realName : "SeaLord",
            cast : "None",
            ability : "Every time anyone casts Water spell or summons Water creature, opponent suffers 4 damage and owner gains 2 health."
        },
        seadragon : {
            realName : "Sea Dragon",
            cast : "None",
            ability : " When attacking, each enemy creature suffers 1 damage in addition to standard attack."
        },
        elemental : {
            realName : "Elemental",
            cast : "None",
            ability : "One of the toughest Elementals. Heals itself for 3 whenever any player casts water spell or summons water creature(NOT). Attack equal to owner's Water."
        },
        gydra : {
            realName : "Gydra",
            cast : "None",
            ability : "Strikes not only oposite creatures, but and nearest creatures too"
        },
        icewizzard : {
            realName : "Ice Wizzardr",
            cast : "None",
            ability : "Increases Water by 2 every turn. Suffers 200% damage from fire. All damage from Water equal to 1."
        },
        octopusman : {
            realName : "OctopusMan",
            cast : "NOT INMPLMENTED",
            ability : "NOT IMPLEMENTED"
        },
        icewarrior : {
            realName : "Frozen",
            cast : "Stomp deals 2 damage to opposite creature. Cost 1 Water.",
            ability : "When attacks a creature with level bigger than 5, deals double damage."
        },
        icewall : {
            realName : "Ice Wall",
            cast : "Shift - cost 1 Water, and you can shift the position with any friendly creature. This creature loses 1 health. - NOT INMPLMENTED",
            ability : "When enemy creature attacks it, deals 2 damage to this creature. Loses 2 health each turn."
        },
        deathrage : {
            realName : "DEATH RAGE",
            cast : "Owner's creature are affected by Death Rage effect - they gain doubled attack, but they will die in the beginning of the next turn.",
            ability : "For one final strike before we die.."
        },

        //EARTH
        satyr : {
            realName : "Satyr",
            cast : "Once Satyr casts Dissolve, it dies and creature in the opposed slot suffers 5 damage. If there's no creature, damage dealt to enemy player.",
            ability : "Increases Earth by 1 every turn."
        },
        forestspirit : {
            realName : "ForestSpirit",
            cast : "Casts Youth of Forest, increasing owner player's health by 5. Costs two Earth elements.",
            ability : "Damage from all non-magical attacks and abilities equal to 1."
        },
        dryad : {
            realName : "Dryad",
            cast : "NOT INMPLMENTED",
            ability : "Add +1 to damage for nearbly creature, if nearbly is earth creature add +2. When owner call earth creature - heal Dryad for 2"
        },
        dispercer : {
            realName : "Dispercer",
            cast : "None",
            ability : "Whenever dispercer dies, owner restores 1 of each element."
        },
        golem : {
            realName : "Stone Golem",
            cast : "None",
            ability : "Regenerates 3 health every turn. While owner's Earth less than 3, it suffers 3 damage instead."
        },
        kobold : {
            realName : "Kobold",
            cast : "None",
            ability : "Blocks all damage less than 4. When attacks enemy player, reduces his Earth by 1."
        },
        centaur : {
            realName : "Centaur",
            cast : "Strikes oponent for 2. Costs 1 earth",
            ability : "Attacks the same turn he was summoned(No summon sickness)."
        },
        ent : {
            realName : "Ent",
            cast : "Casts Entangle Roots, damaging each enemy for 1 and losing 2 points of own health.",
            ability : "Attack both : creature and oponent"
        },
        elf : {
            realName : "Elf",
            cast : "Fires arrow to enemy creature, dealing 6 damage.",
            ability : "Never attacks directly. Can only shoot arrows, using casting ability"
        },
        aciddragon : {
            realName : "Acid Dragon",
            cast : "",
            ability : "When attacks, poisons her target. This target will lose 2 health every turn"
        },
        woodbeast : {
            realName : "Wood Beast",
            cast : "NOT IMPLEMNTED",
            ability : "Attack equal to owner earth-3. Receive +10 damage from fire spells"
        },
        spider : {
            realName : "Giant Spider",
            cast : "Casts Spider Web, preventing enemy creature from attack in its' turn. Costs 2 Earth. Does not prevent Enemy creature from casting.",
            ability : "When summoned, permanently gains +3 to attack if owner has other Earth creatures in play."
        },


        //SPELLS cards
        absolutedefence : {
            realName: "Absolute Defence",
            cast : "Owner's creatures gain protection from all attacks. This defence only lasts one turn and lasts till next owner's turn.",
            ability : " It's just like an unpenetrable wall has suddenly appeared. Anyone under your command will survive anything!"
        },
        acidstorm : {
            realName: "Acid Storm",
            cast : "Each creature suffers up to 16 points of damage. If a player has Sea Lord on a field, his creatures left unaffected.",
            ability : "Amazingly poisonous magic storm, has no mercy to both friends and foes."
        },
        airshield : {
            realName: "AirShield",
            cast : "Your creatures are protected by air shield - all damage to them is reduced by 2.",
            ability : "Handy thing. Many wizards use it all the time."
        },
        armageddon : {
            realName: "ARMAGEDDON",
            cast : "All units on a field suffer 25 damage. Each player suffers 25 damage.",
            ability : "The ultimate spell of all Orion wizards. The strongest and most harmful. Beware, it's far too powerful!"
        },
        armorofgod : {
            realName: "ARMOR OF GOD",
            cast : "Owner's creatures are blessed, healed by 6, negative buffs are removed, their attack strength permanently increases by 1.",
            ability : "With this armor you are closer to Gods themselves.."
        },
        blackwind : {
            realName: "BLACK WIND",
            cast : "Winds away the strongest enemy creature.",
            ability : "Perfect against high-level enemy creatures. One of the most useful spells."
        },
        bless : {
            realName: "BLESS",
            cast : "All owner's creatures Blessed: receive +1 to attack, (===restore 1 point of health every time they are hit== - this part not). Undead creatures cannot be blessed and suffer 10 damage instead.",
            ability : "Your army's now under God's protection, and your enemy is doomed forever!"
        },
        blind : {
            realName: "blind",
            cast : " Enemy creatures are blinded - they lose 3 attack each, and skip their next turn.",
            ability : "Allright, you are now on your own in this new world of darkness."
        },
        bloodritual : {
            realName: "BLOOD RITUAL",
            cast : " You gain 6 Fire. As a cost, your creatures suffer 3 damage each.",
            ability : "It is nice if you don't have any creatures, actually. Gives such a nice feeling of freedom."
        },
        lighting : {
            realName: "CHAIN LIGHTNING",
            cast : " First enemy creature suffers damage equal to owner's Air+2. Lightning travels forth and hits each enemy creature, losing 2 damage each time it hits.",
            ability : "For example, if owner has 10 Air and enemy has all 5 creatures, they suffer this damage (left to right): 12-10-8-6-4"
        },
        chaosvortex : {
            realName: "CHAOS VORTEX",
            cast : " Banishes each creature into hell. Each banished creature gives caster 1 Death.",
            ability : "Whenever one unfolds Chaos, no mortal can stand its fearful ugly nature."
        },
        coverofdarkness : {
            realName: "COVER OF DARKNESS",
            cast : "All living creatures suffer 13 damage. All undead creatures heal for 5.",
            ability : "The Lord of Chaos most useful tool. Your army of darkness shall reign forever."
        },
        curse : {
            realName: "CURSE",
            cast : "Reduces all enemy elements by 1.",
            ability : "Curse and Doom are now your enemy's only guests."
        },
        earthchant : {
            realName: "EARTH CHANT",
            cast : "Increases owner's Earth by 8, decreasing other elements as much as 1 of each.",
            ability : "Concentrate all your will on Earth, and Earth will be your destiny!"
        },
        earthquake : {
            realName: "EARTHQUAKE",
            cast : "Hits each creature for 15 damage. Doesn't affect owner's creatures, if onwer's Earth > 12.",
            ability : "Even the earth itself is a powerful weapon."
        },
        fireball : {
            realName: "FIREBALL",
            cast : "Each enemy creature suffers damage equal to owner's Fire + 3.",
            ability : "As easy as it is - a ball of burning fire."
        },
        firespikes : {
            realName: "FIRE SPIKES",
            cast : "Deals 3 damage to each enemy creature.",
            ability : "Cheap and still good. Pure Fire."
        },
        flamingarrow : {
            realName: "FLAMING NET",
            cast : "If enemy has less Fire than owner does, enemy suffers damage, equal to this difference, multiplied by 2. Otherwise enemy suffers 1 damage.",
            ability : " Now this is a smart one - a magic arrow made of pure Fire, never misses your foe."
        },
        lifesacrifice : {
            realName: "LIFE SACRIFICE",
            cast : "Owner loses health equal to his Life. Enemy suffers damage, double of this amount.",
            ability : ""
        },
        icebolt : {
            realName: "ICE BOLT",
            cast : "Inflicts 10 + Water/2 damage to enemy player. Caster suffers 6 damage as a side effect.",
            ability : " Large bolt of Ice, fired at a great speed. Superior efficiency!"
        },
        //20.11.12
        empower : {
            realName: "EMPOWER",
            cast : "Permanently increases attack of your creatures by 2.",
            ability : "Just how good you are? Doesn't matter, you're now twice as good."
        },
        forbiddenmagic : {
            realName: "FORBIDDEN MAGIC",
            cast : "Your enemy loses 5 Fire elements, while you suffer 5 damage as a cost for using these forbidden techniques.",
            ability : ""
        },
        forceofnature : {
            realName: "FORCE OF NATURE",
            cast : "Deals damage to each enemy creature equal to half of it's remaining health. No effect to Earth creatures.",
            ability : ""
        },
        godswrath : {
            realName: "GODS WRATH",
            cast : "All undead on a field are destroyed. Owner receives 3 Life and 1 health for each destroyed creature.",
            ability : ""
        },
        /*guardian : {
            realName: "GUARDIAN",
            cast : "Unstable form of this guardian creature forces him to lose 4 health each turn.",
            ability : ""
        },*/
        healingwind : { //checked
            realName: "HEALING WIND",
            cast : "Owner gains health, equal to the number of creatures in play. +3 bonus health for each Air creature.",
            ability : ""
        },
        lightningbolt : {//checked
            realName: "LIGHTNING BOLT",
            cast : "Hits enemy for 6 health.",
            ability : ""
        },
        moonlight : {//checked
            realName: "MOONLIGHT",
            cast : "Heals all creatures to maximum. Affects enemy creatures.",
            ability : ""
        },
        paralize : {//checked
            realName: "PARALYZE",
            cast : "Paralyzes enemy player and creatures for one turn, so they skip one turn.",
            ability : ""
        },
        infernal : {//checked
            realName: "Infernal",
            cast : "",
            ability : "Whenever comes in play, you lose one of your Fire creatures. If you have none of them, Infernal loses half of the initial health."
        },
        plague : {//cheked
            realName: "PLAGUE",
            cast : "Every creature on a field plagued - loses all hit points except one. Ignores all defences and modifiers.",
            ability : ""
        },
        polymorph : {//checked
            realName: "POLYMORPH",
            cast : "Turns enemy creatures with level less than 8 into Satyrs. These creatures, however, will retain health and attack.",
            ability : ""
        },
        quicksands : {//checked
            realName: "QUICKSANDS",
            cast : "Kills all enemy creatures of level less than 5.",
            ability : ""
        },
        unholyword : {
            realName: "UNHOLY WORD",
            cast : "All living creatures with level less than 6 become Zombies.",
            ability : ""
        },
        walloffire : {//cheked
            realName: "Wall of Fire",
            cast : "When summoned, deals 4 damage to each enemy creature. Loses 4 health each turn.",
            ability : ""
        },
        tsunami : {
            realName: "TSUNAMI",
            cast : "Deals damage to all enemy creatures, equal to their level.",
            ability : ""
        },
        solitude : {//cheked
            realName: "SOLITUDE",
            cast : "If you have just one creature on your side, permanently increases attack of this creature by 5. Otherwise fails.",
            ability : ""
        },
        rejuvenation : {//cheked
            realName: "REJUVENATION",
            cast : " Heals owner equal to his Life Element*3. Owner loses all Life elements. ",
            ability : ""
        },
        starfall : {
            realName: "STARFALL",
            cast : "Deals damage to each enemy creature, equal to amount of enemy creatures on a field.",
            ability : ""
        },
        wingsofwisdom : {
            realName: "WISDOM WINGS",
            cast : "Grants owner +2 to all elements, except Air.",
            ability : ""
        },
        revival : {
            realName: "REVIVAL",
            cast : "Heals all friendly creatures for 4. Gives owner 2 health for each of his creatures on a field.",
            ability : ""
        },
        spellbreaker : {
            realName: "SPELLBREAKER",
            cast : "Owner's creatures become permanently immune to all damaging spells, spell effects, and poison." +
                "Remember that your creatures can no longer be affected by Bless, Restructure and other good spell effects.",
            ability : ""
        },

        //Fire
        demon : {
            realName : "Demon",
            cast : "Whenever Demon casts Fire Breeds owner loses 1 Earth and receives 2 Fire elements.",
            ability : "Doesn't suffer from Fire and Earth spells."
        },
        firelord : {
            realName : "Fire Lord",
            cast : "Increase friendly unit attack for +2, but this unit will receive 2 damage atevery start move",
            ability : "Add +2 to fire to owner and oponent each move"
        },
        reddrake : {
            realName : "Red Drake",
            cast : "None",
            ability : "When summoned, each enemy suffers 3 damage. Red Drake Suffers no damage from Fire spells and creatures."
        },
        hellriser : {
            realName : "Hell Riser",
            cast : "Sacrificing owner's Fire creature gives 3 Fire to the owner, also healing owner by this amount.",
            ability : "Damage from Water is multiplied by 2. Whenever hellriser dies, owner suffers 10 damage."
        },
        berserk : {
            realName : "Berserk",
            cast : "Deal 4 damage to oposite creature, but suffer 4 damage",
            ability : "Suffers +2 damage from all attacks."
        },
        succubus : {
            realName : "Succubus",
            cast : "None",
            ability : "Add +2 to attack to all owner creature. After die  - attack bonus removed"
        },
        fireshaman : {
            realName : "FireShaman",
            cast : "Casts Shuriken to deal 2 damage directly to enemy player.",
            ability : "When someone hurts FireShaman, it roars in fury, dealing 1 damage to each enemy creature."
        },
        efreet : {
            realName : "Efreet",
            cast : "Casts Fire Shield on any owner's creature. Costs 2 Fire. Fire Shield burns creature from inside, damaging it for 2 points per turn, unless it's a Fire creature.",
            ability : "Whenever any creature attacks Efreet, that creature suffers half of damage sent back (same applies to Fire Shield spell). " +
                "Upon summoning, all enemy Water creatures suffer 6 damage. - NOT"
        },
        vulcano : {
            realName : "Vulcano",
            cast : "Casts Volcano Explode. Vulcan dies, but every unit on field suffers damage equal to 50% of Vulcan's health.",
            ability : "Fire Elemental. Immune to harmful Fire spells. When summoned, enemy player loses 3 Fire, and opposed Vulcan unit suffers 9 damage. Attack equal to owner's Fire + 3."
        },


        //Air
        fairy : {
            realName : "Fairy",
            cast : "Target enemy creature will attack owner",
            ability : "Increase own attack for +1 when some creature die in the battle"
        },
        nymph : {
            realName : "Nymph",
            cast : "None",
            ability : "Owner receives 1 Air at the beginning of Owners turn."
        },
        gargoyle : {
            realName : "Gargoyle",
            cast : "Casts Stone Skin to itself and will receive -2 less damage. Costs 3 air and 1 earth",
            ability : "No damage from air and earth spells"
        },
        phoenix : {
            realName : "Phoenix",
            cast : "None",
            ability : "If was killed by fire spell or fire creature - will revive"
        },
        soulofwinds : {
            realName : "Soul of Winds",
            cast : "None",
            ability : "Each move lose 20 HP. Damage from attacks = 20"
        },
        manticore : {
            realName : "Manticore",
            cast : "Remove ability to cast from enemy creature. Costs 2 air.",
            ability : "Damage from magic reduced to 50%. Deal +3 more damage to creatures which can cast"
        },
        zeus : {
            realName : "Zeus",
            cast : "Deal 8 damage to enemy creature with level less than 7. Costs 2 Air",
            ability : "If attack Lord of light - instantly kill him"
        },
        lordlight : {
            realName : "Lord of light ",
            cast : "Deal damage equal to owner amount of air to enemy creature. Costs 1 Air",
            ability : "If attack Zeus - instantly kill him"
        },
        titan : {
            realName : "Titan",
            cast : "Deal 3 damage to all enemy Earth creatures. Costs 1 Air",
            ability : "After call reduce oponent Air for -3. After call to battle add +1 to own attack for each Air creature in the game"
        },
        kronos : {
            realName : "Kronos",
            cast : "Set max HP of enemy creature to 50%, and reduce it to this level. ",
            ability : "Attacks creatures of a level less than 10 with additional damage, equal to the level difference. "
        },
        astralcloud : {
            realName : "Astral Dragon",
            cast : "None",
            ability : "Receive -2 less damage from attacks. Reduce costs of casting spell for -2(NOT IMPLEMENTED)"
        },


        //Life
        magichealer : {
            realName : "Magichealer",
            cast : "None",
            ability : "Whenever owner player loses health, Magic Healer heals player by this amount, losing hit points equally"
        },
        priest : {
            realName : "Priest",
            cast : "None",
            ability : "Increases owner's Life by 2 every turn, decreasing Death by the same amount. Decreases owner's Life by 3 every time owner casts Death spells."
        },
        swordsman : {
            realName : "Swordsman",
            cast : "None",
            ability : "This trained fighter hurts everyone who attacks him by 2. Cannot attack Life creatures."
        },
        apostale : {
            realName : "Apostale",
            cast : "Serves Death. Once cast, Apostate permanently turns into a Banshee. Banshee restores only 1/2 of normal health.",
            ability : "Steals 2 owner's Life and gives owner 1 Death in the beginning of owner's turn."
        },
        pegasus : {
            realName : "Pegasus",
            cast : "Holy Strike deals 5 damage to a target creature. If it is undead creature, Pegasus also suffers 3 damage himself. Costs 2 Life.",
            ability : "When summoned, each owner's creature is healed for 3. Also, it destroys harmful spell effects from each of them.(buffs not IMPLEMEMNTED)"
        },
        monk : {
            realName : "Monk",
            cast : "True Prayer on enemy creature reduces this creature's attack by 2 permanently (to no less than 1 ). Cost 1 Life.",
            ability : "Heals friendly creatures by 1 each turn. Heals owner by 2 each turn."
        },
        paladin : {
            realName : "Paladin",
            cast : "Casts Exorcizm. Destroys any undead, but suffers 10 damage himself. Owner also loses 2 Life as a cost of this holy casting.",
            ability : "Brings 300% of damage to undead creatures."
        },
        unicorn : {
            realName : "Unicorn",
            cast : "Casts Unicorn Aura. This Aura destroys useful spell effects from enemy creatures. Costs 2 Life.",
            ability : "Unicorn reduces damage from spells to owner's creatures by 50%. Cures poison from owner's creatures.(NOT IMPLEMEMNTED)"
        },
        chimera : {
            realName : "Chimera",
            cast : "None",
            ability : "When Chimera is on a field, every spell casting costs 50% less for the owner(NOT IMPLEMEMNTED). Whenever you summon creature, you gain health equal to this creature's level."
        },
        etherial : {
            realName : "etherial",
            cast : "None",
            ability : "Suffers no damage from direct attacks and casting abilities, only spells can harm him. If Life or Earth creature is summoned, Ethereal heals owner for 3."
        },



        //Death
        skeleton : {
            realName : "Skeleton",
            cast : "None",
            ability : "If oponent creatures have less than 7 lives - deal +2 more damage"
        },
        ghost : {
            realName : "Ghost",
            cast : "Casts Bloody Ritual. As a result, owner loses 5 health, but receives one Death.",
            ability : " Whenever attacked by a creature, suffers 50% less damage, and owner suffers other 50% damage. When suffers from spell, Ghost recieves 200% of normal damage."
        },
        shadow : {
            realName : "Shadow",
            cast : "Blend. Choose friendly creature - this creature is healed by 10 and gains +10 maximum health. Shadow dies.",
            ability : "All damage to Shadow is reduced by 50% to no less than 1. Suffers extra 5 damage from Life creatures' attacks."
        },
        zombie : {
            realName : "Zombie",
            cast : "None",
            ability : "When kill creature increase own max hit points and restore it"
        },
        evilsoccer : {
            realName : "Evil Sorccer",
            cast : "Burning Hands. Deals 2 damage to opposite creature. Cost 1 Death.",
            ability : "Magical attack, always equal 5. No damage from magic, unless it's another Evil Sorcerer."
        },
        werewolf : {
            realName : "Werewolf",
            cast : "Casts Blood Rage on self. Strikes twice as hard this turn, but owner loses 3 Death points on casting.",
            ability : "When dies, becomes a ghost."
        },
        banshee : {
            realName : "Banshee",
            cast : "None",
            ability : "When summoned, deals 8 damage to enemy. Once it attacks enemy player, dies and enemy player suffers 10 points of extra damage. If Banshee dies from other creature or spell, enemy player doesn't suffer."
        },
        assasin : {
            realName : "Assasin",
            cast : "Assassinate. Can instantly kill any creature, if it has less than 10 health. Cost 4 Death.",
            ability : " If opposed to a creature, deals additional 4 damage."
        },
        darklord : {
            realName : "Dark Lord",
            cast : "Steal Spell steals all spell effects from any enemy creature, DarkLord receives these enchants. Owner loses 1 Death. - NOT",
            ability : "Whenever creature dies, Darklord heals owner for 3 and regenerates self for 2."
        },
        leach : {
            realName : "War Leach",
            cast : "Casts Death Bolt, hitting enemy player with 7 of damage. Owner loses 5 Death. If owner's Death becomes zero, he suffers 10 damage himself.",
            ability : "When summoned, deals 10 damage to creature in the opposite slot and two adjacent slots. Attacks Life units with additional 5 damage."
        },
        vampire : {
            realName : "Vampire",
            cast : "None",
            ability : "Restore half of damage dealed to enemy"
        },
        grimreaper : {
            realName : "Grim Reaper",
            cast : "Consumes target enemy creature of level 3 or less. Owner player loses 3 Death elements.",
            ability : "Whenever owner creature dies, increases owner's Death by one."
        },
        darkdruid : {
            realName : "Dark Druidr",
            cast : "NONE",
            ability : "Whenever enemy creature dies, heals owner to a number of life, equal to level of destroyed creature. Doesn't work for undead creatures."
        },
        enchanter : {
            realName : "Enchanter",
            cast : "By casting Refill regenerates self for 4. Owner loses 2 Earth.",
            ability : "Heals Earth creatures by 1 each turn. If his health becomes equal to 1, dissolves to surround, dying and healing owner's creatures for 5."
        },
        //done
        enchanter : {
            realName : "Enchanter",
            cast : "By casting Refill regenerates self for 4. Owner loses 2 Earth.",
            ability : "Heals Earth creatures by 1 each turn. If his health becomes equal to 1, dissolves to surround, dying and healing owner's creatures for 5."
        },
        //done
        steallife : {
            realName : "STEAL LIFE",
            cast : "If owner's Death less than 8, steals 5 health from enemy player. Otherwise steals Death + 5.",
            ability : ""
        },
        totalweakness : {
            realName : "TOTAL WEAKNESS",
            cast : "Every enemy creature suffers effect of Weakness: its attack decreased by 50% (rounded down).",
            ability : ""
        },
        restructure : {
            realName : "RESTRUCTURE",
            cast : "All owner's creatures gain +3 health to their maximum, healing for 6 in the same time.",
            ability : ""
        },
        hermit : {
            realName : "Samurai",
            cast : "When attacking, can instantly kill enemy creature, if its level equal 5.",
            ability : "Gives target creature a buff - Gift of Life. Costs 4 Life, and Samurai dies. Creature under this buff will rebirth after it died."
        },
        valor : {
            realName : "Valor",
            cast : "$Healing Cloud: Heals self by 5, friendly creatures by 2, ^and enemies by 1. ^Cost 3 Life.",
            ability : "Whenever attacks Death creature, heals self by 4. Also heals by 1 each turn. When attacking, deals damage, equal to 50% ^of his actual"
        },
        tigerspirit : {
            realName : "Tiger Spirit",
            cast : "Kill creature if it has health less or equal 5.",
            ability : " Whenever it attacks a creature, this creature permanently loses 1 attack, if its attack is more than 2"
        },
        tryton : {
            realName : "Tryton",
            cast : "Heal all friendly units equal to health left. Dies from this. Cannot cast if cannot attack this turn.",
            ability : "Increases its attack by 1 each turn. Losses 2 Health each turn."
        },
        wavepower : {
            realName : "Wave Power",
            cast : "Two first weakest enemy creatures die. Doesn't affect creatures of level 6 or higher.",
            ability : ""
        },
        seastrike : {
            realName : "Sea Striker",
            cast : "Every enemy	creature suffers damage	equal to its attack	-1",
            ability : ""
        },
        undeadstrike : {
            realName : "Undead strike",
            cast : "Each owner's undead attacks opposite creature, if there is one.",
            ability : ""
        },
        damnation : {
            realName : "Damnation",
            cast : "Affects all enemy creatures. Increases damage, dealt to these creatures, by 2. Permanent buff.",
            ability : ""
        }
    }
};



