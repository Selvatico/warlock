var ff = false;
CardsUI.gameActions = {

    ajax:CardsUI.ajax,
    INITGAME : function (){

        //todo check this block

        var _self = CardsUI,
            Hero = loadClass("Hero"),
            gamePlayers,
            tempPlayerObj,
            bHolder,
            syncConfig;

        //init game class
        _self.gameObject = new CardsHero();
        _self.gameObject.setConfig(GameData.itself);

        //init players classes
        var oponentPlayer = new Hero(GameData.oponent);
        oponentPlayer.gameLink = _self.gameObject;

        var myPlayer = new Hero(GameData.me);
        myPlayer.gameLink = _self.gameObject;

        _self.gameObject.players[oponentPlayer.uid] = oponentPlayer;
        _self.gameObject.players[myPlayer.uid] = myPlayer;

        //make copy of object poiner
        _self.myHeroObject = _self.gameObject.players[GameData.me.uid];
        _self.myID = GameData.me.uid;

        gamePlayers = _self.gameObject.players;

        //iterate through all players and place their cards to board
        for (var player in gamePlayers) {
            if (gamePlayers.hasOwnProperty(player)) {
                for (var bIndex in gamePlayers[player].boardHolder) {
                    bHolder = gamePlayers[player].boardHolder;
                    if (bHolder.hasOwnProperty(bIndex) && bHolder[bIndex]) {
                        //memory shortcut
                        tempPlayerObj = _self.gameObject.getPlayerObj(player);

                        //ref to server sync config
                        syncConfig = bHolder[bIndex];
                        //emulate call card
                        tempPlayerObj.callCard(syncConfig.creatureName, bIndex, false, syncConfig);
                        //set current creature config

                        tempPlayerObj.boardHolder[bIndex].setConfig(syncConfig);
                    }
                }
            }
        }
        CardsUI.gameActions.checkCardAvalaible();
        _self.gameObject.reinitUILog();
        $("#turn_" + CardsUI.gameObject.currentTurn).show();
    },
    checkCardAvalaible : function () {
        var resources = CardsUI.myHeroObject.elementsResource;
        $.each($(".cards li"), function(index, element){
            var costs = $(element).data("costs"),
                elem = $(element).data("elem"),
                disabler = $(element).find(".disabledCard");
            if (costs && elem) {
                if (costs <= resources[elem]) {
                    disabler.hide();
                } else {
                    disabler.show();
                }
            }
        });
    },
    checkTurn:function () {
        if (CardsUI.gameObject.currentTurn == CardsUI.myHeroObject.color) {
            return true;
        } else {
            CardsUI.service.alert("It is not your turn now!");
            return false;
        }
    },
    checkGameStatus:function () {
        if (CardsUI.gameObject.gameStatus == "run") {
            return true;
        } else {
            CardsUI.service.alert("Game was paused. Waiting another player");
            return false;
        }
    },
    checkCanCall:function () {
        if (!CardsUI.myHeroObject.alreadyCast) {
            return true;
        } else {
            CardsUI.service.alert("You already call monster or cast a spell");
            return false;
        }
    },
    /**
     *
     * @param data
     * @param [callback]
     * @return {Boolean}
     */
    callCard:function (data, callback) {
        var cardInfo,
            allCardsInfo = CardsUI.creatureInfo,
            params = {},
            _self = CardsUI.gameActions;

        if (!_self.checkTurn() || !_self.checkGameStatus() || !_self.checkCanCall()) return false;

        if (data.e && data.n) {
            //get card info
            cardInfo = (allCardsInfo[data.e] && allCardsInfo[data.e][data.n]) ? allCardsInfo[data.e][data.n].baseParams : false;

            //spell or not a spell
            if (allCardsInfo[data.e][data.n].type && allCardsInfo[data.e][data.n].type == "spell") {
                cardInfo = allCardsInfo[data.e][data.n];
                cardInfo.creatureName = data.n;
            } else {
                cardInfo = allCardsInfo[data.e][data.n].baseParams;
            }

            //make a move
            if (cardInfo) {
                if (CardsUI.myHeroObject.canPay(data.e, cardInfo.costs)) {
                    params.a = "cc";
                    params.d = [data.n, data.index];
                    CardsUI.ajax("/game/move", params, function (response) {
                        if (response.r) {
                            cardInfo.me = true;
                            cardInfo.creatureName = data.n;
                            cardInfo.boardIndex = data.index;
                            CardsUI.gameActions.socketAction.syncGameAction(
                                {sync : "callCard", args : [cardInfo.creatureName, data.index ], i : CardsUI.myID });
                            CardsUI.canCall = false;
                        } else {
                            console.error(response);
                        }
                        console.log("callCardResponse", response);
                    });
                } else {
                    CardsUI.showMsg("You have not enoigh resources to call card!");
                }
            }
        }
    },
    endMove:function () {
        var params = {};
        params.a = 'em';
        params.d = [];
        CardsUI.ajax("/game/move", params, function (response) {
            if (response.r) {
                CardsUI.canCall = true;
                CardsUI.gameActions.socketAction.syncGameAction({sync : "endMove", args : [], i : CardsUI.myID });
                CardsUI.myBoardHolder.find("a.spell").show();
            }
        });
    },
    surrender : function () {
        var params = {};
        params.a = 'sr';
        params.d = [];
        CardsUI.ajax("/game/move", params, function (response) {
            if (response.r) {
                CardsUI.canCall = true;
                CardsUI.gameActions.socketAction.syncGameAction({sync : "surrender", args : [], i : CardsUI.myID });
            }
        });
    },
    /**
     *
     * @param index
     * @return {*}
     */
    castCreature:function (index) {

        if (!CardsUI.gameActions.checkTurn()) return false;

        var _self = CardsUI,
            castCreature = _self.myHeroObject.boardHolder[index],
            selector,
            castFuntion,
            $targetElements,
            $creatureElement = CardsUI.myBoardHolder.find("li:eq(" + (index -1) +")"),
            cancelFunc,
            classToAdd,
            boardToEvent;

        if (castCreature.alreadyCast) return false;
        classToAdd = (castCreature.castTarget.indexOf("enemy") == -1) ? "glowToCallGreen" : "glowToCallRed";
        boardToEvent = (castCreature.castTarget.indexOf("enemy") == -1) ? "myBoardHolder" : "oponentBoardHolder";

        /**
         * Exec after selecting target
         */
        castFuntion = function clickCast() {
            var allowToCast,
                config = {ind : castCreature.boardIndex};

            //UI staff
            CardsUI[boardToEvent].off("click", "." + classToAdd, castFuntion);

            //don't click if no need
            if (castCreature.castTarget != "none") {
                $creatureElement.find("a.spell").click();
                $creatureElement.removeClass("selected");
            }

            //perform casting
            if (castCreature.abilityCost != 0) {
                if (castCreature.owner.canPay(castCreature.abilityElement, castCreature.abilityCost)) {
                    allowToCast = true;
                } else {
                    CardsUI.service.alert("You have not enoigh resources to call card!");
                }
            } else {
                allowToCast = true;
            }

            if (arguments.length > 0) {
                config.target = (CardsUI[boardToEvent].find("li").index($(this))) + 1;
            }

            CardsUI.ajax("/game/move", {a : "cca", d : [config]}, function (response) {
                    if (response.r) {
                        if (allowToCast) {
                            castCreature.castAbility(config);
                            $(".popover").remove()
                            $creatureElement.find("a.spell").hide()
                        }
                    }
            });
        };

        CardsUI[boardToEvent].off("click", "." + classToAdd, castFuntion);
        $("." + classToAdd).removeClass(classToAdd);

        //function to revert selection and other UI changes
        cancelFunc = function cancelCasting(event) {
            if ($targetElements) { $targetElements.removeClass(classToAdd);  }
            $creatureElement.off("click", "a.spell", cancelFunc);

            $(this).html("Cast");

            if (event) {
                event.stopPropagation();
            }
        };

        if (castCreature.castTarget != "none") {
            selector = castCreature.getCastSelector();
            console.log("SELECTOR", selector);
            if (selector != "") {
                $targetElements = $(selector);
                if (selector.length > 0) {
                    $targetElements.addClass(classToAdd);
                    CardsUI[boardToEvent].on("click", "." + classToAdd, castFuntion);
                    $creatureElement.on("click", "a.spell", cancelFunc).find("a.spell").html("Cancel");
                    $creatureElement.addClass("selected");
                }
            }
        } else {
            castFuntion();
        }
    },
    socketAction:{
        /**
         * Universal method to sync state between server side and client side
         * @param {Object} syncData
         */
        syncGameAction : function (syncData) {
            console.warn("SYNC_GAME_STATE_CALL", syncData)

            var _self = CardsUI,
                game = _self.gameObject,
                playerId = syncData.i,
                playerObj;

            if (syncData.sync && loadClass("Hero").prototype.hasOwnProperty(syncData.sync)) {
                playerObj = game.getPlayerObj(playerId);
                playerObj[syncData.sync].apply(playerObj, syncData.args);

                //Just in case if we need to perform some special operations
                if (_self.gameActions.socketAction[syncData.sync + "Action"]) {
                    _self.gameActions.socketAction[syncData.sync + "Action"](syncData, playerObj);
                }
            } else {
                throw Error("Wrong sync params");
            }
        },
        /**
         * @deprecated
         * @param serverData
         */
        callcardAction:function (serverData) {
            console.warn("socket.callCard-POST", serverData);
        },
        endMoveAction:function (data, player) {
            $(".whoMove").hide();
            CardsUI.service.log({type : "game", actor : "player", actorName: player.login, msg : " end his move"});
            $("#turn_" + CardsUI.gameObject.currentTurn).show();
            if (data.i != CardsUI.myID) {
                $("#endMove").show();
                CardsUI.myBoardHolder.find("a.spell").show();
            } else {
                $("#endMove").hide();
            }
        },
        dieCreatureAction:function (data) {

        },
        disconnectAction:function (data) {
            console.warn("socket.disconnect.action", data.cr);
            var another = (CardsUI.myHeroObject.color == data.cr) ? "playerTwo" : "playerOne";
            $("." + another + " .net").removeClass("redFont").removeClass("greenFont").addClass("redFont").html("Disconnected");
            CardsUI.gameObject.gameStatus = "pause";
            CardsUI.service.log({type : "error", msg : "Oponent disconnected. Pause the game..."});
        },
        connectAction:function (data) {
            var msg = "";
            var another = (CardsUI.myHeroObject.color == data.cr) ? "playerTwo" : "playerOne";
            $("." + another + " .net").removeClass("redFont").removeClass("greenFont").addClass("greenFont").html("online");
            CardsUI.gameObject.gameStatus = "run";
            if (another == "playerOne") {
                 msg = "Oponent reconected. Continue game..";
            } else {
                msg  = "Connect established. Continue game...";
            }
            CardsUI.service.log({type : "info", msg : msg});

        },
        continueAction:function (data) {
            $(".sOverlay").hide();
            console.warn("socket.continue.action", data)
        },
        startMoveAction:function (data) {
            $(".sOverlay").hide();
            console.warn("socket.startMove.action", data);
        },
        startAction : function () {
            $(".sOverlay").hide();
            CardsUI.gameObject.gameStatus = "run";
            CardsUI.service.log({type : "info", msg : "All players connected. Start the game.."});
        }
    }

};

