"use strict";
//for IDE only
var fakeObject = {resources:1, gameId:1, off:function (a, b, c) {}};
var tttt;

//emulate server side code
var require = require || function (name, notStrict) {
    if (~name.indexOf("/")) {
        name = name.split("/");
        name = name[name.length - 1]
    }
    if (exports.hasOwnProperty(name)) {
        return exports[name];
    }
    if (!notStrict) {
        throw Error("Required Class `" + name + "` not found");
    }
};

//just a cap
var loadClass = require;

//emulate server side exports object
var exports = exports || {};

//emulate some server code
exports.util = {
    inherits:(function () {
        if (Object.create) {
            return function (ctor, superCtor) {
                ctor.super_ = superCtor;
                ctor.prototype = Object.create(superCtor.prototype, {
                    constructor:{
                        value:ctor,
                        enumerable:false,
                        writable:true,
                        configurable:true
                    }
                });
            };
        } else {
            return function (ctor, ctor2) {
                function F() {
                }

                F.prototype = ctor2.prototype;
                ctor.prototype = new F();
            }
        }
    })(),
    isArray:function (obj) {
        return Object.prototype.toString.call(obj) === '[object Array]';
    }
};
//cap for client side code
exports.events = {EventEmitter:((typeof io != "undefined") ? io.EventEmitter : {})};

//cap for client side code
var process = {nextTick : function () {}};

/**
 * {Object} GameData.resources
 * @type {*|Object}
 */
var GameData = GameData || {};

var CardsUI = (function () {

    var CONNECTED = 1,
        DISCONNECTED = -1,
        version = "0.0.2",
        offsetTopMap1,
        offsetTopMap2;

    var initer = {
        index:function () {
            $("#signInModal").delegate(".modal-footer .btn-primary", "click", function () {
                CardsUI.signIn();
            });
            $(".formLogin").on("click", "#logInBtn",  function () {
                $("#logInNav").submit();
            });
        },
        main:function () {

        },
        user_sign_in : function () {

        },
        game_find:function () {
            $("#pageLayout").delegate("#findGame", "click", function () {
                $("#findGame").addClass("disabled");
                $("#stopFind").removeClass("disabled");
                CardsUI.findGame();
            });

            $("#pageLayout").delegate("#stopFind", "click", function () {
                $("#stopFind").addClass("disabled");
                $("#findGame").removeClass("disabled");
                CardsUI.stopFind();
            });
            CardsUI.connectToLive();
        },
        game_find3 : function () {
            CardsUI.FindGame.init();
        },
        game_cards : function () {
            var infoList = CardsUI.creatureInfo;
           for (var element in CardsUI.creatureInfo) {
               if (element != "TEXT_INFO" && infoList.hasOwnProperty(element)) {
                   //
                   var elementName = '<div class="elements"><h2 class="' + element + '"><strong><span></span></strong>' + element.toUpperCase() +'</h2></div>';
                   $(".pageCards").append(elementName);
                   for (var card in infoList[element]) {
                       var dataForTpl = (infoList[element][card].baseParams) ? infoList[element][card].baseParams : infoList[element][card];
                       var mainTPl = CardsUI.templates.infoTpl(infoList.TEXT_INFO[card]);

                       dataForTpl.creatureName = card;
                       dataForTpl.name = card;
                       dataForTpl.alreadyCast = true;
                       dataForTpl.abilityElement = false;
                       if (!dataForTpl.lives) {
                           dataForTpl.lives = "";
                           dataForTpl.attackPower = "";
                       }

	                   var infoTpl = (dataForTpl.lives) ? CardsUI.templates.constructSingle(dataForTpl) : CardsUI.templates.getOneSpell(dataForTpl);
                       $(".pageCards").append(mainTPl.replace("PIC_HOLDER", infoTpl));
                   }
               }
           }

        },
        game_play2: function() {
            initer.game_play();
        },
        game_play : function () {
            var $deckList = $(".cardsWrap .deckList");

            //build pocket
            //CardsUI.templates.buildPocket(GameData.me);


            /*$deckList.find("li")
                .mouseover(function(){$(this).find("a.spell").show()})
                .mouseout(function(){$(this).find("a.spell").hide()});*/

            //elements switcher
            /*$(".playerTwo").find(".elements").delegate("li", "click", function () {
                $(".playerTwo").find(".elements li").removeClass("active");
                var elementName = $(this).data("elemetn");

                $(".cardsWrap").find(".h1d").hide();
                $(".cardsWrap").find("#" + elementName +"Slider").show();
                $(this).addClass("active")

            });*/

            /*$("body").delegate("#endMove","click", function () {
                console.log("END MOVE")
                CardsUI.gameActions.endMove();
            });*/

            /*$("body").delegate(".surrender","click", function () {
                console.log("surrender")
                if (confirm("Are you sure? If you will surrender - you lose")) {
                    CardsUI.gameActions.surrender();
                }
            });*/

            //call card logic

            /*$deckList.delegate("a.spell", "click", function () {

                if (!CardsUI.gameActions.checkTurn()) return false;

                var elData = $(this).data("options"),
                    $parentLi = $deckList.find("." + elData.n),
                    $playerDesk = $("#board_" + GameData.me.uid),
                    $otherCards = $(".cards li").not(".active"),
                    info;

                if (elData && elData.n && elData.e) {
                    if (CardsUI.creatureInfo[elData.e] && CardsUI.creatureInfo[elData.e][elData.n]) {
                        info = CardsUI.creatureInfo[elData.e][elData.n];
                        if (info.type && info.type == "spell") {
                            CardsUI.gameActions.callCard(elData);
                            return;
                        }

                        //place creature to desk
                        var placeToDesk = function placeToDesk() {
                            var $cloned = $parentLi.clone().css("border", "none"),
                                $clicked = $(this);
                            elData.index = $(this).data("cell");
                            clickSpell(false);
                            CardsUI.gameActions.callCard(elData, function () {
                                $cloned.find(".spell").html("Вызвать");
                                $clicked.replaceWith($cloned);
                            });
                        };

                        //switcher for call btn
                        var clickSpell = function clickSpell(event) {
                            if (event) {
                                event.stopPropagation();
                            }
                            $parentLi.off("click", ".spell", clickSpell).removeClass("active");
                            $parentLi.find(".spell").html("Call");
                            $playerDesk.off("click", ".empty", placeToDesk).find(".empty").removeClass("active").find(".clickHere").hide();
                        };

                        var setToAll = function setToAll(){
                            console.log("switch")

                            $otherCards.off("click", ".spell", setToAll);
                            clickSpell();
                        };

                        $parentLi.on("click", ".spell", clickSpell).addClass("active");
                        $parentLi.find(".spell").html("Cancel");
                        $otherCards.on("click", ".spell", setToAll);
                        $playerDesk.on("click", ".empty", placeToDesk).find(".empty").addClass("active").find(".clickHere").show();
                    }
                }
            });*/


            /*CardsUI.connectToLive(function () {
                CardsUI._sendSocket("jp", {id:GameData.gameID});
                CardsUI.socket.on("disconnect", function () {
                    $("#mainText").html("Your connection to server lost. Waiting...").css({"letter-spacing" : "7px;"});
                    $(".sOverlay").show();
                });

                CardsUI.socket.on("reconnect", function () {
                    $("#mainText").html("Connection restored. Page will be refreshed in 3 sec.").css({"letter-spacing" : "7px;"});
                    $(".sOverlay").show();
                    setTimeout(function(){
                        document.location.reload();
                    }, 3000);
                });
            });


            CardsUI.myBoardHolder = $(".deckTwo");
            CardsUI.oponentBoardHolder = $(".deckOne");*/

            CardsUI.myBoardHolder.delegate("a.spell", "click", function () {
                var pp = $(this).parent();
                var indexOfCast = pp.data("index");
                if (indexOfCast && indexOfCast > 0 && indexOfCast < 6) {
                     CardsUI.gameActions.castCreature(indexOfCast);
                }
            });
        }
    };

    return {
        version:version,
        /**
         * {io.Socket}
         */
        socket:null,
        gamestatus:false,
        connected:false,
        resources:{},
        //boardCards:{},
        currentTurn:false,
        canCall:false,
        /**
         * {CardsHero}
         */
        gameObject : null,
        /**
         * {Hero}
         */
        myHeroObject : null,

        myBoardHolder : null,
        oponentBoardHolder: null,
        //offsetTopPlayer:offsetTopMap1,
        //offsetBotPlayer:offsetTopMap2,
        _sendSocket:function (action, data) {
            data.a = action;
            this.socket.send(JSON.stringify(data));
            return this;
        },
        playAction:function (data) {
            if (data.d) {
                this.redirect("/game/play2/" + data.d);
            }
        },
        connectToLive:function (func) {
            var _self = this;
            if (_self.socket == null) {
                _self.socket = io.connect("http://" + document.location.host +"/", {"max reconnection attempts":5, 'flash policy port':10843});

                _self.socket.on('connect', function () {
                    console.log("connected");
                    _self.connected = true; if (func) {func();}
                });

                _self.socket.on("disconnect", function () {console.log("disconnect");_self.connected = false;});
                _self.socket.on('reconnect', function () {console.log("reconnect");});
                _self.socket.on('message', function (data) {
                    if (data) {
                        data = JSON.parse(data);
                        if (data && data.a) {
                            var actionName = data.a + "Action";
                            if (typeof _self[actionName] == "function") {
                                _self[actionName](data);
                            } else if (typeof CardsUI.gameActions.socketAction[actionName] == "function") {
                                CardsUI.gameActions.socketAction[actionName](data);
                            }
                        }
                    }
                });
            } else if (!_self.socket.socket.connected) {
                _self.socket.socket.reconnect();
            }
        },
        findGame:function () {
            var _self = this;
            var findGame = function () {
                _self._sendSocket("fg", {a:1})
            };


            if (_self.connected) {
                findGame();
            } else {
                _self.connectToLive(findGame);
            }
        },
        stopFind : function () {
            var _self = this;
            _self._sendSocket("sf", {a:1});
        },
        showMsg:function (msg, type) {
            type = type || "success";
            alert(msg);
        },
        redirect:function (href) {
            document.location.href = document.location.protocol + "//" + document.location.host + href;
        },
        bindControls:function (page) {
            if (initer[page] && typeof initer[page] == "function") {
                initer[page]();
            }
        },
        signIn:function () {
            var _self = this,
                params = {
                    e:$("#emailsignup").val(),
                    p:$("#passwordsignup").val(),
                    p2:$("#passwordsignup_confirm").val(),
                    n:$("#usernamesignup").val()
                },
                $alertError = $(".alert-error");
            $alertError.html('').addClass("hide");
            this.ajax("/user/sign_in", params, function (data) {
                if (data.r) {
                    _self.redirect("/");
                    $alertError.addClass("hide");
                } else {
                    if (data.error.length > 0) {
                        $alertError.html(data.error.join("<br />")).removeClass("hide");
                    }
                }
            });
        },
        logIn:function (username, pass) {
            var _self = this,
                usernameR = username || $("#lUsername").val(),
                passR = pass || $("#lPass").val(),
                params = {l:usernameR, p:passR};
            this.ajax("/user/log_in", params, function (data) {
                if (data.r) {
                    _self.redirect("/game/find3");
                } else {
                    _self.showMsg(data.msg);
                }
            });
        },
        formLogin : function () {
            this.logIn($("#lUsername2").val(), $("#lPass2").val())
        },
        /**
         * One point for all Ajax requests
         * @param url
         * @param params
         * @param [callback]
         * @param [type]
         */
        ajax:function ajaxHelper(url, params, callback, type) {
            $.post(url, params, callback, ((!type) ? "json" : type))
                .error(function() {})
                .complete(function(data, textStatus) {
                    if (textStatus == "error") {
                        var msg = JSON.parse(data.responseText);
                        if(msg) {
                            msg = "<span style='font-size: 20px;'>SERVER ERROR OCURED. Please report about it to <a href='https://github.com/Selvatico/warlock-issue/issues'>" +
                                "Bug tracker</a><br>"
                                + 'If you are on game page please find game page and start new once.<br></span> Error stack trace: '
                                + msg.error.message + "\n" + msg.error.stack + "\n Please send this info to admins";
                            console.error("====SERVER ERROR====", msg);
                            CardsUI.service.popup({html:msg});
                        }
                    }
                });
        }
    }
})();

/**
 *
 * @type {Object}
 */
CardsUI.service = {
    /**
     *
     * @param {String} msg Messsage text
     * @param {String} [type] Can be error, sucess, info
     */
    alert:function (msg, type) {
        alert(msg);
    },
    _getLogNameLink:function (n, rn) {
         return '<a style="text-decoration: underline" class="logName" data-realname="' + n + '">' + rn + '</a>';
    },
    /**
     *
     * @param data
     */
    log:function (data) {

        var color = "",
            msg = "",
            now =  (new Date()).toLocaleTimeString(),
            $logUl = $(".log ul");

        if (data) {
            if (data.type) {
                if (data.time) {
                    msg = (new Date(data.time)).toLocaleTimeString() + ": ";
                }
                switch (data.type) {

                    case "error":
                        if (data.msg) {
                            msg +=now  + ": <span style='color: red;'>" + data.msg + "</span>";
                        }
                        break;

                    case "info":
                        if (data.msg) {
                            msg += now + ": <span style='color: green;'>" + data.msg + "</span>";
                        }
                        break;

                    case "game":
                        if (data.actor) {

                            if (!data.time) {
                                msg += now + ": ";
                            }

                            if (data.actor == "player") {
                                msg += "Player <i>" + data.actorName + "</i> ";
                            } else if (data.actor == "monster") {
                                msg += '<a style="text-decoration: underline" class="logName" data-realname="' + data.internalName + '">' + data.showName + '</a>';
                            }
                            if (data.action == "callMonster") {
                                msg += 'call <a style="text-decoration: underline" class="logName" data-realname="' + data.internalName + '">' + data.showName + '</a> to battle';
                            } else if (data.action == "castSpell") {
                                msg += 'casted <a style="text-decoration: underline" class="logName" data-realname="' + data.internalName + '">' + data.showName + '</a>';
                            } else if (data.action == "deal") {
                                msg += this._getLogNameLink(data.attack.n, data.attack.rn) + " deal <span style='color:red;'>" + data.dmg + "</span> points of damage to " + this._getLogNameLink(data.internalName, data.showName);
                            } else if (data.action == "damaged") {
                                msg += this._getLogNameLink(data.internalName, data.showName) + " received <span style='color:red;'>" + data.dmg + "</span> points of damage";
                            } else if (data.msg) {
                                msg += data.msg;
                            }
                        }
                        break;
                }
            }
        }

        if (msg !== "") {
            $logUl.append('<li>' + msg + '</li>');
            $logUl.scrollTop(9999);
        }
    },
    /**
     *
     */
    popup : function (config) {
        var idpop = (config.id) ? config.id : "mainpop",
            baseTpl = '<div class="popup_overlay">' +
                '<div class="popup" id="' + idpop +'">' +
                '<a data-close="' + idpop +'" class="popup_close">X</a>' +
                '<div class="content">POP_CONTENT</div>' +
                '</div>' +
                '</div>',
            content = '',
            holder,
            title = "Info";

        if (config.elem) {
            holder = $("#" + config.elem);
            if (holder.length > 0) {
                content = holder.html();
            }
        } else if (config.html) {
            content = config.html;
        }

        if (content != "") {
            baseTpl = baseTpl.replace("POP_CONTENT", content)
            $(baseTpl).appendTo("body").show();
        }
    },
    popupClose : function (id) {
        var parent = $("#" + id).parent(".popup_overlay");
        if (parent.length > 0) {
            $(parent).fadeOut().html('').remove();
        }
    }
};


CardsUI.FindGame = {
    chatSelector : "#mainChatLog",
    overlaySelector : ".sOverlay",
    events : {
        user_left : "userLeft",
        new_user  : "newUser",
        new_msg   : "newMsg",
        chat_joined : "chatJoined",
        disconnect  : "disconnected"
    },
    htmlEvents : {
        "#sendMessage" : ["submit", "submitSendMsg"],
        "#findGame2"   : ["click", "startFindGame"],
        "#stopFind"    : ["click", "stopFindGame"]
    },
    connected : false,
    init : function () {
        var _self = CardsUI.FindGame;
        CardsUI.connectToLive(function () {
            CardsUI.socket.emit("join_chat");
            //bind all socket event listeners
            if (!_self.connected) {
                for (var eventN in _self.events) {
                    if (_self.events.hasOwnProperty(eventN)) {
                        CardsUI.socket.on(eventN, _self[_self.events[eventN]]);
                    }
                }
            }
            _self.connected = true;
        });

        //bind HTML events
        for (var eventH in _self.htmlEvents) {
            if (_self.htmlEvents.hasOwnProperty(eventH)) {
                $("body").on(_self.htmlEvents[eventH][0], eventH, _self[_self.htmlEvents[eventH][1]]);
            }
        }
    },
    userLeft : function (data) {
        console.log("user left chat", data);
        $("#usersList").find("[data-username='" + data.u + "']").html('').remove();;
    },
    newUser  : function (data) {
        console.log("new member in chat", data);
        if ($("#usersList").find("[data-username='" + data.u + "']").length == 0) {
            $("#usersList").append('<li data-username="' + data.u +'"><i class="icon-user"></i>' + data.u +'</li>');
        }
    },
    newMsg  : function (data) {CardsUI.FindGame.addMsgToChat(data);},

    chatJoined  : function (data) {
        $(".sOverlay").hide();
        CardsUI.FindGame.addMsgToChat({msg : "<span style='color: green'>You joined to chat server</span>"});
    },
    disconnected: function () {
        CardsUI.FindGame.addMsgToChat({msg : "<span style='color: red'>Connection to server lost</span>"});
    },
    submitSendMsg : function (event) {
        CardsUI.socket.emit("chat_msg",{msg : $("#chatInput").val()});
        $("#chatInput").val('');
        event.preventDefault();
    },
    startFindGame : function (event) {
        $("#findGame2").addClass("disabled").html("Searching....");
        $("#stopFind").removeClass("disabled");
        CardsUI.findGame();
    },
    stopFindGame  : function (event) {
        $("#stopFind").addClass("disabled");
        $("#findGame2").removeClass("disabled").html("Find game");
        CardsUI.stopFind();
    },
    addMsgToChat:function (msgObj) {
        var msg = "",
            _self = CardsUI.FindGame;
        if (!msgObj.time) {
            msg += "[" + (new Date()).toLocaleTimeString() + "] ";
        }

        if (msgObj.u) {
            msg += "<a>" + msgObj.u + "</a>: ";
        }

        if (!msgObj.msg) {
            return false;
        }

        msg += msgObj.msg;

        $(_self.chatSelector).append("<li>" + msg + "</li>");
    }
}

$(document).ready(function () {
    if (typeof tpl != "undefined") {
        CardsUI.bindControls(tpl);
        $("body").on("click",".popup_close", function () {
            CardsUI.service.popupClose($(this).data("close"));
        });
    }
});
