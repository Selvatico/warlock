var md5 = require('MD5');
var mongo = require("mongodb");

exports.index = {
    //model : require(),

    /**
     * Registration method
     */
    /**
     * @type UsersModel
     */
    DB : require("../models/user.model"),
    //_idToString :
    ajax_sign_in:function (req, resp, app) {
        var _self = this,
            emailRegExp = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/,
            usernameRegExp = /^[a-zA-Z0-9]+$/;

        if (req.session.member && req.session.member.u) return {r:true };

        var response = {r:false, error:[]},
            params = req.body,
            _self = this;


        if (!params.e || params.e.length < 3 || !emailRegExp.test(params.e)) {
            response.error.push("You entered wrong email adress");
        }


        if (!params.n) {
            if(params.n.length < 3) {
                if (!usernameRegExp.test(params.n)) {
                    response.error.push("Wrong username. It should contains only latin chars and numbers");
                }
            }
        }

        //check paswords
        if (params.p && params.p.length >= 6) {
            if (params.p !== params.p2) {
                response.error.push("Password confirmation doesn't match");
            }
        } else {
            response.error.push("Password should contain at leat 6 chars");
        }

        if (response.error.length > 0) {
            return response;
        }

        //var md5Email = md5(req.body.e);
        var md5Email = (params.e),
            username = params.n;

        _self.DB.checkUser({email : params.e},function (result){
             if (!result) {
                 response.r = true;
                 var idU = _self.createUID(10),
                     userData = {p:md5(params.p), u: username, uid:idU, rating : 1200, wins: 0, lose: 0, drafts : 0, leaves : 0, email : params.e};

                 _self.DB.createUser(userData, function(record){
                     userData._id = record._id.toString();
                 });

                 req.session.member = userData;
                 resp.send(response);
             } else {
                 response.error.push("This email already in use");
                 resp.send(response);
             }
        });
    },
    createUID:function (length) {
        var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz",
            randomisation = '',
            llen = length || 14;
        for (var i = 0; i < llen; i++) {
            var rnum = Math.floor(Math.random() * chars.length);
            randomisation += chars.substring(rnum, rnum + 1);
        }
        return randomisation;
    },
    ajax_log_in:function (req, resp, app) {
        var response = {r:false, msg:"Wrong login or password"};
        this.DB.checkUser({email : req.body.l}, function (record){
            if (record) {
                if (record.p == md5(req.body.p)) {
                    record._id = record._id.toString();
                    req.session.member = record;
                    response.r = true;
                }
            }
            resp.send(response);
        });
    },
    logout:function (req, resp, app) {
        if (req.session.member) {
            delete req.session.member;
        }
        resp.redirect("/");
    },
    "account/:id?" : function (req, resp, app) {
        //if (req.params.id) {
    },
    index : function () {

    },
    edit : function () {

    },
	sign_in : function () {

	},
    rankings:function (req, resp, app) {
        this.DB.rankingsList(false, function(rows){
            resp.render("user/rankings", {session : req.session, users : rows, conf : app.appMyConfig, tpl : "user_rankings"});
        });
        return {stop:true};
    }
};
