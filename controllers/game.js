var gameManager = require("../classes/CardsHeroManager").manager;

var allowedGameMoves = {
    cc:"callCardProxy",
    em:"endMove",
    cca : "castCreateAbility",
    sr  : "surrender"
};

exports.index = {
    DB : require("../models/game.model"),
    /**
     *
     * @param [req]
     * @param [res]
     * @return {Object}
     */
    find:function (req, res) {
        var response = {tpl:"join"};
        //if not logged in go away please
        if (!req.session.member) {
            res.redirect("/");
        }
        return response;
    },
    find3 : function () {
        return {users : gameManager.chatMembers.members};
    },
    ajax_move:function (req, res, app) {
        var params = req.body, session = req.session;
        if (params && params.a && allowedGameMoves[params.a]) {
            if (session.member.gameId) {
                res.send(
                    gameManager.performGameAction(session.member.gameId, session.member.uid, allowedGameMoves[params.a], params.d, res)
                );
            }


        }
    },
    /**
     * Game page
     * @param {Object} req Request object
     * @param {Object} req.params Params from parsed url
     * @param {Object} req.session Session of member
     * @param {Object} res Response object
     * @param {Object} app
     * @returns {null|Object}
     */
    "play/:id?":function (req, res, app) {
        var playerUid,
            tplVars = {};
        if (!req.session.member || !req.session.member.uid) res.redirect("/");
        playerUid = req.session.member.uid;

        //if we didn't received game ID
        if (req.params.id) {
            //check game exist
            var game = gameManager.getRunningGame(req.params.id.toString());
            if (game) {
                if (game.inGame(playerUid) && !game.isFinished()) {
                    req.session.member.gameId = req.params.id.toString();
                    tplVars.gameData = game.gatherGameInfo(playerUid);
                    tplVars.found = true;
                }
                return tplVars;
            } else {
                return {found : false};
                //res.redirect(404,"/game/find");
            }
        } else {
            return {found : false};
            //res.redirect(404,"/game/find");
        }
    },
    "play2/:id?" : function (req, res, app) {
        var playerUid,
            tplVars = {};
        if (!req.session.member || !req.session.member.uid) res.redirect("/");
        playerUid = req.session.member.uid;

        //if we didn't received game ID
        if (req.params.id) {
            //check game exist
            var game = gameManager.getRunningGame(req.params.id.toString());
            if (game) {
                if (game.inGame(playerUid) && !game.isFinished()) {
                    req.session.member.gameId = req.params.id.toString();
                    tplVars.gameData = game.gatherGameInfo(playerUid);
                    tplVars.found = true;
                }
                return tplVars;
            } else {
                return {found : false};
                //res.redirect(404,"/game/find");
            }
        } else {
            return {found : false};
            //res.redirect(404,"/game/find");
        }
    },

    about : function () {

    },
    cards : function () {

    }
};