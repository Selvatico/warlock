var UsersModel = {
    tableObj : false,
    collectionName : "users",
    /**
     * Save reference to users collection object from native mongo driver
     * @param {Collection} collection Link to users collection in DB
     */
    init  : function (collection) {
        if (collection) {
            this.tableObj = collection;
        }
        //this.test_query();
    },
    test_query : function () {
        this.tableObj.find().toArray(function(err, results){
            console.log(err, results);
        });
    },
    /**
     * Check if email exist in the base when user sign in.
     * This functions used when member sign in to check exist of email in base and when member log in.
     * @param {Object} fields Fields and value which to use as where statement
     * @param {Function} callback Funtionm which receive result of check
     */
    checkUser : function (fields, callback) {
        this.tableObj.findOne(fields, function(err, result){
            if (err) throw err;
            callback(((result == null) ? false : result));
        });
    },
    /**
     * Method to create new member after sign in.
     * @param {Object} data New user data
     * @param {Function} callback
     */
    createUser : function (data, callback){
        this.tableObj.insert(data, function (err, docs) {
            if (err) throw err;
            callback(docs[0]);
        });
    },
    /**
     * Change user rating after enad of the game
     * @param {String|Object} iUid Member ID created be site
     * @param {Object} [data] Value to add to current member rating. Can be positive or negative
     */
    changeRating:function (iUid, data) {
        var _self = this,
            updateData;
        if (typeof iUid == "object") {
            for (var member in iUid) {
                if (iUid.hasOwnProperty(member)) {
                    _self.changeRating(member, iUid[member]);
                }
            }
            return;
        }

        _self.tableObj.findOne({uid:iUid}, function (err, row) {
            if (err) throw err;
            if (row != null) {
                updateData = {
                    rating:row.rating + data.rating,
                    lose : row.lose + data.lose,
                    wins : row.wins + data.wins
                };
                _self.tableObj.update({uid:iUid}, {$set:updateData}, {safe:true}, function (err) {
                    if (err) throw err;
                    console.log("Member rating updated");
                })
            }
        });
    },
    /**
     * Get all users ordered by ranking
     * @param {Object} filter
     * @param {Function} callback Function to give rows
     */
    rankingsList : function (filter, callback) {
        this.tableObj.find().sort({ rating:-1 }).toArray(function (err, rows) {
            if (err) throw err;
            if (callback) {
                callback(rows);
            }
        });
    }

};


module.exports = UsersModel;