var StatsModel = {
    tableObj : false,
    collectionName : "stats",
    /**
     * @param collection
     */
    init  : function (collection) {
        if (collection) {
            this.tableObj = collection;
        }
        //this.test_query();
    },
    test_query : function () {
        this.tableObj.find().toArray(function(err, results){
            console.log(err, results);
        });
    }
};


module.exports = StatsModel ;