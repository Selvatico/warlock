var GameModel = {
    tableObj : false,
    collectionName : "games",
    /**
     * @param collection
     */
    init  : function (collection) {
        if (collection) {
            this.tableObj = collection;
        }
    },
    test_query : function () {
        this.tableObj.find().toArray(function(err, results){
            console.log(err, results);
        });
    },
    saveGameStats : function (data) {
        this.tableObj.insert(data, function(err, docs){
            if (err) throw err;
            console.log("Game stats saved");
        });
    }
};


module.exports = GameModel;