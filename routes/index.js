var util = require('util');
var fs = require('fs')
    , express = require('../node_modules/express');
var mongo = require("mongodb");

/**
 * Simple omplementation of MVC pattern
 * @type {Object}
 */
exports.mvcLoader = {
    /**
     * Move through controllers dir and load files and pass them to createRoutes method
     * @param {Object} app Global Express app object
     */
    bootControllers:function (app) {
        var _self = this;
        fs.readdir(app.basePath + '/controllers', function (err, files) {
            //console.log(arguments);return;
            if (err) throw err;
            files.forEach(function (file) {
                _self.createRoutes(app, file);
            });
        });
    },
    /**
     * Model part of MVC
     * @param {Object} app Express app object
     * @param {Object} config MongoDB conect config
     */
    bootModels : function (app, config) {
        app.mongoClient = new mongo.Db(config['db'], new mongo.Server(config['host'], config['port'], {}), {w: 1});
        app.mongoClient.open(function(err, p_client) {
            console.log("MongoDB conected");

            fs.readdir(app.basePath + '/models', function (err, files) {
                if (err) throw err;
                files.forEach(function (file) {
                    var model = require(app.basePath + '/models/' + file.replace('.js', ''));
                    if (model.collectionName) {
                        app.mongoClient.collection(model.collectionName, function(err, collection){
                            model.init(collection);
                        });
                    }
                });
            });
        });
    },
    /**
     * Assign vars which will be avalaible through all templates
     * @param {Object} app Express app object
     */
    templateGlobalVars:function (app) {
        app.tplVars = {
            title:"Cards Hero",
            version:app.version,
            conf : app.appMyConfig
        }
    },
    /**
     * @deprecated
     * @param app
     */
    bootGameClasses:function (app) {},
    /**
     * Iterate through loaded controllers methods
     * and create routes POST or GET, depends on method name prefix
     * @param {Object} app Global Express app object
     * @param {String} file Controller file name
     */
    createRoutes:function (app, file) {
        var name = file.replace('.js', '')
            , actions = require(app.basePath + '/controllers/' + name).index
            , prefix = '/' + name;

        //default router
        app.get('/', function (req, res) {
            res.render("index", {gl:app.tplVars, conf : app.appMyConfig, tpl:"index", session:req.session});
        });


        Object.keys(actions).forEach(function (action) {
            //check if it method and not a field
            if (typeof actions[action] != "function") return false;
            //check http method
            var httpMethod = (~action.indexOf("ajax")) ? "post" : "get";
            //create url for browser
            var actionURL = (httpMethod == "post") ? action.replace("ajax_", "").toLowerCase() : action;
            //save route in global app object
            console.log(prefix + '/' + actionURL);
            app[httpMethod](prefix + '/' + actionURL, function (request, response) {

                //prevent of direct calls via browser for ajax requests
                if (httpMethod == "post" && !request.xhr) return false;

                var data = actions[action](request, response, app);
                var viewParams = (data != undefined) ? data : {};

                if (!!viewParams.stop) {return false;}

                if (httpMethod == "post") {
                    if (data) {
                        return response.send(viewParams);
                    }
                } else {
                    viewParams.gl = app.tplVars;
                    viewParams.conf = app.appMyConfig;
                    viewParams.session = request.session;
                    viewParams.requestParam = request.params;
                    var viewName = (~action.indexOf("/")) ? action.split("/")[0] : action;
                    viewParams.tpl = name + "_" + viewName;
                    response.render(name + "/" + viewName, viewParams);
                }
                return true;
            });
            return true;
        });
    }
};