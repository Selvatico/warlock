module.exports = {
    redis : {
        host : "127.0.0.1",
        port : "6379",
        opt  : {}
    },
    server : {
        host : "192.168.1.15",
        port : 3001
    },
    mongo : {
        host : "127.0.0.1",
        port : 27017,
        db   : "cards"
    },
    staticUrl : ""
};
