module.exports = {
    redis : {
        host : "127.0.0.1",
        port : "6379",
        opt  : {}
    },
    server : {
        host : "localhost",
        port : 3000
    },
    mongo : {
        host : "127.0.0.1",
        port : 27017,
        db   : "cards"
    },
    staticUrl : ""
};